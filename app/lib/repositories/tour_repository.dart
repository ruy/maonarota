import 'dart:async';
import 'dart:isolate';
import 'dart:ui';
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:mao_na_rota/model/download_item.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/model/trivia.dart';
import 'package:mao_na_rota/model/update_tour_item.dart';
import 'package:mao_na_rota/services/local_storage.dart';
import 'package:mao_na_rota/data/tours.dart' as mock;
import 'package:mao_na_rota/services/api_client.dart';
import 'package:mao_na_rota/model/tour.dart';

class TourRepository {
  ReceivePort _port = ReceivePort();
  StreamController<UpdateTourItem> updateStreamController = StreamController();
  Stream<UpdateTourItem> updateStream;
  List<Tour> _tours;

  TourRepository() {
    IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_port');

    updateStream = updateStreamController.stream;

    _port.listen((dynamic data) {
      String taskId = data[0];
      DownloadTaskStatus status = data[1];

      Tour tour = tourByTaskId(taskId);
      if (tour != null) {
        updateStreamController.add(UpdateTourItem(
          tour: tour,
          completed: status == DownloadTaskStatus.complete,
          taskId: taskId,
        ));
      }
    });
  }

  Tour tourByTaskId(String taskId) {
    try {
      return _tours.firstWhere((tour) =>
          tour.localFiles.any((downloadItem) => downloadItem.id == taskId));
    } on StateError {
      return null;
    }
  }

  Future<List<Tour>> _parseTourResponse(String response) async {
    // List<Object> parsed = json.decode(response) as List<Object>;

    // return parsed.map<Tour>((json) => Tour.fromJson(json)).toList();

    String path = await LocalStorage.applicationsDirectory;

    List<Tour> tours = mock.getGuidedTours()..addAll(mock.getAutoGuidedTours());

    for (Tour tour in tours) {
      tour.appDirPath = path;
    }
    return tours;
  }

  List<Tour> mergeRetrievedAndSavedTours(
      List<Tour> retrieved, List<Tour> saved) {
    retrieved.sort((first, second) => first?.id?.compareTo(second.id));

    for (Tour tour in saved) {
      int indexInRetrived = binarySearch(retrieved, tour,
          compare: (first, second) => first?.id?.compareTo(second.id));

      if (indexInRetrived != -1) {
        if (retrieved[indexInRetrived].lastUpdated != null &&
            tour.lastUpdated != null &&
            retrieved[indexInRetrived].lastUpdated.isAfter(tour.lastUpdated)) {
          removeDownloadedTour(tour);
        } else {
          // retrieved[indexInRetrived] = tour;
        }
      } else {
        removeDownloadedTour(tour);
      }
    }

    return retrieved;
  }

  void saveToursLocally() {
    try {
      LocalStorage.saveListInStorage('saved_tours', jsonEncode(_tours));
    } catch (e) {}
  }

  Future<List<Tour>> getTours() async {
    String response = await API.getTours();
    List<Tour> tours = await _parseTourResponse(response);

    try {
      List<Tour> savedTours = await getSavedTours();

      if (tours.isEmpty) {
        tours = savedTours;
      } else if (savedTours.isNotEmpty) {
        tours = mergeRetrievedAndSavedTours(tours, savedTours);
      }
    } finally {
      _tours = tours;
      saveToursLocally();
    }

    return tours;
  }

  Future<List<Tour>> getSavedTours() async {
    return _parseSavedTours(
        await LocalStorage.getListInStorage('saved_tours') ?? '');
  }

  List<Tour> _parseSavedTours(String jsonString) {
    if (jsonString.isEmpty) {
      return [];
    }

    List<Object> parsed = json.decode(jsonString) as List<Object>;

    return parsed.map<Tour>((json) => Tour.fromJson(json)).toList();
  }

  void saveTours(List<Tour> tours) {
    _tours = tours;
    saveToursLocally();
  }

  Future<String> download(String filename, String url, String appDirPath) {
    return FlutterDownloader.enqueue(
      fileName: filename,
      url: Uri.parse(url).toString(),
      showNotification: false,
      savedDir: appDirPath,
      openFileFromNotification: false,
    );
  }

  Future<Tour> downloadTour(Tour tour) async {
    String appDirPath = await LocalStorage.applicationsDirectory;

    // Tour Audio

    if (tour.audioDescriptionUrl != null &&
        tour.audioDescriptionUrl.isNotEmpty) {
      tour.localFiles.add(
        DownloadItem(
          id: await download(tour.localAudioDescriptionFilename,
              tour.audioDescriptionUrl, appDirPath),
          url: tour.audioDescriptionUrl,
        ),
      );
    }

    // Tour Picture

    if (tour.pictureUrl != null && tour.pictureUrl.isNotEmpty) {
      tour.localFiles.add(
        DownloadItem(
          id: await download(
              tour.localPictureFilename, tour.pictureUrl, appDirPath),
          url: tour.pictureUrl,
        ),
      );
    }

    for (var i = 0; i < tour.points.length; i++) {
      if (tour.points[i].audioDescriptionUrl != null &&
          tour.points[i].audioDescriptionUrl.isNotEmpty) {
        // Tour Point Audio

        tour.localFiles.add(
          DownloadItem(
            id: await download(tour.points[i].localAudioDescriptionFilename,
                tour.points[i].audioDescriptionUrl, appDirPath),
            url: tour.points[i].audioDescriptionUrl,
          ),
        );
      }

      // Tour Point Pictures

      tour.points[i].pictureUrls.asMap().forEach(
        (j, url) async {
          if (url != null && url.isNotEmpty) {
            tour.localFiles.add(
              DownloadItem(
                id: await download(
                    tour.points[i].localPictureFilenames[j], url, appDirPath),
                url: url,
              ),
            );
          }
        },
      );

      // Trivia
      tour.points[i].trivia.forEach(
        (triviaItem) async {
          if (triviaItem.mediaUrl != null && triviaItem.mediaUrl.isNotEmpty) {
            tour.localFiles.add(
              DownloadItem(
                  id: await download(triviaItem.localMediaFilename,
                      triviaItem.mediaUrl, appDirPath),
                  url: triviaItem.mediaUrl),
            );
          }
        },
      );
    }

    // Map
    if (tour.mapUrl != null && tour.mapUrl.isNotEmpty) {
      tour.localFiles.add(
        DownloadItem(
            id: await download(tour.localMapFilename, tour.mapUrl, appDirPath),
            url: tour.mapUrl),
      );
    }

    return tour;
  }

  Future<Tour> removeDownloadedTour(Tour tour) async {
    for (DownloadItem item in tour.localFiles) {
      FlutterDownloader.remove(taskId: item.id, shouldDeleteContent: true);
    }

    tour.localFiles = [];
    tour.shouldBeSaved = false;

    for (TourPoint point in tour.points) {
      point.isSaved = false;
      for (Trivia trivia in point.trivia) {
        trivia.isSaved = false;
      }
    }

    return tour;
  }
}
