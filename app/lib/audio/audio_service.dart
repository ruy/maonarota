import 'dart:async';
import 'package:audio_service/audio_service.dart';
import 'package:just_audio/just_audio.dart';

class AudioServiceTask extends BackgroundAudioTask {
  AudioPlayer _audioPlayer;
  bool _playing = false;
  MediaItem mediaItem;

  StreamSubscription<AudioPlaybackEvent> _eventSubscription;

  @override
  Future<void> onStart(Map<String, dynamic> params) async {
    _audioPlayer = AudioPlayer();

    AudioServiceBackground.setState(
      controls: [],
      playing: false,
      processingState: AudioProcessingState.connecting,
    );

    _audioPlayer.playbackStateStream
        .where((state) => state == AudioPlaybackState.completed)
        .listen((state) async {
      await _audioPlayer.stop();
    });

    _eventSubscription = _audioPlayer.playbackEventStream.listen(
      (event) {
        final bufferingState =
            event.buffering ? AudioProcessingState.buffering : null;
        switch (event.state) {
          case AudioPlaybackState.paused:
            _setState(
                processingState: bufferingState ?? AudioProcessingState.ready,
                position: event.position,
                controls: [rewindControl, playControl, fastForwardControl]);
            break;
          case AudioPlaybackState.playing:
            _setState(
                processingState: bufferingState ?? AudioProcessingState.ready,
                position: event.position,
                controls: [rewindControl, pauseControl, fastForwardControl]);
            break;
          case AudioPlaybackState.connecting:
            _setState(
              processingState: AudioProcessingState.connecting,
              position: event.position,
            );
            break;
          default:
            break;
        }
      },
    );
  }

  @override
  void onTaskRemoved() {
    onStop();
    super.onTaskRemoved();
  }

  void setUrl(url) async {
    if (_playing) {
      await _audioPlayer.pause();
    }

    Duration duration = await _audioPlayer.setUrl((url as String));

    mediaItem = MediaItem(
      id: url,
      album: '',
      title: '',
      duration: duration,
    );

    AudioServiceBackground.setMediaItem(
      mediaItem,
    );

    onPlay();
  }

  @override
  Future<void> onCustomAction(String name, dynamic url) async {
    switch (name) {
      case 'set':
        setUrl(url);
        break;
    }
  }

  void playPause() {
    if (AudioServiceBackground.state.playing) {
      onPause();
    } else {
      onPlay();
    }
  }

  @override
  void onPlay() {
    if (AudioServiceBackground.state.processingState ==
        AudioProcessingState.completed) {
      _playing = true;
      _audioPlayer.seek(Duration(milliseconds: 0));
      _audioPlayer.play();
    } else if (AudioServiceBackground.state.processingState !=
        AudioProcessingState.error) {
      _playing = true;
      _audioPlayer.play();
    }
  }

  @override
  void onPause() {
    if (AudioServiceBackground.state.processingState !=
            AudioProcessingState.completed &&
        AudioServiceBackground.state.processingState !=
            AudioProcessingState.error) {
      _playing = false;
      _audioPlayer.pause();
    }
  }

  void stop() {
    _playing = false;
    _audioPlayer.stop();
  }

  @override
  void onSeekTo(Duration position) async {
    AudioProcessingState nextState;

    if (_audioPlayer.playbackState == AudioPlaybackState.none ||
        _audioPlayer.playbackState == AudioPlaybackState.connecting) {
      return;
    }

    if (position != null &&
        position.inSeconds >= _audioPlayer.playbackEvent.duration.inSeconds) {
      position = _audioPlayer.playbackEvent.duration;
      stop();
      nextState = AudioProcessingState.completed;
    } else if (position != null && position.inSeconds < 0) {
      position = Duration(seconds: 0);
      nextState = AudioProcessingState.ready;
    }

    await _audioPlayer.seek(position);

    _setState(
      processingState: nextState,
      position: position,
    );
  }

  void forward30() {
    _audioPlayer
        .seek(_audioPlayer.playbackEvent.position + fastForwardInterval);
  }

  void rewind30() {
    _audioPlayer.seek(_audioPlayer.playbackEvent.position - rewindInterval);
  }

  @override
  Future<void> onRewind() async {
    rewind30();
  }

  @override
  Future<void> onFastForward() async {
    forward30();
  }

  @override
  void onClick(MediaButton button) {
    playPause();
  }

  @override
  Future<void> onStop() async {
    if (_audioPlayer != null &&
        _audioPlayer.playbackState != AudioPlaybackState.none) {
      await _audioPlayer.stop();
      await _audioPlayer.dispose();
    }
    if (_eventSubscription != null) {
      _eventSubscription.cancel();
    }

    _playing = false;
    await _setState(processingState: AudioProcessingState.stopped);
    await super.onStop();
  }

  Future<void> _setState({
    AudioProcessingState processingState,
    Duration position,
    Duration bufferedPosition,
    List<MediaControl> controls,
  }) async {
    if (position == null) {
      position = _audioPlayer.playbackEvent.position;
    }

    await AudioServiceBackground.setState(
      controls: controls ?? [],
      processingState:
          processingState ?? AudioServiceBackground.state.processingState,
      playing: _playing,
      position: position,
      bufferedPosition: bufferedPosition ?? position,
      speed: _audioPlayer.speed,
    );
  }

  // List<MediaControl> getControls() {
  //   if (_playing) {
  //     return [
  //       seekBack30,
  //       pauseControl,
  //       seekFwd30,
  //     ];
  //   } else {
  //     return [
  //       seekBack30,
  //       playControl,
  //       seekFwd30,
  //     ];
  //   }
  // }
}

MediaControl playControl = MediaControl(
  androidIcon: 'drawable/ic_stat_play_arrow',
  label: 'Play',
  action: MediaAction.play,
);
MediaControl pauseControl = MediaControl(
  androidIcon: 'drawable/ic_stat_pause',
  label: 'Pausa',
  action: MediaAction.pause,
);
MediaControl fastForwardControl = MediaControl(
  androidIcon: 'drawable/ic_stat_forward_30',
  label: '-30s',
  action: MediaAction.fastForward,
);

MediaControl rewindControl = MediaControl(
  androidIcon: 'drawable/ic_stat_replay_30',
  label: '+30s',
  action: MediaAction.rewind,
);
