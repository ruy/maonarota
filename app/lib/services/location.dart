import 'package:location/location.dart';

Stream<bool> locationServiceStatus(Duration interval) async* {
  Location location = Location();

  while (true) {
    var serviceEnabled = await location.serviceEnabled();
    var permissionGranted = await location.hasPermission();

    if (serviceEnabled && permissionGranted == PermissionStatus.granted) {
      yield true;
    } else {
      yield false;
    }
  }
}
