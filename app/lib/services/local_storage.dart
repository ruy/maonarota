import 'dart:io';
import 'package:path_provider/path_provider.dart';

// Base: https://www.raywenderlich.com/5965747-data-persistence-on-flutter#toc-anchor-008

// When adding Json Serializable classes:
// Use this: flutter packages pub run build_runner build --delete-conflicting-outputs

class LocalStorage {
  static Future<String> get applicationsDirectory async {
    return (await getApplicationDocumentsDirectory()).path;
  }

  static Future<String> get localPath async {
    final directory = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static void saveListInStorage(String key, String json) async {
    final filename = await _getFilename(key);
    final file = await _localFile(filename);

    if (!await file.parent.exists()) await file.parent.create(recursive: true);

    await file.writeAsString(json);
  }

  static Future<String> getListInStorage(String key) async {
    try {
      return await _getObjectInFilesAsString(key) ?? '';
    } catch (e) {
      return '';
    }
  }

  static Future<String> _getObjectInFilesAsString(String key) async {
    final filename = await _getFilename(key);
    final file = await _localFile(filename);

    if (await file.exists()) {
      return file.readAsString();
    }
    return null;
  }

  static Future<String> _getFilename(String key) async {
    return '/' + key;
  }

  static Future<File> _localFile(String filename) async {
    final path = await localPath;
    return File('$path/$filename');
  }
}
