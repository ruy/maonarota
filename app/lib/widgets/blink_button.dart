import 'package:flutter/material.dart';

class BlinkButton extends StatefulWidget {
  final IconData icon;
  final Function onTap;
  final bool disabled;

  BlinkButton({
    @required this.icon,
    @required this.onTap,
    this.disabled = false,
  });

  @override
  _BlinkButtonState createState() => _BlinkButtonState();
}

class _BlinkButtonState extends State<BlinkButton>
    with SingleTickerProviderStateMixin {
  // var _isActive = 1.0;
  AnimationController _controller;
  Animation _animation;
  CurvedAnimation _curve;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 20),
    );
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    _animation = Tween(
      begin: 1.0,
      end: 0.6,
    ).animate(_curve);

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void blink() {
    // if (_isActive == true) {
    // print('here 1');
    // setState(() {
    //   _isActive = false;
    // });

    // Future.delayed(
    //   const Duration(milliseconds: 1),
    //   () {
    //     print('here 2');

    //     setState(() {
    //       _isActive = false;
    //     });
    //   },
    // );
    // }
  }

  void _onTapUp(TapUpDetails details) {
    if (widget.disabled) {
      return;
    }
    widget.onTap();
    _controller.reverse();
    // setState(() {
    //   _isActive = !_isActive;
    // });
    // setState(() {
    //   _isActive = 1.0;
    // });
    // blink();
  }

  void _onTapDown(TapDownDetails details) {
    if (widget.disabled) {
      return;
    }
    _controller.forward();
  }

  void _onTapCancel() {
    _controller.reverse();

    // setState(() {
    //   _isActive = 1.0;
    // });
  }

  // Color _statusToColor() {
  //   if (_isActive) {
  //     return Theme.of(context).accentColor;
  //   }

  //   return Color(0xFF7B7B7B);
  // }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: FadeTransition(
        opacity: _animation,
        child: Container(
          width: 22,
          height: 22,
          color: Colors.transparent,
          child: Icon(
            widget.icon,
            color: Color(0xFF7B7B7B),
            size: 22,
          ),
        ),
      ),
      onTapDown: (details) => _onTapDown(details),
      onTapUp: (details) => _onTapUp(details),
      onTapCancel: _onTapCancel,
    );
  }
}
