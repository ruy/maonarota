import 'package:flutter/material.dart';

class DetalSectionTitle extends StatelessWidget {
  final String title;
  final Widget suffix;

  DetalSectionTitle({
    @required this.title,
    this.suffix,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 17,
            fontWeight: FontWeight.w500,
          ),
        ),
        if (suffix != null) suffix
      ],
    );
  }
}
