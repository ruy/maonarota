import 'package:flutter/material.dart';

class CalendarMonth extends StatelessWidget {
  final String month;

  CalendarMonth({
    @required this.month,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: Text(
        month,
        style: TextStyle(
          color: Color(0xFF7C7C7C),
          fontSize: 16,
        ),
      ),
    );
  }
}
