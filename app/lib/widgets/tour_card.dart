import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/views/tour/tour_point_detail.dart';
import 'package:mao_na_rota/widgets/title_header.dart';

class TourCard extends StatefulWidget {
  final TourPoint point;

  TourCard({
    @required this.point,
  });

  @override
  _TourCardState createState() => _TourCardState();
}

class _TourCardState extends State<TourCard> {
  // var _active = false;

  void _onTap(BuildContext context) {
    Navigator.of(context)
        .pushNamed(TourPointDetail.routeName, arguments: widget.point.id);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(15.0),
      ),
      child: GestureDetector(
        onTap: () => _onTap(context),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            boxShadow: [
              BoxShadow(color: Colors.black87, blurRadius: 4),
            ],
          ),
          width: MediaQuery.of(context).size.width * 0.88,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TitleHeader(
                titleInfo: widget.point.titleInfo,
                audioDescriptionUrl: widget.point.audio,
              ),
              // SizedBox(
              //   height: 5,
              // ),
              // Text(
              //   widget.point.description,
              //   style: TextStyle(
              //     fontSize: 16,
              //     color: Color(0xFF7B7B7B),
              //     fontWeight: FontWeight.normal,
              //   ),
              //   softWrap: true,
              //   maxLines: 3,
              //   overflow: TextOverflow.ellipsis,
              // ),
              // if (!_active)
              //   Align(
              //     alignment: Alignment.bottomRight,
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.end,
              //       crossAxisAlignment: CrossAxisAlignment.center,
              //       children: [
              //         Text(
              //           'MAIS',
              //           style: TextStyle(
              //             color: Color(0xFF7B7B7B),
              //             fontSize: 10,
              //             fontWeight: FontWeight.normal,
              //           ),
              //         ),
              //         SizedBox(
              //           width: 2,
              //         ),
              //         Icon(
              //           Icons.keyboard_arrow_right,
              //           color: Color(0xFF7B7B7B),
              //           size: 15,
              //         ),
              //       ],
              //     ),
              //   ),
            ],
          ),
        ),
      ),
    );
  }
}
