import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:mao_na_rota/model/tour.dart';

class CarouselItemDescription extends StatelessWidget {
  final Tour tour;

  CarouselItemDescription({
    @required this.tour,
  });

  Widget dateText() {
    return Text(
      DateFormat('dd.MM').format(tour.tourDate),
      style: TextStyle(
        fontWeight: FontWeight.w600,
        color: Colors.white,
        fontSize: 18,
      ),
    );
  }

  Widget titleText(context) {
    return AutoSizeText(
      tour.titleInfo.title.toUpperCase(),
      style: TextStyle(
        fontWeight: FontWeight.normal,
        color: Theme.of(context).accentColor,
        fontSize: 100,
      ),
      maxLines: 2,
      maxFontSize: 26,
    );
  }

  Widget seeMoreRow(context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'SAIBA MAIS',
            style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 16,
              color: Color(0xFF9D9D9D),
            ),
          ),
          Icon(
            Icons.keyboard_arrow_right,
            size: 20,
            color: Color(0xFF9D9D9D),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        right: 30,
        bottom: 50,
      ),
      width: MediaQuery.of(context).size.width * 0.4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (tour.date != null) dateText(),
          titleText(context),
          seeMoreRow(context),
        ],
      ),
    );
  }
}
