import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour.dart';

class TourInfo extends StatelessWidget {
  final Tour tour;

  TourInfo({
    @required this.tour,
  });

  // Adapted from https://stackoverflow.com/questions/54775097/formatting-a-duration-like-hhmmss
  String _formatDuration(double minutes) {
    var _string = '';
    var _hours = minutes ~/ 60;
    var _updatedMinutes = (minutes - (_hours * 60)).toInt();

    if (_hours > 0) {
      _string += _hours.toString() + 'h';
      if (_updatedMinutes > 0) {
        _string += _updatedMinutes.toString();
      }
    } else {
      _string += minutes.toString() + 'm';
    }
    return _string;
  }

  String _formatDistance(double distance) {
    var _string = '';
    var _meters = distance.remainder(1000).toInt();
    var _km = (distance ~/ 1000);

    if (_km > 1) {
      _string += _km.toString() + ' km';
      if (_meters > 0) {
        _string += (_meters / 1000).toStringAsFixed(1);
      }
    } else {
      _string += _meters.toString() + ' m';
    }
    return _string;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          Icons.access_time,
          size: 24,
          color: Theme.of(context).accentColor,
        ),
        SizedBox(
          width: 8,
        ),
        Text(
          _formatDuration(tour.duration),
          style: TextStyle(
            fontSize: 16,
            color: Color(0xFF7B7B7B),
            fontWeight: FontWeight.normal,
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Icon(
          Icons.directions_walk,
          size: 24,
          color: Theme.of(context).accentColor,
        ),
        SizedBox(
          width: 6,
        ),
        Text(
          _formatDistance(tour.distance),
          style: TextStyle(
            fontSize: 16,
            color: Color(0xFF7B7B7B),
            fontWeight: FontWeight.normal,
          ),
        ),
      ],
    );
  }
}
