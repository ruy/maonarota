import 'package:flutter/material.dart';
import 'package:mao_na_rota/widgets/audio_player_button.dart';
import 'package:mao_na_rota/widgets/detail_section_text.dart';

class DescriptionSection extends StatelessWidget {
  final String text;
  final String audioDescriptionUrl;

  DescriptionSection({
    @required this.text,
    this.audioDescriptionUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: DetailSectionText(
            text: text,
            expandable: true,
          ),
        ),
        if (audioDescriptionUrl != null && audioDescriptionUrl.isNotEmpty)
          Container(
            margin: const EdgeInsets.only(left: 15),
            child: AudioPlayerButton(
              url: audioDescriptionUrl,
            ),
          ),
      ],
    );
  }
}
