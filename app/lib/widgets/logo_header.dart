import 'package:flutter/material.dart';

class LogoHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      padding: const EdgeInsets.symmetric(
        vertical: 20,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            'assets/images/logo_white.png',
            width: 25,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 10),
            child: Text(
              'MÃO\nNA ROTA',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Nunito',
                fontSize: 10,
              ),
            ),
          )
        ],
      ),
    );
  }
}
