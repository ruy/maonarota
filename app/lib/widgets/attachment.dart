// import 'package:flutter/material.dart';
// import 'package:mao_na_rota/model/attachment.dart';
// import 'package:mao_na_rota/widgets/audio_player_button.dart';
// import 'package:mao_na_rota/widgets/download_button.dart';

// class AttachmentItem extends StatelessWidget {
//   final Attachment attachment;
//   final bool enlarged;
//   final Function onTap;

//   AttachmentItem({
//     @required this.attachment,
//     this.enlarged = false,
//     this.onTap,
//   });

//   Widget typeToWidget() {
//     switch (attachment.type) {
//       case AttachmentType.Audio:
//         return AudioPlayerButton(
//           url: attachment.url,
//           enlarged: enlarged,
//         );
//       case AttachmentType.Download:
//       default:
//         return DownloadButton(
//           url: attachment.url,
//           enlarged: enlarged,
//         );
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return typeToWidget();
//   }
// }
