import 'package:flutter/material.dart';
import 'package:mao_na_rota/widgets/action_button.dart';

class DownloadButton extends StatefulWidget {
  final String url;
  final bool enlarged;
  final Function onTap;

  DownloadButton({
    @required this.url,
    this.onTap,
    this.enlarged = false,
  });

  @override
  _DownloadButtonState createState() => _DownloadButtonState();
}

class _DownloadButtonState extends State<DownloadButton> {
  void _onTap() {
    if (widget.onTap != null) {
      widget.onTap();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ActionButton(
      icon: Icons.file_download,
      onTap: _onTap,
      enlarged: widget.enlarged,
    );
  }
}
