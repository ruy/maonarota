import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';

class DetailSectionText extends StatelessWidget {
  final String text;
  final bool expandable;
  final double fontSize;

  DetailSectionText({
    @required this.text,
    this.expandable = true,
    this.fontSize = 16,
  });

  @override
  Widget build(BuildContext context) {
    if (!expandable) {
      return Text(
        text,
        style: TextStyle(
          fontSize: fontSize,
          color: Color(0xFF7B7B7B),
          fontWeight: FontWeight.normal,
        ),
        textAlign: TextAlign.start,
        softWrap: true,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
      );
    }

    return ExpandableNotifier(
      child: Expandable(
        theme: const ExpandableThemeData(
          tapBodyToCollapse: true,
        ),
        collapsed: ExpandableButton(
          child: Column(
            children: [
              Text(
                text,
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF7B7B7B),
                  fontWeight: FontWeight.normal,
                ),
                softWrap: true,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'MAIS',
                    style: TextStyle(
                      color: Color(0xFF7B7B7B),
                      fontSize: 10,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    color: Color(0xFF7B7B7B),
                    size: 15,
                  ),
                ],
              ),
            ],
          ),
        ),
        expanded: ExpandableButton(
          child: Column(
            children: [
              Text(
                text,
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF7B7B7B),
                  fontWeight: FontWeight.normal,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'MENOS',
                    style: TextStyle(
                      color: Color(0xFF7B7B7B),
                      fontSize: 10,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Icon(
                    Icons.keyboard_arrow_up,
                    color: Color(0xFF7B7B7B),
                    size: 15,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
