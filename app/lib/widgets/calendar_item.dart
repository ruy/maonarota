import 'dart:io';

import 'package:mao_na_rota/views/tour/tour_detail.dart';
import 'package:shimmer/shimmer.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mao_na_rota/model/tour.dart';

class CalendarItem extends StatelessWidget {
  final Tour tour;
  final bool loading;

  CalendarItem({
    @required this.tour,
    this.loading = false,
  });

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      closedColor: Theme.of(context).primaryColor,
      openColor: Theme.of(context).primaryColor,
      transitionType: ContainerTransitionType.fade,
      transitionDuration: const Duration(milliseconds: 500),
      tappable: !loading,
      openBuilder: (context, action) {
        return TourDetail(
          tourId: tour.id,
        );
      },
      closedBuilder: (context, action) {
        return Container(
          padding: const EdgeInsets.symmetric(
            // horizontal: 20.0,
            vertical: 10.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ShaderMask(
                shaderCallback: (rect) {
                  return LinearGradient(
                    begin: Alignment.bottomRight,
                    end: Alignment.topLeft,
                    colors: [Colors.black54, Colors.transparent],
                  ).createShader(
                    Rect.fromLTRB(0, 0, rect.width, rect.height),
                  );
                },
                blendMode: BlendMode.darken,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                  child: loading
                      ? Shimmer.fromColors(
                          enabled: true,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            height: 100,
                          ),
                          baseColor: Theme.of(context).primaryColor,
                          highlightColor: Color(0xFF424141),
                        )
                      : Container(
                          height: 100,
                          decoration: BoxDecoration(
                            color: Colors.grey[800],
                            image: DecorationImage(
                              image: tour.isSaved
                                  ? FileImage(File(tour.picture))
                                  : NetworkImage(tour.picture),
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              loading
                  ? Shimmer.fromColors(
                      enabled: true,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        height: 20,
                      ),
                      baseColor: Theme.of(context).primaryColor,
                      highlightColor: Color(0xFF424141),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              DateFormat('dd.MM').format(tour.tourDate),
                              style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(
                              width: 6,
                            ),
                            Text(
                              tour.titleInfo.title,
                              style: TextStyle(
                                color: Color(0xFFC4C4C4),
                                fontSize: 22,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
              SizedBox(
                height: loading ? 10 : 4,
              ),
            ],
          ),
        );
      },
    );
  }
}
