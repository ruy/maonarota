import 'dart:io';

import 'package:animations/animations.dart';
import 'package:mao_na_rota/widgets/action_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:mao_na_rota/model/trivia.dart';
import 'package:mao_na_rota/widgets/audio_player_button.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_view/photo_view.dart';
import 'package:provider/provider.dart';
import 'package:path_provider/path_provider.dart';

class TourPointTriviaCard extends StatelessWidget {
  final String triviaId;

  TourPointTriviaCard(
    this.triviaId,
  );

  String getExtension(String url) {
    if (url == null || url.isEmpty) return '';
    return '.' + url.split('.').last;
  }

  void download(Trivia trivia) async {
    PermissionStatus permission = await Permission.storage.request();

    if (permission.isGranted) {
      Directory directory = Platform.isAndroid
          ? await getExternalStorageDirectory()
          : await getApplicationDocumentsDirectory();

      FlutterDownloader.enqueue(
        fileName:
            trivia.title.replaceAll(' ', '_') + getExtension(trivia.mediaUrl),
        url: Uri.parse(trivia.mediaUrl).toString(),
        showNotification: true,
        savedDir: directory.path,
        openFileFromNotification: true,
      );
    } else {
      if (permission.isPermanentlyDenied ||
          (permission.isDenied && Platform.isIOS)) {
        openAppSettings();
      }
    }
  }

  Widget pictureCard(Trivia trivia, BuildContext context) {
    bool displayPlaceholder = trivia.media == null || trivia.media.isEmpty;

    return OpenContainer(
      closedElevation: 0,
      closedColor: Theme.of(context).primaryColor,
      openColor: Theme.of(context).primaryColor,
      transitionType: ContainerTransitionType.fade,
      transitionDuration: const Duration(milliseconds: 500),
      tappable: !displayPlaceholder,
      openBuilder: (context, action) {
        return PhotoView(
          imageProvider: trivia.isSaved
              ? FileImage(File(trivia.media))
              : NetworkImage(trivia.media),
          minScale: PhotoViewComputedScale.contained,
          maxScale: PhotoViewComputedScale.contained * 2.0,
          initialScale: PhotoViewComputedScale.contained,
          onTapUp: (_, __, ___) => action(),
          loadingBuilder: (context, event) => Center(
            child: Container(
              width: 20.0,
              height: 20.0,
              color: Theme.of(context).primaryColor,
              child: CircularProgressIndicator(
                backgroundColor: Theme.of(context).accentColor,
                value: event == null
                    ? 0
                    : event.cumulativeBytesLoaded / event.expectedTotalBytes,
              ),
            ),
          ),
          backgroundDecoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
          ),
        );
      },
      closedBuilder: (context, action) {
        return displayPlaceholder
            ? CircleAvatar(
                radius: 18,
                child: Image.asset(
                  'assets/images/logo_small.png',
                  width: 30,
                  height: 30,
                ),
                backgroundColor: Colors.grey[800],
              )
            : CircleAvatar(
                radius: 18,
                backgroundColor: Colors.grey[800],
                backgroundImage: displayPlaceholder
                    ? AssetImage(
                        'assets/images/logo_small.png',
                      )
                    : trivia.isSaved
                        ? FileImage(File(trivia.media))
                        : NetworkImage(trivia.media),
              );
      },
    );
  }

  Widget buildActionButton(Trivia trivia, BuildContext context) {
    switch (trivia.type) {
      case 'attachment':
        return ActionButton.blink(
          onTap: () => download(trivia),
          icon: Icons.file_download,
        );
      case 'picture':
        return pictureCard(trivia, context);
      case 'audio':
      default:
        return AudioPlayerButton(
          url: trivia.media,
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, Trivia>(
      selector: (_, model) => model.triviaByTriviaId(triviaId),
      shouldRebuild: (prev, next) => next != null,
      builder: (context, trivia, _) {
        return Container(
          margin: const EdgeInsets.only(bottom: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (trivia.media != null && trivia.media.isNotEmpty)
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: buildActionButton(trivia, context),
                ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      trivia.title.toUpperCase(),
                      style: TextStyle(
                        fontSize: 13,
                        color: Color(0xFF7B7B7B),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Text(
                      trivia.text,
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFFC4C4C4),
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
