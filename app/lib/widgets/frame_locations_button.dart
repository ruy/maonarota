import 'package:flutter/material.dart';

class FrameLocationsButton extends StatelessWidget {
  final Function onTap;

  FrameLocationsButton({
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 5,
      onPressed: onTap,
      child: Container(
        width: 40,
        height: 40,
        child: Icon(
          Icons.zoom_out_map,
          size: 20,
          color: Colors.white,
        ),
      ),
      color: Theme.of(context).accentColor,
      shape: CircleBorder(
        side: BorderSide(
          color: Colors.white,
          width: 1,
        ),
      ),
    );
  }
}
