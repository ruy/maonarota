import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum MarkerType {
  CurrentMarker,
  NonCurrentMarker,
  User,
}

class CustomMarker extends Marker {
  final MarkerType type;
  final LatLng coordinates;
  final BuildContext context;
  final Function onTap;
  final int index;

  static Widget _markerBuilder(
    int index,
    BuildContext context,
    Function onTap, {
    bool isCurrent = false,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: [
          FaIcon(
            FontAwesomeIcons.mapMarker,
            color: isCurrent
                ? Theme.of(context).accentColor
                : Theme.of(context).primaryColor,
            size: isCurrent ? 50 : 40,
          ),
          Container(
            margin: EdgeInsets.only(
              left: isCurrent ? 13.0 : 10.0,
              top: isCurrent ? 7 : 5,
            ),
            child: Text(
              index.toString(),
              style: TextStyle(
                color: Colors.white,
                fontSize: isCurrent ? 18 : 16,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  static Widget _typeToMarker(
      MarkerType type, BuildContext context, Function onTap, int index) {
    switch (type) {
      case MarkerType.CurrentMarker:
        return _markerBuilder(index, context, onTap, isCurrent: true);
      case MarkerType.User:
        return Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(color: Colors.black54, blurRadius: 8),
            ],
          ),
          child: CircleAvatar(
            backgroundColor: Colors.white,
            child: CircleAvatar(
              radius: 10,
              backgroundColor: Colors.lightBlue,
            ),
          ),
        );
      case MarkerType.NonCurrentMarker:
      default:
        return _markerBuilder(index, context, onTap);
    }
  }

  static void doNothing() {}

  @override
  CustomMarker({
    this.type = MarkerType.NonCurrentMarker,
    @required this.coordinates,
    @required this.context,
    Function onTap,
    this.index = 0,
  })  : onTap = onTap ?? doNothing,
        super(
          width: 25.0,
          height: 25.0,
          point: coordinates,
          builder: (ctx) => _typeToMarker(
            type,
            context,
            onTap,
            index,
          ),
        );
}
