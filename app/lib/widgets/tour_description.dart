import 'dart:io';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';

import 'package:mao_na_rota/model/tour.dart' as model;
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:mao_na_rota/views/tour/tour.dart';
import 'package:mao_na_rota/widgets/audio_player.dart';
import 'package:mao_na_rota/widgets/description_section.dart';
import 'package:mao_na_rota/widgets/detail_section_title.dart';
import 'package:mao_na_rota/widgets/guide_section.dart';
import 'package:mao_na_rota/widgets/title_header.dart';
import 'package:mao_na_rota/widgets/tour_info.dart';
import 'package:mao_na_rota/widgets/tour_point_list_item.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class TourDescription extends StatelessWidget {
  final String tourId;

  TourDescription({
    @required this.tourId,
  });

  void _onTap(context) {
    Navigator.of(context).pushNamed(Tour.routeName, arguments: tourId);
  }

  void onSave(BuildContext context, model.Tour tour) async {
    PermissionStatus permission = await Permission.storage.request();

    if (permission.isGranted) {
      Provider.of<TourListModel>(context, listen: false).downloadTour(tour);
    } else {
      if (permission.isPermanentlyDenied ||
          (permission.isDenied && Platform.isIOS)) {
        openAppSettings();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, model.Tour>(
      selector: (_, model) =>
          model.tours.firstWhere((tour) => tour.id == tourId),
      shouldRebuild: (prev, next) => next != null,
      builder: (context, tour, _) {
        final _pointList = ListView.builder(
          itemBuilder: (context, index) {
            return TourPointListItem(
              point: tour.points[index],
            );
          },
          itemCount: tour.points.length,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          padding: const EdgeInsets.all(0),
        );

        return ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8.0),
            topRight: Radius.circular(8.0),
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              boxShadow: [
                BoxShadow(color: Colors.black, blurRadius: 4),
              ],
            ),
            height: MediaQuery.of(context).size.height - 150,
            width: double.infinity,
            child: Column(
              children: [
                Expanded(
                  child: CustomScrollView(
                    slivers: [
                      SliverAppBar(
                        backgroundColor: Theme.of(context).primaryColor,
                        elevation: 5,
                        automaticallyImplyLeading: false,
                        floating: true,
                        pinned: true,
                        snap: false,
                        expandedHeight: Platform.isIOS ? 135 : 155,
                        centerTitle: false,
                        bottom: PreferredSize(
                          preferredSize:
                              Size.fromHeight(Platform.isIOS ? 55 : 70),
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 20,
                              right: 20,
                              bottom: 15,
                              top: Platform.isIOS ? 5 : 15,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    TourInfo(
                                      tour: tour,
                                    ),
                                    GestureDetector(
                                      onTap: () => _onTap(context),
                                      child: Container(
                                        height: 35,
                                        width: 100,
                                        margin: const EdgeInsets.all(0),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          color: Theme.of(context).primaryColor,
                                          border: Border.all(
                                            color:
                                                Theme.of(context).accentColor,
                                            width: 1.0,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            'INICIAR',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        flexibleSpace: FlexibleSpaceBar(
                          background: Container(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 20,
                              bottom: 30,
                              top: 20,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                TitleHeader(
                                  titleInfo: tour.titleInfo,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Salvar no dispositivo',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Color(0xFF7B7B7B),
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    Container(
                                      width: 60,
                                      height: 15,
                                      child: Switch(
                                        value: tour.shouldBeSaved,
                                        onChanged: (value) {
                                          if (!value) {
                                            Provider.of<TourListModel>(context,
                                                    listen: false)
                                                .removeDownloadedTour(tour);
                                          } else {
                                            onSave(context, tour);
                                          }
                                        },
                                        activeColor:
                                            Theme.of(context).accentColor,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    if (tour.localFiles.isNotEmpty &&
                                        !tour.isSaved)
                                      Row(
                                        children: <Widget>[
                                          SpinKitFadingFour(
                                            color: Color(0xFF436732),
                                            size: 13,
                                          ),
                                          SizedBox(
                                            width: 12,
                                          ),
                                          Text(
                                            'Salvando... ${tour.savingProgress}%',
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Color(0xFF436732),
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ],
                                      ),
                                    if (tour.isSaved)
                                      Text(
                                        'Salvo',
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Color(0xFF436732),
                                          fontWeight: FontWeight.normal,
                                        ),
                                      ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate(
                          [
                            Container(
                              padding: const EdgeInsets.only(
                                left: 20,
                                right: 20,
                                bottom: 15,
                                top: 15,
                              ),
                              color: Theme.of(context).primaryColor,
                              child: Column(
                                children: <Widget>[
                                  DescriptionSection(
                                    text: tour.description,
                                    audioDescriptionUrl: tour.audio,
                                  ),
                                  if (tour.type == model.TourType.GuidedTour)
                                    Container(
                                      margin: const EdgeInsets.only(
                                        top: 20.0,
                                      ),
                                      child: GuideSection(
                                        name: tour.guideName,
                                      ),
                                    ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  DetalSectionTitle(title: 'PARADAS'),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  _pointList,
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                AudioPlayer(),
              ],
            ),
          ),
        );
      },
    );
  }
}
