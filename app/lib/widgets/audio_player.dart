import 'dart:math';

import 'package:flutter/material.dart';
import 'package:audio_service/audio_service.dart';
import 'package:mao_na_rota/widgets/blink_button.dart';
import 'package:rxdart/rxdart.dart';

class ScreenState {
  final MediaItem mediaItem;
  final PlaybackState playbackState;

  ScreenState(this.mediaItem, this.playbackState);
}

class AudioPlayer extends StatefulWidget {
  @override
  _AudioPlayerState createState() => _AudioPlayerState();
}

class _AudioPlayerState extends State<AudioPlayer> {
  void forward30(int currentPosition) => AudioService.fastForward();
  void rewind30(int currentPosition) => AudioService.rewind();

  static void play() => AudioService.play();
  static void pause() => AudioService.pause();
  // static void stop() => AudioService.stop();

  Stream<ScreenState> get _screenStateStream =>
      Rx.combineLatest3<MediaItem, PlaybackState, double, ScreenState>(
          AudioService.currentMediaItemStream,
          AudioService.playbackStateStream,
          Stream.periodic(Duration(milliseconds: 100)),
          (mediaItem, playbackState, _) =>
              ScreenState(mediaItem, playbackState));

  // Adapted from https://stackoverflow.com/questions/54775097/formatting-a-duration-like-hhmmss
  String _formatDuration(int seconds) {
    var _duration = Duration(seconds: seconds);
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(_duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(_duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;

    return StreamBuilder<ScreenState>(
      stream: _screenStateStream,
      builder: (_, snapshot) {
        final screenState = snapshot.data;
        final mediaItem = screenState?.mediaItem;
        final state = screenState?.playbackState;
        final processingState =
            state?.processingState ?? AudioProcessingState.none;
        final playing = state?.playing ?? false;
        int position = state?.currentPosition?.inSeconds ?? 0;
        int duration = mediaItem?.duration?.inSeconds ?? 0;

        double _percentageDone =
            duration != null ? max(0.0, position) / duration : 0;

        if (state == null ||
            mediaItem == null ||
            state.processingState == AudioProcessingState.none ||
            state.processingState == AudioProcessingState.stopped) {
          return Container();
        }

        return Container(
          height: 55,
          width: double.infinity,
          color: Theme.of(context).primaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    height: 1,
                    color: Color(0xFF7B7B7B),
                    width: _width,
                  ),
                  if (_percentageDone > 0.0)
                    Container(
                      height: 1,
                      width: _width * _percentageDone,
                      color: Theme.of(context).accentColor,
                    ),
                ],
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 18,
                        ),
                        BlinkButton(
                          disabled:
                              processingState == AudioProcessingState.none,
                          icon: Icons.replay_30,
                          onTap: () => rewind30(position),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        BlinkButton(
                          disabled:
                              processingState == AudioProcessingState.none,
                          icon: (playing ?? false)
                              ? Icons.pause
                              : Icons.play_arrow,
                          onTap: (playing ?? false) ? pause : play,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        BlinkButton(
                          disabled:
                              processingState == AudioProcessingState.none,
                          icon: Icons.forward_30,
                          onTap: () => forward30(position),
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        right: 25,
                      ),
                      child: Text(
                        "${_formatDuration(position)} / ${_formatDuration(duration)}",
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
