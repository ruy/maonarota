import 'package:flutter/material.dart';

class OwnLocationButton extends StatelessWidget {
  final Function onTap;

  OwnLocationButton({
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 5,
      onPressed: onTap,
      child: Container(
        width: 40,
        height: 40,
        child: Icon(
          Icons.my_location,
          size: 20,
          color: Colors.white,
        ),
      ),
      color: Theme.of(context).accentColor,
      shape: CircleBorder(
        side: BorderSide(
          color: Colors.white,
          width: 1,
        ),
      ),
    );
  }
}
