import 'package:flutter/material.dart';
import 'package:mao_na_rota/widgets/detail_section_text.dart';

class GuideSection extends StatelessWidget {
  final String name;

  GuideSection({
    @required this.name,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'GUIA',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 17,
            fontWeight: FontWeight.w500,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        DetailSectionText(
          text: name,
          expandable: false,
        ),
      ],
    );
  }
}
