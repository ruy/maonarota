import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mao_na_rota/model/tour_point.dart';

import 'package:mao_na_rota/views/tour/tour_point_detail.dart';
import 'package:mao_na_rota/widgets/detail_section_text.dart';

class TourPointListCard extends StatelessWidget {
  final TourPoint point;

  TourPointListCard({
    @required this.point,
  });

  void _onTap(BuildContext context) {
    Navigator.of(context)
        .pushNamed(TourPointDetail.routeName, arguments: point.id);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
      child: GestureDetector(
        onTap: () => _onTap(context),
        child: Container(
          height: 50,
          margin: const EdgeInsets.only(
            right: 20,
          ),
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            border: Border.all(
              color: Color(0xFF303030),
              width: 1.0,
            ),
          ),
          child: Container(
            padding: const EdgeInsets.all(5),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 3,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 1,
                            child: Container(
                              margin: const EdgeInsets.only(bottom: 2),
                              child: FaIcon(
                                FontAwesomeIcons.mapMarkerAlt,
                                color: Theme.of(context).accentColor,
                                size: 20,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Flexible(
                            flex: 3,
                            child: Text(
                              point.titleInfo.title.toUpperCase(),
                              style: TextStyle(
                                color: Color(0xFFC4C4C4),
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Text(
                        'MAIS',
                        style: TextStyle(
                          color: Color(0xFF7B7B7B),
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Expanded(
                  child: DetailSectionText(
                    fontSize: 14,
                    expandable: false,
                    text: point.description,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
