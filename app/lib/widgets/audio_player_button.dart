import 'package:flutter/material.dart';
import 'package:mao_na_rota/audio/audio_service.dart';
import 'package:mao_na_rota/widgets/action_button.dart';
import 'package:audio_service/audio_service.dart';

void audioPlayerTaskEntrypoint() async {
  AudioServiceBackground.run(() => AudioServiceTask());
}

class AudioPlayerButton extends StatefulWidget {
  final String url;
  final bool enlarged;
  final Function onTap;

  AudioPlayerButton({
    @required this.url,
    this.onTap,
    this.enlarged = false,
  });

  @override
  _AudioPlayerButtonState createState() => _AudioPlayerButtonState();
}

class _AudioPlayerButtonState extends State<AudioPlayerButton> {
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  void _onTap() async {
    PlaybackState _state = AudioService.playbackState;

    if (_state == null || _state.processingState == AudioProcessingState.none) {
      setState(() {
        isLoading = true;
      });

      await AudioService.start(
        androidStopForegroundOnPause: true,
        rewindInterval: Duration(seconds: 30),
        fastForwardInterval: Duration(seconds: 30),
        backgroundTaskEntrypoint: audioPlayerTaskEntrypoint,
        androidNotificationChannelName: 'Mão na Rota',
        androidNotificationColor: 0xFFDC5586,
        androidNotificationIcon: 'mipmap/ic_launcher',
      );
    }

    setState(() {
      isLoading = false;
    });

    if (widget.onTap != null) {
      widget.onTap();
    }

    AudioService.customAction('set', widget.url);
  }

  @override
  Widget build(BuildContext context) {
    return ActionButton(
      icon: Icons.volume_up,
      loading: isLoading,
      onTap: _onTap,
      enlarged: widget.enlarged,
      statusStream: AudioService.currentMediaItemStream
          .map((mediaItem) => mediaItem?.id == widget.url),
      startActive: AudioService.currentMediaItem?.id == widget.url,
    );
  }
}
