import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ActionButton extends StatefulWidget {
  final IconData icon;
  final Function onTap;
  final bool enlarged;
  final bool startActive;
  final bool shouldBlink;
  final Stream<bool> statusStream;
  final bool loading;

  ActionButton({
    @required this.icon,
    @required this.onTap,
    this.enlarged = false,
    this.startActive = false,
    this.statusStream,
    this.loading = false,
  }) : this.shouldBlink = false;

  ActionButton.blink({
    @required this.icon,
    @required this.onTap,
    this.enlarged = false,
    this.loading = false,
  })  : this.startActive = false,
        this.shouldBlink = true,
        this.statusStream = null;

  @override
  _ActionButtonState createState() => _ActionButtonState();
}

class _ActionButtonState extends State<ActionButton> {
  var _isActive = false;

  void blink() {
    if (_isActive == true) {
      Future.delayed(
        const Duration(milliseconds: 200),
        () {
          if (mounted) {
            setState(() {
              _isActive = false;
            });
          }
        },
      );
    }
  }

  @override
  void initState() {
    if (widget.shouldBlink) {
      _isActive = false;
    } else {
      _isActive = widget.startActive;
    }

    if (widget.statusStream != null) {
      widget.statusStream.listen(
        (activeStatus) {
          if (mounted) {
            setState(() {
              _isActive = activeStatus;
            });
          }
        },
      );
    }

    super.initState();
  }

  void _onTap() {
    if (!_isActive) {
      widget.onTap();
    }

    if (mounted) {
      setState(() {
        _isActive = true;
      });
    }

    if (widget.shouldBlink) {
      blink();
    }
  }

  Color _statusToColor() {
    // if (widget.enlarged) {
    //   if (_isActive) {
    // return Theme.of(context).accentColor;
    // }
    // return Colors.white;
    // }

    if (_isActive) {
      return Theme.of(context).accentColor;
    } else {
      return Color(0xFF303030);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.loading ? () {} : _onTap,
      child: Container(
        height: widget.enlarged ? 45 : 35,
        width: widget.enlarged ? 45 : 35,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Theme.of(context).primaryColor,
          border: Border.all(
            color: _statusToColor(),
            width: 1.0,
          ),
        ),
        child: Center(
          child: widget.loading
              ? SpinKitFadingFour(
                  size: 16,
                  color: Theme.of(context).accentColor,
                )
              : Icon(
                  widget.icon,
                  color: Theme.of(context).accentColor,
                  size: 16,
                ),
        ),
      ),
    );
  }
}
