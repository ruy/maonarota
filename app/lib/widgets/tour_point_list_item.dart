import 'dart:io';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/views/tour/tour_point_detail.dart';

class TourPointListItem extends StatelessWidget {
  final TourPoint point;

  TourPointListItem({
    @required this.point,
  });

  @override
  Widget build(BuildContext context) {
    bool displayPlaceholder = point.pictures == null ||
        point.pictures.isEmpty ||
        point.pictures.first == null ||
        point.pictures.first.isEmpty;

    return OpenContainer(
      closedElevation: 0,
      closedColor: Theme.of(context).primaryColor,
      openColor: Theme.of(context).primaryColor,
      transitionType: ContainerTransitionType.fade,
      transitionDuration: const Duration(milliseconds: 700),
      openBuilder: (context, action) {
        return TourPointDetail(
          pointId: point.id,
        );
      },
      closedBuilder: (context, action) {
        return Container(
          height: 50,
          margin: const EdgeInsets.only(bottom: 5, right: 5),
          color: Theme.of(context).primaryColor,
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                displayPlaceholder
                    ? CircleAvatar(
                        radius: 18,
                        child: Image.asset(
                          'assets/images/logo_small.png',
                          width: 30,
                          height: 30,
                        ),
                        backgroundColor: Colors.grey[800],
                      )
                    : CircleAvatar(
                        radius: 18,
                        backgroundColor: Colors.grey[800],
                        backgroundImage: point.isSaved
                            ? FileImage(File(point.pictures.first))
                            : NetworkImage(point.pictures.first),
                      ),
                SizedBox(
                  width: 17,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        point.titleInfo.title,
                        style: TextStyle(
                          color: Color(0xFFC4C4C4),
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        point.description,
                        style: TextStyle(
                          color: Color(0xFF7B7B7B),
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
