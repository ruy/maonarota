import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/title_info.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/widgets/audio_player_button.dart';

class TitleHeader extends StatefulWidget {
  final TitleInfo titleInfo;
  final String audioDescriptionUrl;

  TitleHeader({
    @required this.titleInfo,
    this.audioDescriptionUrl,
  });

  @override
  _TitleHeaderState createState() => _TitleHeaderState();
}

class _TitleHeaderState extends State<TitleHeader> {
  String _typeText() {
    switch (widget.titleInfo.type) {
      case TourType.GuidedTour:
        return 'ROTA GUIADA';
      case TourType.AutoGuidedTour:
        return 'ROTA AUTO GUIADA';
      case TourType.TourPoint:
      default:
        return 'PARADA';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.audioDescriptionUrl != null &&
            widget.audioDescriptionUrl.isNotEmpty)
          Container(
            margin: const EdgeInsets.only(right: 15),
            child: AudioPlayerButton(
              url: widget.audioDescriptionUrl,
              enlarged: true,
            ),
          ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    _typeText(),
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              Text(
                widget.titleInfo.title.toUpperCase(),
                style: TextStyle(
                  color: Color(0xFFC4C4C4),
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              )
            ],
          ),
        )
      ],
    );
  }
}
