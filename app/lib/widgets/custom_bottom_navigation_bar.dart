import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

class NavigationBarItem {
  IconData icon;
  String title;

  NavigationBarItem({
    this.icon,
    this.title,
  });
}

class CustomBottomNavigationBarItem extends StatelessWidget {
  final IconData icon;
  final Function onTap;
  final Color color;

  CustomBottomNavigationBarItem({
    @required this.icon,
    @required this.onTap,
    @required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        icon,
        size: 20,
        color: color,
      ),
      onPressed: onTap,
    );
  }
}

class CustomBottomNavigationBar extends StatefulWidget {
  final Function onTapItem;

  CustomBottomNavigationBar({
    @required this.onTapItem,
  });

  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  var _currentIndex = 0;

  void _onTapIcon(int index) {
    setState(() {
      _currentIndex = index;
    });
    widget.onTapItem(index);
  }

  @override
  Widget build(BuildContext context) {
    var _selectedItemColor = Theme.of(context).accentColor;
    var _unselectedItemColor = Color(0xFF666666);

    return CurvedNavigationBar(
      height: 50,
      color: Colors.grey[850],
      backgroundColor: Theme.of(context).primaryColor,
      buttonBackgroundColor: _selectedItemColor,
      items: <Widget>[
        Icon(Icons.account_balance,
            color: _currentIndex == 0
                ? Theme.of(context).primaryColor
                : _unselectedItemColor),
        Icon(Icons.calendar_today,
            color: _currentIndex == 1
                ? Theme.of(context).primaryColor
                : _unselectedItemColor),
        Icon(Icons.info,
            color: _currentIndex == 2
                ? Theme.of(context).primaryColor
                : _unselectedItemColor),
      ],
      onTap: (index) {
        _onTapIcon(index);
      },
    );
  }
}
