import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/widgets/audio_player.dart';

import 'package:mao_na_rota/widgets/description_section.dart';
import 'package:mao_na_rota/widgets/detail_section_title.dart';
import 'package:mao_na_rota/widgets/title_header.dart';
import 'package:mao_na_rota/widgets/tour_point_trivia_card.dart';
import 'package:provider/provider.dart';

class TourPointDescription extends StatelessWidget {
  final String pointId;

  TourPointDescription({
    @required this.pointId,
  });

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, Tour>(
        selector: (_, model) => model.tourByTourPointId(pointId),
        shouldRebuild: (prev, next) => next != null,
        builder: (context, tour, _) {
          TourPoint point =
              tour.points.firstWhere((point) => point.id == pointId);

          final _triviaList = ListView.builder(
            itemBuilder: (context, index) {
              return TourPointTriviaCard(
                point.trivia[index].id,
              );
            },
            itemCount: point.trivia.length,
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: const EdgeInsets.all(0),
          );

          return ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0),
              topRight: Radius.circular(10.0),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(color: Colors.black, blurRadius: 4),
                ],
              ),
              height: MediaQuery.of(context).size.height - 150,
              width: double.infinity,
              child: Column(
                children: [
                  Expanded(
                    child: CustomScrollView(
                      slivers: [
                        SliverAppBar(
                          backgroundColor: Theme.of(context).primaryColor,
                          elevation: 5,
                          automaticallyImplyLeading: false,
                          floating: true,
                          pinned: true,
                          snap: false,
                          expandedHeight: Platform.isIOS ? 55 : 77,
                          centerTitle: false,
                          titleSpacing: 0,
                          bottom: PreferredSize(
                            preferredSize:
                                Size.fromHeight(Platform.isIOS ? 63 : 77),
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 20,
                                right: 20,
                                bottom: Platform.isIOS ? 15 : 10,
                                top: Platform.isIOS ? 1 : 10,
                              ),
                              child: TitleHeader(
                                titleInfo: point.titleInfo,
                              ),
                            ),
                          ),
                        ),
                        SliverList(
                          delegate: SliverChildListDelegate(
                            [
                              Container(
                                padding: const EdgeInsets.only(
                                  left: 20,
                                  right: 20,
                                  bottom: 15,
                                ),
                                color: Theme.of(context).primaryColor,
                                child: Column(
                                  children: <Widget>[
                                    DescriptionSection(
                                      text: point.description,
                                      audioDescriptionUrl: point.audio,
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    DetalSectionTitle(title: 'SAIBA MAIS'),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    _triviaList,
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  AudioPlayer(),
                ],
              ),
            ),
          );
        });
  }
}
