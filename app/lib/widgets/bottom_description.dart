import 'package:flutter/material.dart';

class BottomDescription extends StatelessWidget {
  final String text;

  BottomDescription({
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(15.0),
        topRight: Radius.circular(15.0),
      ),
      child: Container(
        padding: const EdgeInsets.only(
          top: 15,
          left: 20,
          right: 20,
          bottom: 15,
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(color: Colors.black38, blurRadius: 4),
          ],
        ),
        height: 85,
        child: Text(
          text,
          style: TextStyle(
            fontSize: 13,
            color: Color(0xFF7B7B7B),
          ),
        ),
      ),
    );
  }
}
