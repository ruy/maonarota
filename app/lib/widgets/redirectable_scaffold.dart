import 'dart:async';

import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mao_na_rota/views/locale-permission/locale_permission.dart';
import 'package:mao_na_rota/services/location.dart';

class RedirectableScaffold extends StatefulWidget {
  final PreferredSizeWidget appBar;
  final Widget body;
  final Widget bottomNavigationBar;

  const RedirectableScaffold({
    Key key,
    this.appBar,
    @required this.body,
    this.bottomNavigationBar,
  }) : super(key: key);

  @override
  _RedirectableScaffoldState createState() => _RedirectableScaffoldState();
}

class _RedirectableScaffoldState extends State<RedirectableScaffold> {
  Location location = Location();
  bool _redirected = false;
  bool _loading = true;
  StreamSubscription<bool> locationServiceCurrentStatus;

  @override
  void initState() {
    super.initState();
    Stream<bool> locationServiceStream =
        locationServiceStatus(const Duration(seconds: 5));

    locationServiceCurrentStatus =
        locationServiceStream.listen((isEnabled) async {
      if (!isEnabled && !_redirected) {
        await redirect();
      } else if (isEnabled && mounted) {
        setState(() {
          _loading = false;
        });
      }
    });

    // WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    locationServiceCurrentStatus.cancel();
    super.dispose();
  }

  Future<void> redirect() async {
    if (_redirected) {
      return;
    }

    _redirected = true;

    await Navigator.of(context).pushNamed(LocalePermission.routeName);

    setState(() {
      _loading = false;
      _redirected = false;
    });
  }

  Widget loadingScreen() {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Center(
        child: SpinKitFadingFour(
          color: Theme.of(context).accentColor,
          size: 40.0,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.appBar,
      body: _loading ? loadingScreen() : widget.body,
      bottomNavigationBar: _loading ? null : widget.bottomNavigationBar,
    );
  }
}
