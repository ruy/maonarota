import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/views/tour/tour_detail.dart';

import 'caroulsel_item_description.dart';

class CarouselItem extends StatelessWidget {
  final Tour tour;

  const CarouselItem({
    @required this.tour,
  });

  void _onTap(BuildContext context) {
    Navigator.of(context).pushNamed(TourDetail.routeName, arguments: tour.id);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onTap(context),
      child: Stack(
        children: <Widget>[
          (tour.picture == null || tour.picture.isEmpty)
              ? Container(
                  height: double.infinity,
                  width: double.infinity,
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: Image.asset(
                      'assets/images/logo_small.png',
                      width: 150,
                      height: 150,
                    ),
                  ),
                )
              : ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.bottomRight,
                      end: Alignment.topLeft,
                      colors: [Colors.grey[900], Colors.transparent],
                    ).createShader(
                        Rect.fromLTRB(0, 0, rect.width, rect.height));
                  },
                  blendMode: BlendMode.darken,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[800],
                      image: DecorationImage(
                        image: tour.isSaved
                            ? FileImage(File(tour.picture))
                            : NetworkImage(tour.picture),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: CarouselItemDescription(
                    tour: tour,
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              // color: Colors.yellow,
              width: double.infinity,
              padding: const EdgeInsets.all(
                20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/logo_white.png',
                    width: 30,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
