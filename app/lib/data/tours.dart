import 'package:mao_na_rota/model/title_info.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/model/trivia.dart';

List<Tour> getAutoGuidedTours() {
  return [
    Tour(
      id: '7',
      points: [
        TourPoint(
          id: '99',
          latitude: -22.9036593,
          longitude: -47.061378,
          description:
              'A primeira parada é onde tudo começou, a casa em que nasceu Carlos Gomes, localizada na Rua Regente Feijó, número 1251. Atualmente, é a localização de um edifício, porém, existe uma placa de identificação e belas ilustrações em homenagem ao compositor, com pinturas na fachada relembrando a casa que um dia esteve ali Na verdade, o nome original da rua onde nasceu é Rua da Matriz Nova, número 50. Mas com o crescimento da cidade de Campinas, o nome foi alterado para Regente Feijó.',
          pictureUrls: [
            'https://carlosgomes.campinas.sp.gov.br/sites/carlosgomes.campinas.sp.gov.br/files/250615_casa_onde_morou_carlos_gomes_rua_regente_feijo_foto_carlos_bassan_011.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/j3kxmjoxweyv9a4/p1.mp3?',
          titleInfo: TitleInfo(
            title: 'Onde nasceu Carlos Gomes',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Vida',
              id: '0',
              text:
                  '“Carlos Gomes transcendeu a música. Ele é conhecido como o maior compositor do continente norte-americano e, hoje, é um patrimônio cultural da humanidade. Mas o seu maior legado foi o fator de unidade e pacificação nacional da nação brasileira. Quando Carlos Gomes estava doente, em Belém, o Brasil estava muito conturbado: Guerra de Canudos, Revolta da Armada, Revolução Federalista, os estados do Sul (Rio Grande do Sul, Santa Catarina e Paraná) querendo se desmembrar do Brasil. Mas quando os brasileiros começaram a tomar conhecimento da doença de Carlos Gomes, seu sacrifício, seu sofrimento e seu martírio em Belém do Pará, a população foi se comovendo e foi deixando a política de lado. E, com isso, permitiu que a nação se pacificasse e Campos Salles fosse eleito”.',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/sc5e5s9tnynpniy/p1t1.mp3',
            ),
            Trivia(
              title: 'Localização',
              id: '2',
              text: 'Rua Regente Feijó, número 1251',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/8p1dpyuqofhkmsy/p1t.m4a',
            ),
          ],
        ),
        TourPoint(
          id: '100',
          latitude: -22.9036593,
          longitude: -47.061378,
          description:
              'Próximo à casa onde nasceu, encontra-se o Museu Carlos Gomes, localizado no mesmo prédio que o Museu Campos Salles e a Pinacoteca do Centro de Ciências, Letras e Artes. Fundado em 1956, é o único museu com acervos exclusivamente de Carlos Gomes e com exposição permanente de documentos, peças e objetos pessoais do maestro. Entre os artigos expostos estão: a colher usada por Santos Dumont na colocação da pedra fundamental do monumento-túmulo do maestro; o piano de cauda; as batutas; e uma coleção de partituras originais de três de suas grandes composições: O Guarani, O Escravo e Maria Tudor.',
          pictureUrls: [
            'https://live.staticflickr.com/1190/5104462931_ec7ac1e1cc_b.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/g8m4xiy2syl0muj/p2.mp3',
          titleInfo: TitleInfo(
            title: 'Museu Carlos Gomes',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Visite o museu!',
              id: '10',
              text:
                  'Rua Bernardino de Campos, 989, Centro. Horário de funcionamento: Segunda a sexta, das 9h às 12h e das 14h às 17h30. Visitas monitoradas: Segunda a sexta, das 14h às 17h30. Tel.: (19) 3231-256. E-mail: ccla@ccla.org.br. Site: ccla.org.br/museu-carlos-gomes/',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/fp0qwkekj43h9u6/p2t.m4a',
            ),
          ],
        ),
        TourPoint(
          id: '101',
          latitude: -22.9029,
          longitude: -47.0596,
          description:
              'Localizado na Praça Bento Quirino, centro da cidade de Campinas, o monumento túmulo não passa despercebido. A obra do escultor Rodolfo Bernardelli é estruturada por uma pedra fundamental em granito, que foi colocada no local onde se encontra até hoje pelo pai da aviação, Santos Dumont. No granito, encontram-se duas estátuas em bronze: no topo, a de Carlos Gomes em posição de maestro, e a seus pés, a estátua de sua prima, Maria Monteiro - cantora lírica campineira, que teve seu trabalho mundialmente reconhecido. Ao redor da criação principal, há um pequeno gramado que, em forma de circunferência, guarda os nomes em bronze das óperas mais famosas do compositor. Carlos Gomes faleceu em Belém do Pará em 16 de setembro de 1896 e foi enterrado em seu monumento túmulo em 2 de julho de 1905. Assim, até que a obra de Bernardelli estivesse pronta, o maestro foi sepultado no mausoléu da Família Ferreira Penteado, no Cemitério da Saudade, conhecido na época como Cemitério do Fundão.',
          pictureUrls: [
            'https://portalcbncampinas.com.br/wp-content/uploads/2012/09/tumulo_carlos_gomes.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/lgsb0gm0tfltwax/p3.mp3',
          titleInfo: TitleInfo(
            title: 'Monumento Túmulo Carlos Gomes',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Localização',
              id: '11',
              text: 'Praça Bento Quirino, Avenida Dr Thomaz Alves, Centro.',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/gv4nwvk1944hk6w/p3t.m4a?dl=0',
            ),
          ],
        ),
        TourPoint(
          id: '102',
          latitude: -22.9015,
          longitude: -47.0603,
          description:
              'Em frente ao Monumento-túmulo, encontra-se a Basílica Nossa Senhora do Carmo, conhecida antigamente como a Igreja Matriz de Campinas. Apesar de não ser um local óbvio de referência ao grande maestro, foi onde Carlos Gomes foi batizado.',
          pictureUrls: [
            'https://arquidiocesecampinas.com/wp-content/uploads/2012/10/46-Par-N-S-Carmo-Campinas.jpg',
            'https://media-cdn.tripadvisor.com/media/photo-s/0c/ac/4c/83/uma-paz-incrivel-essa.jpg'
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/43pwnru2tnl88sv/p4.m4a',
          titleInfo: TitleInfo(
            title: 'Basílica Nossa Senhora do Carmo',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Localização',
              id: '12',
              text: 'Rua Barão de Jaguara, s/n - Centro, Campinas – SP',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/kcllhn3559d8dth/p4t.m4a',
            ),
          ],
        ),
        TourPoint(
          id: '104',
          latitude: -22.9073356,
          longitude: -47.0575739,
          description:
              'O Tonico’s Boteco é o local ideal para a pausa para o almoço durante o roteiro. O nome do estabelecimento é uma homenagem ao maestro, que tinha o apelido de Tonico. Instalado dentro de um casarão centenário, possui como decoração caricaturas de Carlos Gomes, posters de suas obras e fotos antigas da cidade de Campinas. Além de ter um cardápio diversificado, o boteco já conquistou alguns prémios da revista Veja Campinas, entre eles o de “Melhor Música ao Vivo” por 3 anos, além de ter sido indicado outras 8 vezes.',
          pictureUrls: [
            'https://www.baressp.com.br/bares/fotos2/Tonicos_02-min.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/eo64ihclvb6rjwp/p5.m4a',
          titleInfo: TitleInfo(
            title: 'Tonico’s Boteco',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Localização',
              id: '14',
              text:
                  'Rua Barão de Jaguara, 1373 – Centro, Campinas-SP. Horário de funcionamento: de segunda a sábado, das 11h às 01h30. Tel: (19) 3236.1664',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/8k7ndn16ln2frbx/p5t.m4a',
            ),
          ],
        ),
        TourPoint(
          id: '105',
          latitude: -22.9059,
          longitude: -47.0603,
          description:
              'Após o falecimento de Carlos Gomes, em Belém do Pará, a trajetória de viagem do corpo demorou 8 dias para chegar até Campinas. O caixão passou pelo Rio de Janeiro, por Santos, por São Paulo e por Jundiaí antes de chegar à cidade natal do maestro. De acordo com o historiador Jorge Alves de Lima, em Campinas, embora a cidade tivesse 15 mil habitantes, havia mais de 30 mil pessoas na rua esperando a chegada do corpo de Carlos Gomes, que foi diretamente para a Catedral Metropolitana de Campinas, onde foi velado por quatro dias. Antes de ser sepultado, foi celebrada uma missa solene em homenagem ao maestro com a presença do governador do estado de São Paulo, Campos Salles.',
          pictureUrls: [
            'https://s2.glbimg.com/yHHf_SDx8BSrHWx4R7wQ5HjRhO8=/0x0:1700x1065/1008x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2017/3/A/cJsVnYS2m268NdLtL09w/catedral.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/azwgjufl94ur0c0/p6.m4a',
          titleInfo: TitleInfo(
            title: 'Catedral Metropolitana de Campinas',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Localização',
              id: '15',
              text: 'Praça José Bonifácio, s/n - Centro, Campinas - SP',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/u0i461i5qmjajht/p6t.m4a',
            ),
          ],
        ),
        TourPoint(
          id: '106',
          latitude: -22.9117506,
          longitude: -47.0652065,
          description:
              'Destaque na produção cafeeira, em meados do século XIX, Campinas era uma cidade rica e de referência na produção cultural e tecnológica. Assim, em 1850, a pedido das elites, foi construído o primeiro teatro municipal de Campinas, o Teatro São Carlos, com 62 camarotes e 250 lugares na plateia. O teatro recebeu grandes artistas internacionais, como a famosa atriz francesa, Sarah Bernhardt. E teve a primeira orquestra regida pelo músico José Pedro de Sant&#39;Anna Gomes, irmão de Carlos Gomes. De acordo com o historiador Jorge Alves de Lima, em 1889, Carlos Gomes pensou em encenar em Campinas a ópera O Escravo, mas o teatro não comportava todo o cenário para a apresentação. Impulsionada pelo grande desenvolvimento econômico café, a cidade cresceu e o teatro se tornou pequeno para comportar os cidadãos que buscavam espetáculos musicais e teatrais. Assim, em 1922, o Teatro São Carlos foi demolido e, em 1930, foi inaugurado no mesmo local o pomposo Teatro Municipal de Campinas, com capacidade de 1300 lugares. A construção ostentava a riqueza da cidade com escadarias e adornos cobertos com pó de ouro e lustres de cristal trazidos da Boêmia, atual República Tcheca. Em homenagem ao grande maestro campineiro, o teatro passou a ser conhecido como Teatro Municipal Carlos Gomes, em 1959. Mas, em pouco tempo, em 1965, o teatro foi demolido apesar dos protestos populares contra a ação e sem explicações. Embora o prédio do Teatro Carlos Gomes não exista mais, em 2004, a Zeladoria do Centro e a Secretaria de Cultura, Esportes e Turismo (SMCET) anunciaram que abaixo do local onde era o teatro, ainda existem antigas fundações do edifício.',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/2/29/Teatro_Municipal_de_Campinas_%281%29.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/xzjn3wg5l3s1l10/p7.mp3',
          titleInfo: TitleInfo(
            title: 'Antigo Teatro Municipal de Campinas Carlos Gomes',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Localização',
              id: '16',
              text:
                  'Localizado entre as ruas Treze de Maio e Costa Aguiar, na Praça Rui Barbosa.',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/8e7w9vzm45q8x7v/p7t.m4a',
            ),
          ],
        ),
        TourPoint(
          id: '107',
          latitude: -22.9034,
          longitude: -47.0566,
          description:
              'Em 1870, foi inaugurada a praça que, apenas em 1880, seria conhecida como Praça Carlos Gomes, como uma homenagem ao grande maestro e compositor. Três anos mais tarde, 100 palmeiras imperiais foram adquiridas e plantadas ao redor da praça, como um símbolo do Império e da presença de D. Pedro II, que visitou a cidade em três ocasiões oficiais. Em 1911, foi a primeira praça de Campinas a obter calçamento em paralelepípedo e, dois anos mais tarde, um lago com queda d’agua. Em 1914, foi realizado o aplainamento e o ajardinamento, além da construção do coreto, que permitiu a realização de concertos populares, que acontecem até os dias atuais. Antes do ajardinamento, a praça era apenas um grande terreno cercado pelas palmeiras imperiais e servia como campo de futebol para jovens de classe média-baixa. Ali, 12 jovens passaram a se encontrar com frequência para praticar o esporte e, em abril de 1911, resolveram fundar o Guarani Futebol Clube. O nome do time foi uma homenagem à famosa ópera O Guarani, composta por Carlos Gomes, e as cores escolhidas para o clube, verde e branco, fazem referência ao verde da praça e à luz do dia que os iluminava. Atualmente, a Praça Carlos Gomes manteve o paisagismo com as palmeiras imperiais semelhante ao da época de sua implantação. E possui monumentos dedicados a Rui Barbosa e a Tomás Alves.',
          pictureUrls: [
            'https://www.z1portal.com.br/wp-content/uploads/2019/07/carlos3.jpg',
          ],
          audioDescriptionUrl:
              'https://dl.dropboxusercontent.com/s/u8l6vpdwo2epz8h/p8.mp3',
          titleInfo: TitleInfo(
            title: 'Praça Carlos Gomes',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Legado',
              id: '18',
              text:
                  '“A morte só acontece quando a pessoa morta é esquecida. Quando ela é relembrada, ela permanece viva. A arte transcende o tempo e o espaço, assim, Carlos Gomes é e sempre será eterno”.',
              type: 'audio',
              mediaUrl:
                  'https://dl.dropboxusercontent.com/s/v9162zzk81nabqp/p8t.m4a',
            ),
          ],
        ),
      ],
      description:
          'Carlos Gomes dispensa apresentações. Reconhecido nacional e internacionalmente como o maior compositor e maestro de óperas do Brasil e gênio musical das Américas, Antônio Carlos Gomes fez história ao ter suas obras encenadas nos grandes teatros da Europa e da América do Norte. O brasileiro deixou como legado os feitos de sua trajetória, como as famosas produções de O Guarani, Fosca (considerada por ele sua melhor obra) e Salvator Rosa. O maestro nasceu na antiga Vila de São Carlos (atual Campinas), mais especificamente em uma casa humilde na Rua da Matriz Nova número 50, dia 11 de julho de 1836. Nhô Tonico, como era conhecido, foi filho do músico Manuel José Gomes (Maneco Músico) e de Fabiana Jaguari Gomes, que, na infância de Carlos Gomes, foi assassinada tragicamente aos 28 anos. O historiador e membro da Academia Campinense de Letras, Jorge Alves de Lima, que estuda há 20 anos a história de Carlos Gomes e já escreveu quatro livros sobre a vida do grande maestro campineiro, conta que o pequeno Tonico teve sua iniciação na música ao integrar a Banda Musical de Campinas, cujo maestro era seu pai. “Em 1841, D. Pedro II veio a Campinas e, ao ouvir a Banda do Maneco Músico, impressionou-se com a performance do jovem, que tocava triângulo na época. Foi a primeira vez que o Imperador teve contato com o jovem músico”, explica o historiador. Mais velho, Carlos Gomes estudou canto, clarineta, piano e violino com o premiado violinista francês Paul Julien, além de aprofundar seus estudos de óperas. Mas, apesar de viver intensamente no mundo da música, o futuro maestro e compositor também trabalhava como alfaiate, costurando calças e paletós.\n Com grande dedicação, aos 15 anos, Carlos Gomes já se destacava como músico ao compor valsas, quadrilhas e polcas. Três anos mais tarde, realizou sua primeira missa, a Missa de São Sebastião, e, pouco tempo depois, compôs as famosas modinhas Quem Sabe? (inspirada e composta em sua cidade natal), Suspiro d’Alma e o Hino Acadêmico. Aos 25 anos, já estudando no Conservatório de Música do Rio de Janeiro, chamou novamente a atenção do Imperador D. Pedro II, com a encenação de sua primeira ópera, A Noite do Castelo, no Teatro Lyrico Fluminense. Três anos depois, após ter sua segunda ópera encenada, Joana de Flandres, o Governo Imperial concedeu a ele uma bolsa de estudos no Conservatório de Milão, na Itália. Em seu período de estudo, compôs diversas óperas, mas a grande projeção internacional que recebeu aconteceu após formado, em 1870, com a ópera O Guarani. A partir de então, suas composições foram encenadas em grandes teatros, aclamadas pelo público e enaltecidas pelo grande compositor Giuseppe Verdi. Carlos Gomes permaneceu na Itália até os 60 anos, país em que compôs a maior parte das suas óperas e viveu ao lado da esposa Adelina Péri, com quem teve cinco filhos. Durante este período, voltou algumas vezes para Campinas, mas o principal contato que teve com a sua cidade natal era por meio de cartas, principalmente para o seu irmão, o músico José Pedro de Sant&#39;Anna Gomes. “Nas cartas, ele contava do sucesso da Itália, o que estava acontecendo lá; relembrava com muita saudade Campinas, os amigos, sua trajetória infantil e juvenil. Carlos Gomes sentia muita falta da pátria, morando num país estranho, frio e com pessoas de grande sucesso na música”, explica o historiador Jorge Alves de Lima. O fim da vida do grande maestro foi marcado pela angústia e sofrimento. Apesar da grande projeção de suas obras, a situação financeira de Carlos Gomes era muito difícil, em Milão. Estava com um dos filhos doente, sem dinheiro e quase passando necessidades, quando recebeu um depósito de 20 contos de réis ouro do governo brasileiro intimando-o a compor o hino da República. “Em respeito e gratidão ao Imperador D. Pedro II, que estava nas últimas, em Paris, Carlos Gomes negou o pedido e devolveu todo o dinheiro, mesmo precisando”, conta o historiador. Aos 56 anos, enfrentou um câncer na língua e na garganta, além da morte prematura de três dos seus filhos. Tinha saudade da sua pátria, mas, por ser monarquista e próximo a D. Pedro II, o fim do Império e a Proclamação da República adiaram a sua volta ao Brasil até 1896, quando retornou às terras brasileiras para assumir a direção do Conservatório Musical de Belém do Pará. O maestro exerceu a função por cinco meses, até sua morte, em 16 de setembro de 1896, aos 60 anos. Seu corpo foi trazido para Campinas e tem como endereço do seu descanso eterno o monumento-túmulo na região central campineira. Após seu falecimento, foi homenageado em diversas cidades do Brasil com monumentos; nomes de ruas, teatros e escolas. Sua ópera, O Guarani, passou a ser tema do rádiojornal Voz do Brasil, desde 1935. E, em 1990, o maestro teve seu rosto estampado na nota de 5000 cruzeiros. Em Campinas, sua cidade natal, não poderia ser diferente. A memória de Carlos Gomes está guardada em diferentes partes da cidade, como o local da casa em que nasceu; o monumento-túmulo onde está enterrado; e o museu que leva seu nome e conta sobre a sua trajetória de vida. Existem, entretanto, outros pontos de referência que homenageiam o maestro e compositor, entre eles: a Rua e o Bairro Carlos Gomes, a Escola Municipal Carlos Gomes; e o Conservatório Carlos Gomes.',
      distance: 4000,
      duration: 110,
      pictureUrl:
          'https://digitaispuccampinas.files.wordpress.com/2015/09/monumento-carlos-gomes.jpg',
      audioDescriptionUrl:
          'https://dl.dropboxusercontent.com/s/vaf0d4ne4dhxh4x/td.mp3',
      titleInfo: TitleInfo(
        title: 'Carlos Gomes',
        type: TourType.AutoGuidedTour,
      ),
      type: TourType.AutoGuidedTour,
    ),
    Tour(
      id: '8',
      points: [
        TourPoint(
            id: '102',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: [
              Trivia(
                title: 'Curiosidade',
                id: '95',
                text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
                type: 'audio',
                mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
              ),
            ]),
        TourPoint(
          id: '103',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://files.freemusicarchive.org/storage-freemusicarchive-org/music/no_curator/Kevin_MacLeod/Classical_Sampler/Kevin_MacLeod_-_P_I_Tchaikovsky_Dance_of_the_Sugar_Plum_Fairy.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '99',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'Canção',
              mediaUrl:
                  'https://files.freemusicarchive.org/storage-freemusicarchive-org/music/no_curator/Kevin_MacLeod/Classical_Sampler/Kevin_MacLeod_-_P_I_Tchaikovsky_Dance_of_the_Sugar_Plum_Fairy.mp3',
            ),
            Trivia(
              title: 'Curiosidade',
              id: '97',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
      ],
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet risus vitae orci ultrices, suscipit rhoncus libero aliquet. Sed ante sem, tincidunt in magna vitae, ultrices porta enim. Nulla sodales dictum tortor tempor sollicitudin. Praesent consectetur mi eget mauris iaculis sollicitudin. Nunc facilisis, ex ornare auctor lobortis, ipsum turpis rhoncus neque, vel rhoncus orci lacus ac felis. In faucibus venenatis lacus eu dignissim. Etiam suscipit purus pretium lorem eleifend, viverra facilisis eros molestie. Maecenas non dolor luctus, blandit sem id, condimentum arcu. Proin vulputate scelerisque dui eget accumsan. Nunc sed orci malesuada, sodales sapien non, efficitur est. Aliquam sodales, erat non pulvinar egestas, odio sapien luctus lorem, ac viverra velit enim ut justo. Aenean fermentum est in condimentum dignissim.',
      distance: 10000,
      duration: 300,
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl:
          'https://files.freemusicarchive.org/storage-freemusicarchive-org/music/no_curator/Kevin_MacLeod/Classical_Sampler/Kevin_MacLeod_-_P_I_Tchaikovsky_Dance_of_the_Sugar_Plum_Fairy.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Auto Guiado 2',
        type: TourType.AutoGuidedTour,
      ),
      type: TourType.AutoGuidedTour,
    ),
  ];
}

List<Tour> getGuidedTours() {
  return [
    Tour(
      id: '1',
      points: [
        TourPoint(
            id: '1',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: []),
        TourPoint(
            id: '2',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: []),
        TourPoint(
            id: '3',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: []),
        TourPoint(
            id: '4',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: []),
        TourPoint(
          id: '5',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [],
        ),
      ],
      description:
          'Nam a varius elit. Proin a arcu sit amet quam pellentesque mattis. Maecenas et suscipit nisl. Aenean auctor tempor nulla, pellentesque rutrum lorem ultrices nec. Mauris nec tortor mauris. Nunc a magna fermentum, mollis est id, accumsan elit. Vestibulum posuere, lectus quis porttitor accumsan, metus elit luctus odio, vitae facilisis est arcu ut metus. Mauris feugiat a quam pulvinar euismod. Duis vel semper justo. Proin viverra lacus ut vehicula pharetra. Nulla rutrum tortor ut dolor interdum, sit amet elementum eros laoreet. Morbi et eleifend dui. Morbi nec urna sit amet neque elementum sodales. Sed tellus lectus, sodales ac tortor gravida, lacinia lacinia odio. Phasellus porta est a urna tempor maximus. Donec in faucibus neque. =',
      distance: 4000,
      duration: 110,
      guideName: 'Consectetur adipiscing elit. Nulla at',
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Guiado',
        type: TourType.GuidedTour,
      ),
      type: TourType.AutoGuidedTour,
      date: "2020-05-12T11:00:00.000Z",
    ),
    Tour(
      id: '2',
      points: [
        TourPoint(
          id: '6',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 + 0.02,
          description:
              'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [],
        ),
        TourPoint(
          id: '7',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 + 0.02,
          description:
              'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [],
        ),
        TourPoint(
          id: '8',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 + 0.02,
          description:
              'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '92',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
        TourPoint(
          id: '9',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 + 0.02,
          description:
              'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '93',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
        TourPoint(
          id: '10',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '91',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'Canção',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
            Trivia(
              title: 'Curiosidade',
              id: '89',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
        TourPoint(
          id: '11',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '71',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'Canção',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
            Trivia(
              title: 'Curiosidade',
              id: '72',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
      ],
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet risus vitae orci ultrices, suscipit rhoncus libero aliquet. Sed ante sem, tincidunt in magna vitae, ultrices porta enim. Nulla sodales dictum tortor tempor sollicitudin. Praesent consectetur mi eget mauris iaculis sollicitudin. Nunc facilisis, ex ornare auctor lobortis, ipsum turpis rhoncus neque, vel rhoncus orci lacus ac felis. In faucibus venenatis lacus eu dignissim. Etiam suscipit purus pretium lorem eleifend, viverra facilisis eros molestie. Maecenas non dolor luctus, blandit sem id, condimentum arcu. Proin vulputate scelerisque dui eget accumsan. Nunc sed orci malesuada, sodales sapien non, efficitur est. Aliquam sodales, erat non pulvinar egestas, odio sapien luctus lorem, ac viverra velit enim ut justo. Aenean fermentum est in condimentum dignissim.',
      distance: 10000,
      duration: 300,
      guideName: 'Consectetur adipiscing elit. Nulla at',
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Guiado',
        type: TourType.GuidedTour,
      ),
      type: TourType.GuidedTour,
      date: "2020-04-28T11:00:00.000Z",
    ),
    Tour(
      id: '3',
      points: [
        TourPoint(
          id: '13',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '81',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
      ],
      description:
          'Nam a varius elit. Proin a arcu sit amet quam pellentesque mattis. Maecenas et suscipit nisl. Aenean auctor tempor nulla, pellentesque rutrum lorem ultrices nec. Mauris nec tortor mauris. Nunc a magna fermentum, mollis est id, accumsan elit. Vestibulum posuere, lectus quis porttitor accumsan, metus elit luctus odio, vitae facilisis est arcu ut metus. Mauris feugiat a quam pulvinar euismod. Duis vel semper justo. Proin viverra lacus ut vehicula pharetra. Nulla rutrum tortor ut dolor interdum, sit amet elementum eros laoreet. Morbi et eleifend dui. Morbi nec urna sit amet neque elementum sodales. Sed tellus lectus, sodales ac tortor gravida, lacinia lacinia odio. Phasellus porta est a urna tempor maximus. Donec in faucibus neque. =',
      distance: 4000,
      duration: 110,
      guideName: 'Consectetur adipiscing elit. Nulla at',
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Guiado',
        type: TourType.GuidedTour,
      ),
      type: TourType.GuidedTour,
      date: "2020-06-24T11:00:00.000Z",
    ),
    Tour(
      id: '4',
      points: [
        TourPoint(
            id: '14',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: [
              Trivia(
                title: 'Curiosidade',
                id: '83',
                text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
                type: 'audio',
                mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
              ),
            ]),
        TourPoint(
          id: '15',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [
            Trivia(
              title: 'Curiosidade',
              id: '81',
              text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
              type: 'audio',
              mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
            ),
          ],
        ),
      ],
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet risus vitae orci ultrices, suscipit rhoncus libero aliquet. Sed ante sem, tincidunt in magna vitae, ultrices porta enim. Nulla sodales dictum tortor tempor sollicitudin. Praesent consectetur mi eget mauris iaculis sollicitudin. Nunc facilisis, ex ornare auctor lobortis, ipsum turpis rhoncus neque, vel rhoncus orci lacus ac felis. In faucibus venenatis lacus eu dignissim. Etiam suscipit purus pretium lorem eleifend, viverra facilisis eros molestie. Maecenas non dolor luctus, blandit sem id, condimentum arcu. Proin vulputate scelerisque dui eget accumsan. Nunc sed orci malesuada, sodales sapien non, efficitur est. Aliquam sodales, erat non pulvinar egestas, odio sapien luctus lorem, ac viverra velit enim ut justo. Aenean fermentum est in condimentum dignissim.',
      distance: 10000,
      duration: 300,
      guideName: 'Consectetur adipiscing elit. Nulla at',
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Guiado',
        type: TourType.GuidedTour,
      ),
      type: TourType.GuidedTour,
      date: "2020-06-21T11:00:00.000Z",
    ),
    Tour(
      id: '5',
      points: [
        TourPoint(
            id: '16',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: [
              Trivia(
                title: 'Curiosidade',
                id: '45',
                text: 'Morbi pulvinar ligula urna, id iaculis ex rhoncus eu.',
                type: 'audio',
                mediaUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
              ),
            ]),
        TourPoint(
          id: '17',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [],
        ),
      ],
      description:
          'Nam a varius elit. Proin a arcu sit amet quam pellentesque mattis. Maecenas et suscipit nisl. Aenean auctor tempor nulla, pellentesque rutrum lorem ultrices nec. Mauris nec tortor mauris. Nunc a magna fermentum, mollis est id, accumsan elit. Vestibulum posuere, lectus quis porttitor accumsan, metus elit luctus odio, vitae facilisis est arcu ut metus. Mauris feugiat a quam pulvinar euismod. Duis vel semper justo. Proin viverra lacus ut vehicula pharetra. Nulla rutrum tortor ut dolor interdum, sit amet elementum eros laoreet. Morbi et eleifend dui. Morbi nec urna sit amet neque elementum sodales. Sed tellus lectus, sodales ac tortor gravida, lacinia lacinia odio. Phasellus porta est a urna tempor maximus. Donec in faucibus neque. =',
      distance: 4000,
      duration: 110,
      guideName: 'Consectetur adipiscing elit. Nulla at',
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Guiado',
        type: TourType.GuidedTour,
      ),
      type: TourType.GuidedTour,
      date: "2020-07-01T11:00:00.000Z",
    ),
    Tour(
      id: '6',
      points: [
        TourPoint(
            id: '18',
            latitude: -22.8171014 - 0.02,
            longitude: -47.0744113 + 0.02,
            description:
                'estibulum vel lectus id turpis venenatis iaculis sit amet ac nunc. Pellentesque quis dictum sem, quis fringilla libero. Proin euismod quam at consequat bibendum',
            pictureUrls: [
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
            ],
            audioDescriptionUrl:
                'https://yorool.com/api/v1/audio/book/example.mp3',
            titleInfo: TitleInfo(
              title: 'Ponto',
              type: TourType.TourPoint,
            ),
            trivia: []),
        TourPoint(
          id: '19',
          latitude: -22.8171014 - 0.02,
          longitude: -47.0744113 - 0.02,
          description:
              'Praesent ultrices, dolor sagittis feugiat vulputate, lacus nisi vestibulum ligula, id efficitur lorem nulla ac nisi. Nam et urna in ipsum ullamcorper lobortis in quis ligula. ',
          pictureUrls: [
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg'
          ],
          audioDescriptionUrl:
              'https://yorool.com/api/v1/audio/book/example.mp3',
          titleInfo: TitleInfo(
            title: 'Ponto',
            type: TourType.TourPoint,
          ),
          trivia: [],
        ),
      ],
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet risus vitae orci ultrices, suscipit rhoncus libero aliquet. Sed ante sem, tincidunt in magna vitae, ultrices porta enim. Nulla sodales dictum tortor tempor sollicitudin. Praesent consectetur mi eget mauris iaculis sollicitudin. Nunc facilisis, ex ornare auctor lobortis, ipsum turpis rhoncus neque, vel rhoncus orci lacus ac felis. In faucibus venenatis lacus eu dignissim. Etiam suscipit purus pretium lorem eleifend, viverra facilisis eros molestie. Maecenas non dolor luctus, blandit sem id, condimentum arcu. Proin vulputate scelerisque dui eget accumsan. Nunc sed orci malesuada, sodales sapien non, efficitur est. Aliquam sodales, erat non pulvinar egestas, odio sapien luctus lorem, ac viverra velit enim ut justo. Aenean fermentum est in condimentum dignissim.',
      distance: 10000,
      duration: 300,
      guideName: 'Consectetur adipiscing elit. Nulla at',
      pictureUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Palace_of_Fine_Arts_%2816794p%29.jpg/1599px-Palace_of_Fine_Arts_%2816794p%29.jpg',
      audioDescriptionUrl: 'https://yorool.com/api/v1/audio/book/example.mp3',
      titleInfo: TitleInfo(
        title: 'Tour Guiado',
        type: TourType.GuidedTour,
      ),
      type: TourType.GuidedTour,
      date: "2020-07-04T11:00:00.000Z",
    ),
  ];
}
