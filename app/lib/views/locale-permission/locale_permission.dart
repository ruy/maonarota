import 'dart:io';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:location/location.dart' as loc;
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:mao_na_rota/widgets/logo_header.dart';

class LocalePermission extends StatefulWidget {
  static const String routeName = '/permission';

  @override
  _LocalePermissionState createState() => _LocalePermissionState();
}

class _LocalePermissionState extends State<LocalePermission>
    with WidgetsBindingObserver {
  loc.Location location = loc.Location();
  var _isLoading = false;
  bool _serviceEnabled;
  bool _redirected = false;

  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      if (await location.serviceEnabled() &&
          await Permission.location.status.isGranted) {
        Navigator.of(context).pop();
      }
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void _onTap(context) async {
    setState(() {
      _isLoading = true;
    });

    _serviceEnabled = await location.serviceEnabled();

    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();

      if (!_serviceEnabled) {
        setState(() {
          _isLoading = false;
        });
      }
    }

    PermissionStatus permission = await Permission.location.request();

    if (!permission.isGranted) {
      setState(() {
        _isLoading = false;
      });

      if (permission.isPermanentlyDenied ||
          (permission.isDenied && Platform.isIOS)) {
        _redirected = true;
        openAppSettings();
      }
    }

    setState(() {
      _isLoading = false;
    });

    if (_redirected) {
      _redirected = false;
      Navigator.of(context).pop();
    }
  }

  Future<bool> _onPopScope() async {
    return false;
    // if (!_serviceEnabled || _permissionGranted != PermissionStatus.granted) {
    //   return false;
    // }
    // return true;
  }

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;

    return WillPopScope(
      onWillPop: () => _onPopScope(),
      child: Scaffold(
        body: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
          ),
          color: Theme.of(context).primaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LogoHeader(),
              Expanded(
                child: Center(
                  child: Container(
                    margin: const EdgeInsets.only(
                      top: 100.0,
                    ),
                    width: _width * 0.7,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SvgPicture.asset(
                          'assets/images/marker-logo.svg',
                          width: 60,
                          height: 60,
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Text(
                          'Ops!',
                          style: TextStyle(
                            fontSize: 22,
                            color: Theme.of(context).accentColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          'A localização do dispositivo está desativada.\nPrecisamos disso para navegar pelas rotas!',
                          style: TextStyle(
                            fontSize: 18,
                            color: Color(0xFFC4C4C4),
                            fontWeight: FontWeight.normal,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        FlatButton(
                          color: Theme.of(context).accentColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          onPressed: () => _onTap(context),
                          child: Container(
                            height: 36,
                            width: 153,
                            child: Center(
                              child: _isLoading
                                  ? SpinKitFadingFour(
                                      color: Colors.white,
                                      size: 15.0,
                                    )
                                  : Text(
                                      'ATIVAR',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
