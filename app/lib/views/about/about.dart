import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Theme.of(context).primaryColor,
            elevation: 5,
            floating: true,
            pinned: true,
            expandedHeight: 200,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              titlePadding: const EdgeInsets.symmetric(vertical: 10.0),
              title: Text(
                'MÃO NA ROTA',
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 14,
                ),
              ),
              background: Container(
                color: Theme.of(context).primaryColor,
                margin: const EdgeInsets.only(bottom: 10.0),
                height: 100,
                child: Center(
                  child: Image.asset(
                    'assets/images/logo_white.png',
                    width: 100,
                  ),
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 40,
                    vertical: 15,
                  ),
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: Text(
                      'O projeto é uma parceria entre <> e o desenvolvedor Ruy Castilho Barrichelo, realizado como Projeto Final de Graduação em Computação na Unicamp.',
                      style: TextStyle(
                        fontSize: 15,
                        color: Color(
                          0xFF666666,
                        ),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SliverFillRemaining(
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              color: Theme.of(context).primaryColor,
              child: Center(
                child: Column(
                  children: [
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Objetivo',
                      style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).accentColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet ',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(
                          0xFF666666,
                        ),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
