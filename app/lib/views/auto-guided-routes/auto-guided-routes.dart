import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:mao_na_rota/widgets/bottom_description.dart';
import 'package:mao_na_rota/widgets/carousel_item.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class AutoGuidedRoutes extends StatefulWidget {
  @override
  _AutoGuidedRoutesState createState() => _AutoGuidedRoutesState();
}

class _AutoGuidedRoutesState extends State<AutoGuidedRoutes> {
  int _pictureIndex = 0;

  Widget content(TourListModel tourListModel) {
    Size size = MediaQuery.of(context).size;

    if (tourListModel.hasError) {
      return Container(
        height: size.height - 100,
        color: Theme.of(context).primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              width: size.width * 0.7,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/images/logo_white.png',
                    width: 100,
                    height: 100,
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Text(
                    'Ops!',
                    style: TextStyle(
                      fontSize: 22,
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Não foi possível buscar as rotas!\nCheque sua conexão',
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xFFC4C4C4),
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  FlatButton(
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    onPressed: () => tourListModel.loadTours(),
                    child: Container(
                      height: 36,
                      width: 153,
                      child: Center(
                        child: Text(
                          'TENTAR NOVAMENTE',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                            fontWeight: FontWeight.normal,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    List<Tour> tours = tourListModel.autoGuidedTours;

    if (tourListModel.isLoading || (tours != null && tours.isEmpty)) {
      return Container(
        height: size.height - 100,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xFF424141), Theme.of(context).primaryColor],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Center(
                child: Image.asset(
                  'assets/images/logo_white.png',
                  width: 100,
                  height: 100,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: const EdgeInsets.only(
                  right: 40,
                  bottom: 60,
                ),
                width: size.width * 0.45,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      enabled: true,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        height: 35,
                      ),
                      baseColor: Theme.of(context).primaryColor,
                      highlightColor: Color(0xFF424141),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Shimmer.fromColors(
                      enabled: true,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        height: 15,
                      ),
                      baseColor: Theme.of(context).primaryColor,
                      highlightColor: Color(0xFF424141),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Stack(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(bottom: 60),
          child: CarouselSlider(
            items: tours
                .map(
                  (tour) => CarouselItem(
                    tour: tour,
                  ),
                )
                .toList(),
            height: size.height - 100,
            aspectRatio: 16 / 9,
            viewportFraction: 1.0,
            initialPage: 0,
            enableInfiniteScroll: true,
            reverse: true,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 5),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            pauseAutoPlayOnTouch: Duration(seconds: 10),
            enlargeCenterPage: false,
            onPageChanged: (index) {
              setState(() {
                _pictureIndex = index;
              });
            },
            scrollDirection: Axis.horizontal,
          ),
        ),
        Positioned(
          bottom: 80.0,
          left: 5.0,
          right: 5.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: tours.asMap().entries.map(
              (MapEntry entry) {
                return Flexible(
                  flex: 1,
                  child: Container(
                    height: 2.0,
                    margin: EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: 2.0,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: _pictureIndex == entry.key
                          ? Theme.of(context).accentColor
                          : Colors.white,
                    ),
                  ),
                );
              },
            ).toList(),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, TourListModel>(
      selector: (_, model) => model,
      shouldRebuild: (prev, next) => next != null,
      builder: (context, tourList, _) {
        return Stack(
          children: [
            content(tourList),
            Align(
              alignment: Alignment.bottomCenter,
              child: BottomDescription(
                text:
                    'Trilhas e rotas históricas da cidade de Campinas, na palma da sua mão, com auxílio de mapas, descrições de áudio e mais!',
              ),
            ),
          ],
        );
      },
    );
  }
}
