import 'package:flutter/material.dart';
import 'package:mao_na_rota/views/about/about.dart';
import 'package:mao_na_rota/views/auto-guided-routes/auto-guided-routes.dart';

import 'package:mao_na_rota/views/calendar/calendar.dart';
import 'package:mao_na_rota/widgets/custom_bottom_navigation_bar.dart';

class Home extends StatefulWidget {
  static const String routeName = '/home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  PageController _pageController;

  @override
  void initState() {
    _pageController = PageController();
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _onItemTapped(int index) {
    _pageController.animateToPage(index,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  body() {
    return SizedBox.expand(
      child: PageView(
        controller: _pageController,
        children: [
          AutoGuidedRoutes(),
          Calendar(),
          AboutPage(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
      bottomNavigationBar: CustomBottomNavigationBar(
        onTapItem: _onItemTapped,
      ),
    );
  }
}
