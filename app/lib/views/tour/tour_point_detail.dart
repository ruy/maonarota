import 'dart:io';

import 'package:animations/animations.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/widgets/tour_point_description.dart';
import 'package:provider/provider.dart';

class TourPointDetail extends StatefulWidget {
  static const String routeName = '/tour-point-detail';
  final String pointId;

  TourPointDetail({
    @required this.pointId,
  });

  @override
  _TourPointDetailState createState() => _TourPointDetailState();
}

class _TourPointDetailState extends State<TourPointDetail> {
  var items = [1, 1, 1];
  var _pictureIndex = 0;

  void _onTap(context) {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, Tour>(
        selector: (_, model) => model.tourByTourPointId(widget.pointId),
        shouldRebuild: (prev, next) => next != null,
        builder: (context, tour, _) {
          TourPoint point =
              tour.points.firstWhere((point) => point.id == widget.pointId);

          return Scaffold(
            body: Stack(
              children: [
                OpenContainer(
                  closedElevation: 0,
                  closedColor: Theme.of(context).primaryColor,
                  openColor: Theme.of(context).primaryColor,
                  transitionType: ContainerTransitionType.fade,
                  transitionDuration: const Duration(milliseconds: 700),
                  tappable: point.pictures == null &&
                      point.pictures.isNotEmpty &&
                      point.pictures.first == null &&
                      point.pictures.first.isNotEmpty,
                  openBuilder: (context, action) {
                    return PhotoViewGallery.builder(
                      scrollPhysics: const BouncingScrollPhysics(),
                      builder: (BuildContext context, int index) {
                        return PhotoViewGalleryPageOptions(
                          imageProvider: tour.isSaved
                              ? FileImage(File(point.pictures[index]))
                              : NetworkImage(
                                  point.pictures[index],
                                ),
                          minScale: PhotoViewComputedScale.contained,
                          maxScale: PhotoViewComputedScale.contained * 2.0,
                          initialScale: PhotoViewComputedScale.contained,
                          onTapUp: (_, __, ___) => action(),
                        );
                      },
                      gaplessPlayback: true,
                      itemCount: point?.pictures?.length ?? 0,
                      loadingBuilder: (context, event) => Center(
                        child: Container(
                          width: 20.0,
                          height: 20.0,
                          color: Theme.of(context).primaryColor,
                          child: CircularProgressIndicator(
                            backgroundColor: Theme.of(context).accentColor,
                            value: event == null
                                ? 0
                                : event.cumulativeBytesLoaded /
                                    event.expectedTotalBytes,
                          ),
                        ),
                      ),
                      backgroundDecoration: BoxDecoration(
                        color: Colors.grey[800],
                      ),
                    );
                  },
                  closedBuilder: (context, action) {
                    if (point.pictures == null ||
                        point.pictures.isEmpty ||
                        point.pictures.first == null ||
                        point.pictures.first.isEmpty)
                      return Container(
                        height: 160,
                        width: double.infinity,
                        color: Theme.of(context).primaryColor,
                        child: Center(
                          child: Image.asset(
                            'assets/images/logo_small.png',
                            width: 80,
                            height: 80,
                          ),
                        ),
                      );

                    return Container(
                      height: 160,
                      margin: const EdgeInsets.only(bottom: 50),
                      child: PhotoViewGallery.builder(
                        scrollPhysics: const BouncingScrollPhysics(),
                        builder: (BuildContext context, int index) {
                          return PhotoViewGalleryPageOptions.customChild(
                            gestureDetectorBehavior:
                                HitTestBehavior.deferToChild,
                            tightMode: true,
                            child: Container(
                              height: 160,
                              decoration: BoxDecoration(
                                color: Colors.grey[800],
                                image: DecorationImage(
                                  image: tour.isSaved
                                      ? FileImage(File(point.pictures[index]))
                                      : NetworkImage(point.pictures[index]),
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                            minScale: PhotoViewComputedScale.covered,
                            maxScale: PhotoViewComputedScale.covered,
                            initialScale: PhotoViewComputedScale.covered,
                            onTapUp: (_, __, ___) => action(),
                          );
                        },
                        gaplessPlayback: true,
                        itemCount: point.pictures.length,
                        loadingBuilder: (context, event) => Center(
                          child: Container(
                            width: 20.0,
                            height: 20.0,
                            color: Theme.of(context).primaryColor,
                            child: CircularProgressIndicator(
                              backgroundColor: Theme.of(context).accentColor,
                              value: event == null
                                  ? 0
                                  : event.cumulativeBytesLoaded /
                                      event.expectedTotalBytes,
                            ),
                          ),
                        ),
                        onPageChanged: (index) {
                          setState(() {
                            _pictureIndex = index;
                          });
                        },
                      ),
                    );
                  },
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(
                      20,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        GestureDetector(
                          child: Icon(
                            Icons.keyboard_arrow_left,
                            color: Colors.white,
                            size: 30,
                          ),
                          onTap: () => _onTap(context),
                        ),
                        Image.asset(
                          'assets/images/logo_white.png',
                          width: 25,
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 145,
                  left: 5.0,
                  right: 5.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: point.pictures.asMap().entries.map(
                      (MapEntry entry) {
                        return Flexible(
                          flex: 1,
                          child: Container(
                            // width: 8.0,
                            height: 2.0,
                            margin: const EdgeInsets.symmetric(
                              horizontal: 2,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: _pictureIndex == entry.key
                                  ? Theme.of(context).accentColor
                                  : Colors.white,
                            ),
                          ),
                        );
                      },
                    ).toList(),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: TourPointDescription(
                    pointId: point.id,
                  ),
                ),
              ],
            ),
          );
        });
  }
}
