import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:provider/provider.dart';
import 'package:mao_na_rota/model/tour.dart' as model;
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/widgets/audio_player.dart';
import 'package:mao_na_rota/widgets/frame_locations_button.dart';
import 'package:mao_na_rota/widgets/marker.dart';
import 'package:mao_na_rota/widgets/own_location_button.dart';
import 'package:mao_na_rota/widgets/redirectable_scaffold.dart';
import 'package:mao_na_rota/widgets/tour_card.dart';

class Tour extends StatefulWidget {
  static const String routeName = '/tour';
  final String tourId;

  Tour({
    @required this.tourId,
  });

  @override
  _TourState createState() => _TourState();
}

class _TourState extends State<Tour> {
  var lat = 0.0;
  var long = 0.0;
  var _currentPoint = 0;
  double frameLatitude = 0.0;
  double frameLongitude = 0.0;

  StreamSubscription<Position> positionStream;
  MapController mapController;
  Geolocator geolocator;
  List<TourPoint> _tourPoints;

  final locationOptions = LocationOptions(
    accuracy: LocationAccuracy.high,
    distanceFilter: 10,
  );

  void _onTap(context) {
    Navigator.of(context).pop();
  }

  void _frameLocations() {
    mapController.move(
      LatLng(frameLatitude, frameLongitude),
      13,
    );
  }

  void _moveToOwnLocation() {
    mapController.move(
      LatLng(lat, long),
      mapController.zoom,
    );
  }

  @override
  void initState() {
    _tourPoints = Provider.of<TourListModel>(context, listen: false)
        .tourById(widget.tourId)
        .points;
    mapController = MapController();
    geolocator = Geolocator();
    _getPosition();
    positionStream = geolocator.getPositionStream(locationOptions).listen(
      (Position position) {
        if (mounted) {
          setState(
            () {
              lat = position.latitude;
              long = position.longitude;
            },
          );
        }
      },
    );

    double maxLatitude = -double.infinity;
    double minLatitude = double.infinity;
    double maxLongitude = -double.infinity;
    double minLongitude = double.infinity;

    for (var point in _tourPoints) {
      if (point.latitude > maxLatitude) {
        maxLatitude = point.latitude;
      } else if (point.latitude < minLatitude) {
        minLatitude = point.latitude;
      }

      if (point.longitude > maxLongitude) {
        maxLongitude = point.longitude;
      } else if (point.longitude < minLongitude) {
        minLongitude = point.longitude;
      }
    }

    frameLatitude = (maxLatitude + minLatitude) / 2;
    frameLongitude = (maxLongitude + minLongitude) / 2;

    super.initState();
  }

  @override
  void dispose() {
    positionStream.cancel();
    super.dispose();
  }

  void _getPosition() async {
    Position position = await geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    if (mounted) {
      setState(
        () {
          lat = position.latitude;
          long = position.longitude;
        },
      );

      mapController.move(
        LatLng(position.latitude, position.longitude),
        mapController.zoom,
      );
    }
  }

  void _onPointChanged(int index) {
    setState(
      () {
        _currentPoint = index;
      },
    );

    var _tourPoint = _tourPoints[index];

    mapController.move(
      LatLng(
        _tourPoint.latitude,
        _tourPoint.longitude,
      ),
      mapController.zoom,
    );
  }

  void _onPressMarker(int index, CarouselSlider slider) {
    slider.jumpToPage(index);
    setState(
      () {
        _currentPoint = index;
      },
    );

    var _tourPoint = _tourPoints[index];

    mapController.move(
      LatLng(
        _tourPoint.latitude,
        _tourPoint.longitude,
      ),
      mapController.zoom,
    );
  }

  List<CustomMarker> _buildMarkers(
      BuildContext context, CarouselSlider slider) {
    var _userMarker = CustomMarker(
      coordinates: LatLng(lat, long),
      context: context,
      type: MarkerType.User,
    );

    var _tourMarkers = _tourPoints.asMap().entries.map((MapEntry entry) {
      return CustomMarker(
        coordinates: LatLng(entry.value.latitude, entry.value.longitude),
        context: context,
        type: entry.key == _currentPoint
            ? MarkerType.CurrentMarker
            : MarkerType.NonCurrentMarker,
        onTap: () => _onPressMarker(entry.key, slider),
        index: entry.key + 1,
      );
    }).toList();

    _tourMarkers.add(_userMarker);

    return _tourMarkers;
  }

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, model.Tour>(
      selector: (_, model) =>
          model.tours.firstWhere((tour) => tour.id == widget.tourId),
      shouldRebuild: (prev, next) => next != null,
      builder: (context, tour, _) {
        final _pointSlider = CarouselSlider(
          items: tour.points
              .map(
                (point) => TourCard(point: point),
              )
              .toList(),
          height: 90,
          viewportFraction: 0.90,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
          enlargeCenterPage: true,
          onPageChanged: (index) {
            _onPointChanged(index);
          },
          scrollDirection: Axis.horizontal,
        );

        return RedirectableScaffold(
          body: Stack(
            children: [
              FlutterMap(
                mapController: mapController,
                options: MapOptions(
                  center: LatLng(_tourPoints?.first?.latitude ?? lat,
                      _tourPoints?.first?.longitude ?? long),
                  // bounds: LatLngBounds(
                  //   LatLng(maxLatitude, maxLongitude),
                  //   LatLng(minLatitude, minLongitude),
                  // ),
                  zoom: 16.5,
                  plugins: [],
                ),
                layers: [
                  tour.isSaved
                      ? TileLayerOptions(
                          tileProvider: MBTilesImageProvider.fromFile(
                            File(tour.localMapFilename),
                          ),
                          tms: true,
                        )
                      : TileLayerOptions(
                          urlTemplate:
                              "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                          subdomains: ['a', 'b', 'c'],
                        ),
                  MarkerLayerOptions(
                    markers: _buildMarkers(context, _pointSlider),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(
                    20,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.keyboard_arrow_left,
                          color: Theme.of(context).accentColor,
                          size: 30,
                        ),
                        onTap: () => _onTap(context),
                      ),
                      Image.asset(
                        'assets/images/logo_white.png',
                        width: 25,
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  // margin: const EdgeInsets.only(bottom: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: FrameLocationsButton(onTap: _frameLocations),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          bottom: 20,
                        ),
                        child: OwnLocationButton(onTap: _moveToOwnLocation),
                      ),
                      _pointSlider,
                      SizedBox(
                        height: 20,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: AudioPlayer(),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
