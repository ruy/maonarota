import 'dart:io';

import 'package:audio_service/audio_service.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:photo_view/photo_view.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/widgets/redirectable_scaffold.dart';
import 'package:mao_na_rota/widgets/tour_description.dart';
import 'package:provider/provider.dart';

class TourDetail extends StatelessWidget {
  static const String routeName = '/tour-detail';
  final String tourId;

  TourDetail({
    @required this.tourId,
  });

  void _onTap(context) async {
    AudioService.stop();
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, Tour>(
      selector: (_, model) =>
          model.tours.firstWhere((tour) => tour.id == tourId),
      shouldRebuild: (prev, next) => next != null,
      builder: (context, tour, _) {
        return WillPopScope(
          onWillPop: () async {
            AudioService.stop();
            return true;
          },
          child: RedirectableScaffold(
            body: AudioServiceWidget(
              child: Stack(
                children: [
                  OpenContainer(
                    closedElevation: 0,
                    closedColor: Theme.of(context).primaryColor,
                    openColor: Theme.of(context).primaryColor,
                    transitionType: ContainerTransitionType.fade,
                    transitionDuration: const Duration(milliseconds: 700),
                    tappable: tour.picture != null && tour.picture.isNotEmpty,
                    openBuilder: (context, action) {
                      if (tour.picture == null || tour.picture.isEmpty)
                        return Image.asset(
                          'assets/images/logo_small.png',
                          width: 50,
                          height: 50,
                        );

                      return PhotoView(
                        imageProvider: tour.isSaved
                            ? FileImage(File(tour.picture))
                            : NetworkImage(tour.picture),
                        minScale: PhotoViewComputedScale.contained,
                        maxScale: PhotoViewComputedScale.contained * 2.0,
                        initialScale: PhotoViewComputedScale.contained,
                        onTapUp: (_, __, ___) => action(),
                        loadingBuilder: (context, event) => Center(
                          child: Container(
                            width: 20.0,
                            height: 20.0,
                            color: Theme.of(context).primaryColor,
                            child: CircularProgressIndicator(
                              backgroundColor: Theme.of(context).accentColor,
                              value: event == null
                                  ? 0
                                  : event.cumulativeBytesLoaded /
                                      event.expectedTotalBytes,
                            ),
                          ),
                        ),
                        backgroundDecoration: BoxDecoration(
                          color: Colors.grey[800],
                        ),
                      );
                    },
                    closedBuilder: (context, action) {
                      if (tour.picture == null || tour.picture.isEmpty)
                        return Container(
                          height: 160,
                          width: double.infinity,
                          color: Colors.grey[800],
                          child: Center(
                            child: Image.asset(
                              'assets/images/logo_small.png',
                              width: 80,
                              height: 80,
                            ),
                          ),
                        );

                      return Container(
                        height: 160,
                        decoration: BoxDecoration(
                          color: Colors.grey[800],
                          image: DecorationImage(
                            image: tour.isSaved
                                ? FileImage(File(tour.picture))
                                : NetworkImage(
                                    tour.picture,
                                  ),
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      );
                    },
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(
                        20,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            child: Icon(
                              Icons.keyboard_arrow_left,
                              color: Colors.white,
                              size: 30,
                            ),
                            onTap: () => _onTap(context),
                          ),
                          Image.asset(
                            'assets/images/logo_white.png',
                            width: 25,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: TourDescription(
                      tourId: tourId,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
