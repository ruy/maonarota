import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:mao_na_rota/widgets/logo_header.dart';
import 'package:mao_na_rota/widgets/calendar_item.dart';
import 'package:mao_na_rota/widgets/calendar_month.dart';
import 'package:provider/provider.dart';

class Calendar extends StatelessWidget {
  final List<String> months = [
    'Index Fix',
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ];

  @override
  Widget build(BuildContext context) {
    return Selector<TourListModel, TourListModel>(
      selector: (_, model) => model,
      shouldRebuild: (prev, next) => next != null,
      builder: (context, tourList, _) {
        Size size = MediaQuery.of(context).size;

        if (tourList.hasError) {
          return Container(
            height: size.height - 100,
            color: Theme.of(context).primaryColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  width: size.width * 0.7,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        'assets/images/logo_white.png',
                        width: 100,
                        height: 100,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        'Ops!',
                        style: TextStyle(
                          fontSize: 22,
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'Não foi possível buscar as rotas!\nCheque sua conexão',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFFC4C4C4),
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      FlatButton(
                        color: Theme.of(context).accentColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        onPressed: () => tourList.loadTours(),
                        child: Container(
                          height: 36,
                          width: 153,
                          child: Center(
                            child: Text(
                              'TENTAR NOVAMENTE',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                                fontWeight: FontWeight.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }

        List<Tour> tours = tourList.guidedTours;

        if (tours != null) {
          tours
              .sort((tourA, tourB) => tourA.tourDate.compareTo(tourB.tourDate));
        } else if (!tourList.isLoading && tours.isEmpty) {
          return Container(
            color: Theme.of(context).primaryColor,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Agenda Vazia!',
                  style: TextStyle(
                    fontSize: 18,
                    color: Theme.of(context).accentColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Não há datas marcadas para rotas guiadas',
                  style: TextStyle(
                    fontSize: 14,
                    color: Color(0xFFC4C4C4),
                    fontWeight: FontWeight.w400,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          );
        }

        return ShaderMask(
          blendMode: BlendMode.srcOver,
          shaderCallback: (Rect bounds) {
            return LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [Theme.of(context).primaryColor, Colors.transparent],
            ).createShader(
              Rect.fromLTRB(
                bounds.width - 150,
                bounds.height - 150,
                bounds.width,
                bounds.height,
              ),
            );
          },
          child: Container(
            color: Theme.of(context).primaryColor,
            padding: const EdgeInsets.symmetric(
              horizontal: 20.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LogoHeader(),
                Expanded(
                  child: AnimationLimiter(
                    child: ListView.builder(
                      padding: const EdgeInsets.only(bottom: 50),
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: tourList.isLoading ? 6 : tours.length,
                      itemBuilder: (context, index) {
                        Widget child;

                        if (index == 0 || tourList.isLoading) {
                          child = AnimationLimiter(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: AnimationConfiguration.toStaggeredList(
                                duration: const Duration(milliseconds: 300),
                                childAnimationBuilder: (widget) =>
                                    SlideAnimation(
                                  verticalOffset: 20.0,
                                  horizontalOffset: 30.0,
                                  child: FadeInAnimation(
                                    child: widget,
                                  ),
                                ),
                                children: [
                                  if (!tourList.isLoading)
                                    CalendarMonth(
                                      month:
                                          months[tours[index].tourDate.month],
                                    ),
                                  CalendarItem(
                                    tour: tourList.isLoading
                                        ? null
                                        : tours[index],
                                    loading: tourList.isLoading,
                                  ),
                                ],
                              ),
                            ),
                          );
                        } else {
                          var prev = tours[index - 1];
                          var curr = tours[index];

                          if (prev.tourDate.month != curr.tourDate.month) {
                            child = AnimationLimiter(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children:
                                    AnimationConfiguration.toStaggeredList(
                                  duration: const Duration(milliseconds: 300),
                                  childAnimationBuilder: (widget) =>
                                      SlideAnimation(
                                    verticalOffset: 20.0,
                                    horizontalOffset: 30.0,
                                    child: FadeInAnimation(
                                      child: widget,
                                    ),
                                  ),
                                  children: [
                                    CalendarMonth(
                                      month: months[curr.tourDate.month],
                                    ),
                                    CalendarItem(
                                      tour: tours[index],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          } else {
                            child = CalendarItem(
                              tour: tours[index],
                            );
                          }
                        }

                        return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          child: SlideAnimation(
                            verticalOffset: 50.0,
                            horizontalOffset: 10.0,
                            child: FadeInAnimation(
                              child: child,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
