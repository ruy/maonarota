import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:mao_na_rota/model/tour_list.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/material.dart';

import 'dart:isolate';
import 'dart:ui';

import 'package:mao_na_rota/repositories/tour_repository.dart';
import 'package:mao_na_rota/views/locale-permission/locale_permission.dart';
import 'package:mao_na_rota/views/home/home.dart';
import 'package:mao_na_rota/views/tour/tour_detail.dart';
import 'package:mao_na_rota/views/tour/tour.dart';
import 'package:mao_na_rota/views/tour/tour_point_detail.dart';
import 'package:provider/provider.dart';

void downloadCallback(String id, DownloadTaskStatus status, int progress) {
  final SendPort send = IsolateNameServer.lookupPortByName('downloader_port');
  send.send([id, status, progress]);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(debug: true);

  FlutterDownloader.registerCallback(downloadCallback);

  runApp(
    MaoNaRota(),
  );
}

class MaoNaRota extends StatefulWidget {
  @override
  _MaoNaRotaState createState() => _MaoNaRotaState();
}

// TODO fix app icon

class _MaoNaRotaState extends State<MaoNaRota> with WidgetsBindingObserver {
  TourRepository tourRepository = TourRepository();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: ChangeNotifierProvider<TourListModel>(
        create: (_) => TourListModel(repository: tourRepository)..loadTours(),
        child: MaterialApp(
          theme: ThemeData(
            appBarTheme: AppBarTheme(
              elevation: 0,
              brightness: Brightness.dark,
              color: Colors.transparent,
            ),
            primaryColor: Color(0xFF222222),
            accentColor: Color(0xFFDC5586),
            fontFamily: 'Nunito',
            bottomSheetTheme: BottomSheetThemeData(
              backgroundColor: Colors.transparent,
            ),
            textTheme: ThemeData().textTheme.copyWith(
                  bodyText2: TextStyle(
                    color: Color(0xFF7B7B7B),
                  ),
                ),
          ),
          initialRoute: Home.routeName,
          onGenerateRoute: (settings) {
            var _args = settings.arguments;

            switch (settings.name) {
              case Home.routeName:
                return PageTransition(
                  child: Home(),
                  type: PageTransitionType.fade,
                  settings: settings,
                );
                break;
              case LocalePermission.routeName:
                return PageTransition(
                  child: LocalePermission(),
                  type: PageTransitionType.upToDown,
                  settings: settings,
                );
                break;
              case Tour.routeName:
                return PageTransition(
                  child: Tour(
                    tourId: _args,
                  ),
                  type: PageTransitionType.rightToLeftWithFade,
                  settings: settings,
                );
                break;
              case TourDetail.routeName:
                return PageTransition(
                  child: TourDetail(
                    tourId: _args,
                  ),
                  type: PageTransitionType.rightToLeftWithFade,
                  settings: settings,
                );
                break;
              case TourPointDetail.routeName:
                return PageTransition(
                  child: TourPointDetail(
                    pointId: _args,
                  ),
                  type: PageTransitionType.rightToLeftWithFade,
                  settings: settings,
                );
                break;
              default:
                return null;
            }
          },
        ),
      ),
    );
  }
}
