// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'download_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DownloadItem _$DownloadItemFromJson(Map<String, dynamic> json) {
  return DownloadItem(
    url: json['url'] as String,
    id: json['id'] as String,
    completed: json['completed'] as bool,
  );
}

Map<String, dynamic> _$DownloadItemToJson(DownloadItem instance) =>
    <String, dynamic>{
      'url': instance.url,
      'id': instance.id,
      'completed': instance.completed,
    };
