// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tour.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tour _$TourFromJson(Map<String, dynamic> json) {
  return Tour(
    id: json['id'] as String,
    points: (json['points'] as List)
        .map((e) => TourPoint.fromJson(e as Map<String, dynamic>))
        .toList(),
    pictureUrl: json['pictureUrl'] as String,
    titleInfo: TitleInfo.fromJson(json['titleInfo'] as Map<String, dynamic>),
    guideName: json['guideName'] as String,
    duration: (json['duration'] as num).toDouble(),
    distance: (json['distance'] as num).toDouble(),
    type: _$enumDecode(_$TourTypeEnumMap, json['type']),
    description: json['description'] as String,
    date: json['date'] as String,
    audioDescriptionUrl: json['audioDescriptionUrl'] as String,
    mapUrl: json['mapUrl'] as String,
    updatedAt: json['updatedAt'] as String,
    shouldBeSaved: json['shouldBeSaved'] as bool,
  )
    ..localAudioDescriptionFilename =
        json['localAudioDescriptionFilename'] as String
    ..localPictureFilename = json['localPictureFilename'] as String
    ..localMapFilename = json['localMapFilename'] as String
    ..localFiles = (json['localFiles'] as List)
        .map((e) => DownloadItem.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$TourToJson(Tour instance) => <String, dynamic>{
      'id': instance.id,
      'points': instance.points,
      'pictureUrl': instance.pictureUrl,
      'guideName': instance.guideName,
      'duration': instance.duration,
      'distance': instance.distance,
      'type': _$TourTypeEnumMap[instance.type],
      'titleInfo': instance.titleInfo,
      'description': instance.description,
      'date': instance.date,
      'audioDescriptionUrl': instance.audioDescriptionUrl,
      'mapUrl': instance.mapUrl,
      'updatedAt': instance.updatedAt,
      'localAudioDescriptionFilename': instance.localAudioDescriptionFilename,
      'localPictureFilename': instance.localPictureFilename,
      'localMapFilename': instance.localMapFilename,
      'localFiles': instance.localFiles,
      'shouldBeSaved': instance.shouldBeSaved,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

const _$TourTypeEnumMap = {
  TourType.GuidedTour: 'GuidedTour',
  TourType.AutoGuidedTour: 'AutoGuidedTour',
  TourType.TourPoint: 'TourPoint',
};
