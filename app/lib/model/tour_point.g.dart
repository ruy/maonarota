// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tour_point.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TourPoint _$TourPointFromJson(Map<String, dynamic> json) {
  return TourPoint(
    id: json['id'] as String,
    pictureUrls: (json['pictureUrls'] as List).map((e) => e as String).toList(),
    titleInfo: TitleInfo.fromJson(json['titleInfo'] as Map<String, dynamic>),
    trivia: (json['trivia'] as List)
        .map((e) => Trivia.fromJson(e as Map<String, dynamic>))
        .toList(),
    latitude: (json['latitude'] as num).toDouble(),
    longitude: (json['longitude'] as num).toDouble(),
    description: json['description'] as String,
    audioDescriptionUrl: json['audioDescriptionUrl'] as String,
    isSaved: json['isSaved'] as bool,
  )
    ..localAudioDescriptionFilename =
        json['localAudioDescriptionFilename'] as String
    ..localPictureFilenames = (json['localPictureFilenames'] as List)
        .map((e) => e as String)
        .toList();
}

Map<String, dynamic> _$TourPointToJson(TourPoint instance) => <String, dynamic>{
      'id': instance.id,
      'pictureUrls': instance.pictureUrls,
      'trivia': instance.trivia,
      'titleInfo': instance.titleInfo,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'description': instance.description,
      'audioDescriptionUrl': instance.audioDescriptionUrl,
      'localAudioDescriptionFilename': instance.localAudioDescriptionFilename,
      'localPictureFilenames': instance.localPictureFilenames,
      'isSaved': instance.isSaved,
    };
