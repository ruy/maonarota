import 'package:flutter/material.dart';
import 'package:mao_na_rota/model/tour.dart';

class UpdateTourItem {
  final Tour tour;
  final String taskId;
  final bool completed;

  UpdateTourItem({
    @required this.tour,
    @required this.taskId,
    @required this.completed,
  });
}
