// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'title_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TitleInfo _$TitleInfoFromJson(Map<String, dynamic> json) {
  return TitleInfo(
    title: json['title'] as String,
    type: _$enumDecode(_$TourTypeEnumMap, json['type']),
  );
}

Map<String, dynamic> _$TitleInfoToJson(TitleInfo instance) => <String, dynamic>{
      'title': instance.title,
      'type': _$TourTypeEnumMap[instance.type],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

const _$TourTypeEnumMap = {
  TourType.GuidedTour: 'GuidedTour',
  TourType.AutoGuidedTour: 'AutoGuidedTour',
  TourType.TourPoint: 'TourPoint',
};
