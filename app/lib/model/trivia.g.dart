// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trivia.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Trivia _$TriviaFromJson(Map<String, dynamic> json) {
  return Trivia(
    id: json['id'] as String,
    type: json['type'] as String,
    text: json['text'] as String,
    title: json['title'] as String,
    mediaUrl: json['mediaUrl'] as String,
    isSaved: json['isSaved'] as bool,
  )
    ..localMediaFilename = json['localMediaFilename'] as String
    ..appDirPath = json['appDirPath'] as String;
}

Map<String, dynamic> _$TriviaToJson(Trivia instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'text': instance.text,
      'mediaUrl': instance.mediaUrl,
      'title': instance.title,
      'localMediaFilename': instance.localMediaFilename,
      'appDirPath': instance.appDirPath,
      'isSaved': instance.isSaved,
    };
