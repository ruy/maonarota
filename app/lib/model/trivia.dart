import 'package:json_annotation/json_annotation.dart';

part 'trivia.g.dart';

@JsonSerializable(nullable: false)
class Trivia {
  final String id;
  final String type;
  final String text;
  final String mediaUrl;
  String title;
  String localMediaFilename;
  String appDirPath;
  bool isSaved;

  Trivia({
    this.id,
    this.type,
    this.text,
    this.title,
    this.mediaUrl,
    this.isSaved = false,
  }) : localMediaFilename =
            'MaoNaRota_TR_$id.' + (type == 'audio' ? 'mp3' : 'jpg');

  String get media =>
      isSaved ? (appDirPath + '/' + localMediaFilename) : mediaUrl;

  factory Trivia.fromJson(Map<String, dynamic> json) => _$TriviaFromJson(json);
  Map<String, dynamic> toJson() => _$TriviaToJson(this);
}
