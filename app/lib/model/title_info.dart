import 'package:mao_na_rota/model/tour.dart';
import 'package:json_annotation/json_annotation.dart';

part 'title_info.g.dart';

@JsonSerializable(nullable: false)
class TitleInfo {
  final String title;
  final TourType type;

  TitleInfo({
    this.title,
    this.type,
  });

  factory TitleInfo.fromJson(Map<String, dynamic> json) =>
      _$TitleInfoFromJson(json);
  Map<String, dynamic> toJson() => _$TitleInfoToJson(this);
}
