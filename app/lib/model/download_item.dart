import 'package:json_annotation/json_annotation.dart';

part 'download_item.g.dart';

@JsonSerializable(nullable: false)
class DownloadItem {
  final String url;
  final String id;
  bool completed;

  DownloadItem({
    this.url,
    this.id,
    this.completed = false,
  });

  bool get isComplete => completed;

  factory DownloadItem.fromJson(Map<String, dynamic> json) =>
      _$DownloadItemFromJson(json);
  Map<String, dynamic> toJson() => _$DownloadItemToJson(this);
}
