import 'package:mao_na_rota/model/download_item.dart';
import 'package:mao_na_rota/model/title_info.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:json_annotation/json_annotation.dart';

part 'tour.g.dart';

enum TourType {
  GuidedTour,
  AutoGuidedTour,
  TourPoint,
}

@JsonSerializable(nullable: false)
class Tour {
  final String id;
  List<TourPoint> points;
  final String pictureUrl;
  final String guideName;
  final double duration;
  final double distance;
  final TourType type;
  final TitleInfo titleInfo;
  final String description;
  final String date;
  final String audioDescriptionUrl;
  final String mapUrl;
  final String updatedAt;
  String localAudioDescriptionFilename;
  String localPictureFilename;
  String localMapFilename;
  String _appDirPath;
  List<DownloadItem> localFiles = [];
  bool shouldBeSaved;

  Tour({
    this.id,
    this.points,
    this.pictureUrl,
    this.titleInfo,
    this.guideName,
    this.duration,
    this.distance,
    this.type,
    this.description,
    this.date,
    this.audioDescriptionUrl,
    this.mapUrl,
    this.updatedAt,
    this.shouldBeSaved = false,
  })  : localPictureFilename = 'MaoNaRota_T_${id}_picture.jpg',
        localAudioDescriptionFilename = 'MaoNaRota_T_${id}_audio.mp3',
        localMapFilename = 'MaoNaRota_T_${id}_map'; // TODO fix local map

  int get savingProgress {
    if (localFiles == null || localFiles.isEmpty) return 0;

    return ((localFiles.where((item) => item.isComplete).length /
                localFiles.length) *
            100)
        .toInt();
  }

  bool get isSaved {
    if (localFiles == null || localFiles.isEmpty) return false;

    return localFiles.every((item) => item.isComplete);
  }

  bool get isGuided => guideName != null;
  DateTime get lastUpdated =>
      updatedAt != null ? DateTime.parse(updatedAt) : null;
  DateTime get tourDate =>
      (date != null && date.isNotEmpty) ? DateTime.parse(date) : null;

  String get picture =>
      isSaved ? (_appDirPath + '/' + localPictureFilename) : pictureUrl;
  String get audio => isSaved
      ? (_appDirPath + '/' + localAudioDescriptionFilename)
      : audioDescriptionUrl;

  set appDirPath(String path) {
    _appDirPath = path;
    for (TourPoint point in points) {
      point.appDirPath = path;
    }
  }

  factory Tour.fromJson(Map<String, dynamic> json) => _$TourFromJson(json);
  Map<String, dynamic> toJson() => _$TourToJson(this);
}
