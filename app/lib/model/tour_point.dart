import 'package:mao_na_rota/model/title_info.dart';
import 'package:mao_na_rota/model/trivia.dart';
import 'package:json_annotation/json_annotation.dart';

part 'tour_point.g.dart';

@JsonSerializable(nullable: false)
class TourPoint {
  final String id;
  final List<String> pictureUrls;
  List<Trivia> trivia;
  final TitleInfo titleInfo;
  final double latitude;
  final double longitude;
  final String description;
  final String audioDescriptionUrl;
  String localAudioDescriptionFilename;
  List<String> localPictureFilenames;
  String _appDirPath;
  bool isSaved;

  TourPoint({
    this.id,
    this.pictureUrls,
    this.titleInfo,
    this.trivia,
    this.latitude,
    this.longitude,
    this.description,
    this.audioDescriptionUrl,
    this.isSaved = false,
  })  : localPictureFilenames = Iterable.generate(pictureUrls.length)
            .map((i) => 'MaoNaRota_TP_${id}_picture_${i + 1}.jpg')
            .toList(),
        localAudioDescriptionFilename = 'MaoNaRota_TP_${id}_audio.mp3';

  set appDirPath(String path) {
    _appDirPath = path;
    for (Trivia triviaItem in trivia) {
      triviaItem.appDirPath = path;
    }
  }

  List<String> get pictures => isSaved
      ? (localPictureFilenames.map((filename) => _appDirPath + '/' + filename))
          .toList()
      : pictureUrls;
  String get audio => isSaved
      ? (_appDirPath + '/' + localAudioDescriptionFilename)
      : audioDescriptionUrl;

  factory TourPoint.fromJson(Map<String, dynamic> json) =>
      _$TourPointFromJson(json);
  Map<String, dynamic> toJson() => _$TourPointToJson(this);
}
