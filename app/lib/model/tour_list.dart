import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:mao_na_rota/model/tour.dart';
import 'package:mao_na_rota/model/tour_point.dart';
import 'package:mao_na_rota/model/trivia.dart';
import 'package:mao_na_rota/model/update_tour_item.dart';
import 'package:mao_na_rota/repositories/tour_repository.dart';
import 'package:meta/meta.dart';
import 'package:synchronized/synchronized.dart';

class TourListModel extends ChangeNotifier {
  final TourRepository repository;
  // Lock updateLock = new Lock();
  Lock updateDownloadLock = new Lock();

  List<Tour> _tours;

  List<Tour> get tours => _tours;
  List<Tour> get guidedTours =>
      (_tours ?? []).where((tour) => tour.isGuided).toList();
  List<Tour> get autoGuidedTours =>
      (_tours ?? []).where((tour) => !tour.isGuided).toList();

  bool _isLoading = false;
  bool _hasFailed = false;

  bool get isLoading => _isLoading;
  bool get hasError => _hasFailed;

  TourListModel({
    @required this.repository,
    List<Tour> tours,
  }) {
    _tours = tours ?? [];

    repository.updateStream.listen((updateTourItem) {
      updateTourDownloadProgress(updateTourItem);
    });
  }

  Future loadTours() {
    _isLoading = true;
    notifyListeners();

    return repository.getTours().then((loadedTours) {
      _tours = loadedTours;
      _isLoading = false;
      _hasFailed = false;
      notifyListeners();
    }).catchError((error) {
      _isLoading = false;
      _hasFailed = true;
      notifyListeners();
    });
  }

  Future<void> updateTourDownloadProgress(UpdateTourItem updateTourItem) async {
    if (!updateTourItem.completed) return;

    await updateDownloadLock.synchronized(() async {
      Tour tour = updateTourItem.tour;
      int tourIndex = tour.localFiles.indexWhere(
          (downloadItem) => downloadItem.id == updateTourItem.taskId);

      tour.localFiles[tourIndex].completed = updateTourItem.completed;

      if (tour.isSaved) {
        for (TourPoint point in tour.points) {
          point.isSaved = true;
          for (Trivia trivia in point.trivia) {
            trivia.isSaved = true;
          }
        }
      }

      updateTour(tour);
    });
  }

  Future<void> updateTour(Tour tour) async {
    assert(tour != null);
    assert(tour.id != null);
    // await updateLock.synchronized(() async {
    var oldTour = _tours.firstWhere((oldTour) => oldTour.id == tour.id);
    var replaceIndex = _tours.indexOf(oldTour);
    _tours.replaceRange(replaceIndex, replaceIndex + 1, [tour]);
    notifyListeners();
    _uploadItems();
    // });
  }

  void removeDownloadedTour(Tour tour) async {
    assert(tour != null);
    assert(tour.id != null);
    tour.shouldBeSaved = false;
    await updateTour(tour);
    Tour tourWithoutLocalAssets = await repository.removeDownloadedTour(tour);
    updateTour(tourWithoutLocalAssets);
  }

  void downloadTour(Tour tour) async {
    assert(tour != null);
    assert(tour.id != null);
    tour.shouldBeSaved = true;
    await updateTour(tour);
    Tour downloadedTour = await repository.downloadTour(tour);
    updateTour(downloadedTour);
  }

  void _uploadItems() {
    repository.saveTours(_tours);
  }

  Tour tourById(String id) {
    return _tours.firstWhere((tour) => tour.id == id, orElse: () => null);
  }

  Tour tourByTourPointId(String tourPointId) {
    return _tours.firstWhere(
        (tour) => tour.points.any((point) => point.id == tourPointId),
        orElse: () => null);
  }

  Trivia triviaByTriviaId(String triviaId) {
    for (Tour tour in _tours) {
      for (TourPoint point in tour.points) {
        for (Trivia trivia in point.trivia) {
          if (trivia.id == triviaId) {
            return trivia;
          }
        }
      }
    }
    return null;
  }

  Tour tourByTriviaId(String triviaId) {
    return _tours.firstWhere(
        (tour) => tour.points.any(
            (point) => point.trivia.any((trivia) => trivia.id == triviaId)),
        orElse: () => null);
  }
}
