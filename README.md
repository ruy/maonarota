Este é um projeto em parceria com <>, com o intuito de fornecer, tanto gratuitamente, quanto com código-fonte aberto, uma plataforma para que roteiros históricos e culturais possam ser disponibilizados sem custos para a população.
  
Embora inicialmente planejada para uso na cidade de Campinas, o serviço pode ser adaptado e implantado em qualquer localização, mediante  a introdução de conteúdo por organizações ligadas à cultura. 

Os APKs estão disponíveis no link: https://drive.google.com/drive/folders/1QNV-vA_LLFEI9iG54q-CG_dKeOwnQiPh?usp=sharing

O Web App pode ser testado no link: https://mao-na-rota.herokuapp.com/
