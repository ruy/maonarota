import {createMuiTheme} from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#222222",
    },
    secondary: {
      main: "#DC5586",
    },
    text: {
      primary: "#C4C4C4",
      secondary: "#7B7B7B",
    },
  },
  typography: {
    htmlFontSize: 16,
    fontFamily: "'Nunito Sans', sans-serif",
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
  },
  overrides: {
    MuiInput: {
      input: {
        paddingLeft: 0,
        margin: 0,
      },
      root: {
        paddingLeft: 0,
        margin: 0,
      },
    },
    MuiButton: {
      root: {
        backgroundColor: "#222222",
        color: "#FFFFFF",
        "&:hover": {
          backgroundColor: "#222222",
        },
      },
      outlined: {
        borderRadius: 25,
        backgroundColor: "#DC5586",
        color: "#FFFFFF",
        fontWeight: 600,
        height: 36,
        "&:hover": {
          backgroundColor: "#DC5586",
        },
      },
      text: {
        outlined: {
          padding: "0px 50px",
          textAlign: "center",
        },
      },
    },
  },
});

export default theme;
