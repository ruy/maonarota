import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import rootStore from './features/rootStore';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';

ReactDOM.render(
	<React.StrictMode>
		<Provider store={rootStore()}>
			<Router>
				<Route path="/" component={App} />
			</Router>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);

serviceWorker.unregister();
