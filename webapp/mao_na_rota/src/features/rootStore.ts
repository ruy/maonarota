import { Store, applyMiddleware, createStore, Middleware } from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './rootReducer';
import IStore from './IStore';

export default function rootStore(): Store<IStore> {
  const middlewares: Middleware[] = [thunk];

  return createStore(rootReducer(), composeWithDevTools(applyMiddleware(...middlewares)));
}
