import { Dispatch } from 'redux';

import { createAction } from '../IAction';
import * as api from '../../helpers/api';
import HttpErrorResponseModel from '../../models/HttpErrorResponseModel';
import UserModel from './models/UserModel';

export const REQUEST_LOGIN = 'AuthAction.REQUEST_LOGIN';
export const REQUEST_LOGIN_FINISHED = 'AuthAction.REQUEST_LOGIN_FINISHED';

export const LOGOUT = 'AuthAction.LOGOUT';

export const REQUEST_SESSION = 'AuthAction.REQUEST_SESSION';
export const REQUEST_SESSION_FINISHED = 'AuthAction.REQUEST_SESSION_FINISHED';

export function login(email: string, password: string) {
  return async (dispatch: Dispatch<any>) => {
    dispatch(createAction<undefined>(REQUEST_LOGIN));

    // const model: undefined | HttpErrorResponseModel = await api.login(email, password);
    let model: undefined | HttpErrorResponseModel;

    const isError: boolean = model instanceof HttpErrorResponseModel;

    // if (!isError) {
    //   dispatch(getSession());
    // }

    dispatch(
      createAction<undefined | HttpErrorResponseModel>(
        REQUEST_LOGIN_FINISHED,
        model,
        isError
      )
    );
  };
}

export function logout() {
  return async (dispatch: Dispatch<any>) => {
    dispatch(createAction(LOGOUT));
  };
}

export function getSession() {
  return async (dispatch: Dispatch<any>) => {
    dispatch(createAction<undefined>(REQUEST_SESSION));

    // const [model, isError]: [UserModel | HttpErrorResponseModel, boolean] = await api.getUser()
    //   .then((response) => {
    //     return response instanceof HttpErrorResponseModel ?
    //       [response, true] : [new UserModel(response), false];
    //   });

    // dispatch(createAction<UserModel | HttpErrorResponseModel>(REQUEST_SESSION_FINISHED, model, isError));

    dispatch(
      createAction<UserModel | HttpErrorResponseModel>(
        REQUEST_SESSION_FINISHED,
        new UserModel({}),
        false
      )
    );
  };
}
