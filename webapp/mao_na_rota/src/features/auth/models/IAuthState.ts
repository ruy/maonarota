import UserModel from "./UserModel";

export default interface IAuthState {
  user: UserModel | null;
  authorized: boolean;
}
