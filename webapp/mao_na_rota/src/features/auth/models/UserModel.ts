import { BaseModel } from 'sjs-base-model';

/*
    // Returned Api Data Sample
    {
      "uid": "k8ovbeh2pJuw/l0CnD8ugpvRVmgWsh8s",
      "legalName": "DieselBank Soluções em Tecnologia LTDA.",
    }
 */

export default class UserModel extends BaseModel {
  public readonly uid: string = '';

  /*
   * Client-Side properties (Not from API)
   */
  // public noneApiProperties: unknown = null;

  constructor(data: Partial<UserModel>) {
    super();

    this.update(data);
  }
}
