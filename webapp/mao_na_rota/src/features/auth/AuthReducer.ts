import { Reducer } from 'redux';

import IAuthState from './models/IAuthState';
import baseReducer from '../baseReducer';
import IAction from '../IAction';
import * as AuthAction from './AuthAction';
import UserModel from './models/UserModel';

export const initialState: IAuthState = {
  user: null,
  authorized: false,
};

const authReducer: Reducer = baseReducer(initialState, {
  [AuthAction.REQUEST_LOGIN_FINISHED](state: IAuthState): IAuthState {
    return { ...state, authorized: true };
  },

  [AuthAction.LOGOUT](state: IAuthState): IAuthState {
    return { ...state, user: null, authorized: false };
  },

  [AuthAction.REQUEST_SESSION_FINISHED](
    state: IAuthState,
    action: IAction<UserModel>
  ) {
    return { ...state, user: action.payload!, authorized: true };
  },
});

export default authReducer;
