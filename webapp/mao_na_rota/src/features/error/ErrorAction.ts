import IAction, { createAction } from "../IAction";

export const REMOVE: string = 'ErrorAction.REMOVE';
export const CLEAR_ALL: string = 'ErrorAction.CLEAR_ALL';

export const removeById = (id: string): IAction<string> => {
  return createAction(REMOVE, id);
};

export const clearAll = (): IAction<undefined> => {
  return createAction(CLEAR_ALL);
};
