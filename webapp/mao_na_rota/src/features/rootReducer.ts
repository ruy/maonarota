import { Reducer, ReducersMapObject, combineReducers } from "redux";

import IStore from './IStore'
import authReducer from "./auth/AuthReducer";
import toursReducer from "./tours/TourReducer";
import requestingReducer from "./requesting/RequestingReducer";
import errorReducer from "./error/ErrorReducer";

export default function rootReducer(): Reducer<IStore> {
  const reducerMap: ReducersMapObject<IStore> = {
    error: errorReducer,
    requesting: requestingReducer,
    auth: authReducer,
    tours: toursReducer,
  };

  return combineReducers(reducerMap);
}
