import IAuthState from './auth/models/IAuthState'
import ITourState from './tours/models/ITourState'
import IRequestingState from './requesting/models/IRequestingState'
import IErrorState from './error/models/IErrorState'

export default interface IStore {
  readonly error: IErrorState;
  readonly requesting: IRequestingState;
  readonly auth: IAuthState;
  readonly tours: ITourState;
}
