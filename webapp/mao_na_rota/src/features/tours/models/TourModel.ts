import { BaseModel } from 'sjs-base-model';
import TourPointModel from './TourPointModel';

/*
    // Returned Api Data Sample
    {
      "id": "1+hxtpw1CGWrl4QP804tw1Xu5kSn2krY",
      "updatedAt": "",
    }
 */

export default class TourModel extends BaseModel {
  public readonly id: string = '';
  public readonly updatedAt: string = '';
  public points: TourPointModel[] = [];
  public pictureUrl: string = '';
  public guideName: string = '';
  public duration: number = 0;
  public distance: number = 0;
  public title: string = '';
  public description: string = '';
  public date: string = '';
  public audioDescriptionUrl: string = '';
  public mapUrl: string = '';
  /*
   * Client-Side properties (Not from API)
   */
  // public noneApiProperties: unknown = null;

  constructor(data: Partial<TourModel>) {
    super();

    if (!!data.points) {
      for (let index in data.points!) {
        data.points[index] = new TourPointModel(data.points[index]);
      }
    }

    this.update(data);
  }
}
