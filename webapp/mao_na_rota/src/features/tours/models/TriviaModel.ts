import { BaseModel } from 'sjs-base-model';

/*
    // Returned Api Data Sample
    {
      "id": "1+hxtpw1CGWrl4QP804tw1Xu5kSn2krY",
      "updatedAt": "",
    }
 */

export default class TriviaModel extends BaseModel {
  public readonly id: string = '';
  public readonly updatedAt: string = '';
  public readonly title: string = '';
  public readonly description: string = '';
  public readonly type: string = 'audio';

  /*
   * Client-Side properties (Not from API)
   */
  // public noneApiProperties: unknown = null;

  constructor(data: Partial<TriviaModel>) {
    super();

    this.update(data);
  }
}
