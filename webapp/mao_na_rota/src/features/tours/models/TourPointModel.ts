import { BaseModel } from 'sjs-base-model';
import TriviaModel from './TriviaModel';

/*
    // Returned Api Data Sample
    {
      "id": "1+hxtpw1CGWrl4QP804tw1Xu5kSn2krY",
      "updatedAt": "",
    }
 */

export default class TourPointModel extends BaseModel {
  public readonly id: string = '';
  public readonly updatedAt: string = '';
  public readonly title: string = '';
  public readonly description: string = '';
  public readonly trivia: TriviaModel[] = [];
  public readonly pictureUrls: string[] = [];
  /*
   * Client-Side properties (Not from API)
   */
  // public noneApiProperties: unknown = null;

  constructor(data: Partial<TourPointModel>) {
    super();

    this.update(data);
  }
}
