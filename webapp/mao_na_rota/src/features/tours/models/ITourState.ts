import TourModel from "./TourModel";

export default interface ITransactionState {
  tours: TourModel[] | null;
}
