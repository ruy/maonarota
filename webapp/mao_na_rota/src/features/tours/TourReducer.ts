import { Reducer } from 'redux';

import ITourState from './models/ITourState';
import baseReducer from '../baseReducer';
import IAction from '../IAction';
import * as TourAction from './TourAction';
import TourModel from './models/TourModel';
import TourPointModel from './models/TourPointModel';
import { randomInt } from '../../helpers/random';

export const initialState: ITourState = {
  tours: null,
};

const tourReducer: Reducer = baseReducer(initialState, {
  [TourAction.REQUEST_TOUR_FINISHED](
    state: ITourState,
    action: IAction<TourModel[]>
  ): ITourState {
    return { ...state, tours: action.payload! };
  },

  [TourAction.CREATE_TOUR_FINISHED](
    state: ITourState,
    action: IAction<Partial<TourModel>>
  ): ITourState {
    const tourModel: TourModel = new TourModel({
      ...action.payload!,
      id: randomInt(2, 1000).toString(),
    });
    return {
      ...state,
      tours: state.tours === null ? [tourModel] : [...state.tours, tourModel],
    };
  },

  [TourAction.SAVE_TOUR_FINISHED](
    state: ITourState,
    action: IAction<Partial<TourModel>>
  ): ITourState {
    if (state.tours !== null && state.tours.length !== 0) {
      const index: number = state.tours!.findIndex(
        (tour) => tour.id === action.payload!.id
      );

      if (index !== -1) {
        state.tours[index] = {
          ...state.tours[index],
          ...action.payload!,
        } as TourModel;
      }
    }

    return state;
  },

  [TourAction.REMOVE_TOUR_FINISHED](
    state: ITourState,
    action: IAction<string>
  ): ITourState {
    if (state.tours !== null && state.tours.length !== 0) {
      const index: number = state.tours!.findIndex(
        (tour) => tour.id === action.payload!
      );

      if (index !== -1) {
        state.tours.splice(index, 1);
      }
    }

    return state;
  },

  [TourAction.REMOVE_TOUR_POINT_FINISHED](
    state: ITourState,
    action: IAction<{
      tourId: string;
      tourPointId: string;
    }>
  ): ITourState {
    if (state.tours !== null && state.tours.length !== 0) {
      const index: number = state.tours.findIndex(
        (tour) => tour.id === action.payload!.tourId
      );

      if (index !== -1) {
        const pointIndex: number = state.tours![index].points.findIndex(
          (tourPoint) => tourPoint.id === action.payload!.tourPointId
        );

        state.tours[index].points.splice(pointIndex, 1);
      }
    }

    return state;
  },

  [TourAction.SAVE_TOUR_POINT_FINISHED](
    state: ITourState,
    action: IAction<{
      tourId: string;
      tourPoint: Partial<TourPointModel>;
    }>
  ): ITourState {
    if (state.tours !== null && state.tours.length !== 0) {
      const index: number = state.tours!.findIndex(
        (tour) => tour.id === action.payload!.tourId
      );

      if (index !== -1) {
        const pointIndex: number = state.tours![index].points.findIndex(
          (tourPoint) => tourPoint.id === action.payload!.tourPoint.id
        );

        state.tours[index].points[pointIndex] = {
          ...state.tours[index].points[pointIndex],
          ...action.payload!.tourPoint,
        } as TourPointModel;
      }
    }

    return state;
  },

  [TourAction.CREATE_TOUR_POINT_FINISHED](
    state: ITourState,
    action: IAction<{
      tourId: string;
      tourPoint: Partial<TourPointModel>;
    }>
  ): ITourState {
    if (state.tours !== null && state.tours.length !== 0) {
      const index: number = state.tours!.findIndex(
        (tour) => tour.id === action.payload!.tourId
      );

      if (index !== -1) {
        state.tours[index].points.push(
          new TourPointModel({
            ...action.payload!.tourPoint,
            id: randomInt(2, 1000).toString(),
          })
        );
      }
    }

    return state;
  },
});

export default tourReducer;
