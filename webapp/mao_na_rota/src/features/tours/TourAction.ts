import { Dispatch } from 'redux';

import IStore from '../IStore';
import { createAction } from '../IAction';
import Tour from './models/TourModel';
// import * as api from "../../helpers/api";
import * as mock from '../../helpers/mock';

import HttpErrorResponseModel from '../../models/HttpErrorResponseModel';
import TourModel from './models/TourModel';
import TourPointModel from './models/TourPointModel';

export const REQUEST_TOUR = 'TourAction.REQUEST_TOUR';
export const REQUEST_TOUR_FINISHED = 'TourAction.REQUEST_TOUR_FINISHED';

export const CREATE_TOUR = 'TourAction.CREATE_TOUR';
export const CREATE_TOUR_FINISHED = 'TourAction.CREATE_TOUR_FINISHED';

export const SAVE_TOUR = 'TourAction.SAVE_TOUR';
export const SAVE_TOUR_FINISHED = 'TourAction.SAVE_TOUR_FINISHED';

export const REMOVE_TOUR = 'TourAction.REMOVE_TOUR';
export const REMOVE_TOUR_FINISHED = 'TourAction.REMOVE_TOUR_FINISHED';

export const REMOVE_TOUR_POINT_ = 'TourAction.REMOVE_TOURPOINT_';
export const REMOVE_TOUR_POINT_FINISHED =
  'TourAction.REMOVE_TOUR_POINT_FINISHED';

export const SAVE_TOUR_POINT_ = 'TourAction.SAVE_TOURPOINT_';
export const SAVE_TOUR_POINT_FINISHED = 'TourAction.SAVE_TOUR_POINT_FINISHED';

export const CREATE_TOUR_POINT = 'TourAction.CREATE_TOUR_POINT';
export const CREATE_TOUR_POINT_FINISHED =
  'TourAction.CREATE_TOUR_POINT_FINISHED';

export function getTours() {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(createAction<undefined>(REQUEST_TOUR));

    const [model, isError]: [
      Tour[] | HttpErrorResponseModel,
      boolean
    ] = await mock.getTours().then((response) => {
      return response instanceof HttpErrorResponseModel
        ? [response, true]
        : [response.map((json: any) => new Tour(json)), false];
    });

    dispatch(
      createAction<Tour[] | HttpErrorResponseModel>(
        REQUEST_TOUR_FINISHED,
        model,
        isError
      )
    );
  };
}

export function createTour(tour: Partial<TourModel>) {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(
      createAction<Partial<TourModel> | HttpErrorResponseModel>(
        CREATE_TOUR_FINISHED,
        tour
      )
    );
  };
}

export function saveTour(tour: Partial<TourModel>) {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(
      createAction<Partial<TourModel> | HttpErrorResponseModel>(
        SAVE_TOUR_FINISHED,
        tour
      )
    );
  };
}

export function removeTour(tourId: string) {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(
      createAction<string | HttpErrorResponseModel>(
        REMOVE_TOUR_FINISHED,
        tourId
      )
    );
  };
}

export function removeTourPoint(tourId: string, tourPointId: string) {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(
      createAction<
        | {
            tourId: string;
            tourPointId: string;
          }
        | HttpErrorResponseModel
      >(REMOVE_TOUR_POINT_FINISHED, {
        tourId,
        tourPointId,
      })
    );
  };
}

export function saveTourPoint(
  tourPoint: Partial<TourPointModel>,
  tourId: string
) {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(
      createAction<{
        tourId: string;
        tourPoint: Partial<TourPointModel>;
      }>(SAVE_TOUR_POINT_FINISHED, {
        tourId,
        tourPoint,
      })
    );
  };
}

export function createTourPoint(
  tourPoint: Partial<TourPointModel>,
  tourId: string
) {
  return async (dispatch: Dispatch<any>, getState: () => IStore) => {
    dispatch(
      createAction<
        | {
            tourId: string;
            tourPoint: Partial<TourPointModel>;
          }
        | HttpErrorResponseModel
      >(CREATE_TOUR_POINT_FINISHED, {
        tourId,
        tourPoint,
      })
    );
  };
}
