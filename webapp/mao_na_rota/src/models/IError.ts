// Default error interface for actions that doesn't need
//   to make an http request

export default interface IError {
  readonly id: string;
  readonly message: string;
}
