import React from 'react';
import { RouteProps, Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import IStore from '../features/IStore';

interface IStateToProps {
	readonly authorized: boolean;
}

const mapStateToProps = ({ auth }: IStore): IStateToProps => ({
	authorized: auth.authorized
});

const AuthRoute = ({ authorized, ...props }: RouteProps & IStateToProps) =>
	authorized ? <Route {...props} /> : <Redirect to="/login" />;

export default connect(mapStateToProps)(AuthRoute);
