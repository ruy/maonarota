/*
 * Base is the default environment for production.
 * Add everything here and override value in other files if needed.
 */
export default {
  env: process.env.NODE_ENV || 'development',
  maonarota: {
    baseUri: undefined,
    timeout: 3000,
    retries: 3,
    responseType: 'json',
    maxRedirects: 0,
    login: '/login',
    tours: {
      get: '/tours',
      getById: '/tour/:tour-id',
      updateTour: '/tour/update/',
      createTour: '/tour/create/',
      removeTour: '/tour/remove/',
    },
  },
};

