import _ from "lodash";

import base from "./base";
import development from "./development";
import production from "./production";
import test from "./test";

const configs = { development, production, test };
const env = process.env.NODE_ENV || 'development';

const envConfig = configs[env];

const baseConfig = _.merge({}, base);
const mergedConfig = _.merge(baseConfig, envConfig) as typeof base;

export default mergedConfig;
