import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
// import background from '../assets/images/login-background.svg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: theme.palette.primary.main,
      width: "100vw",
      height: "100vh",
      spacing: 0,
    },
    subContainer: {
      display: "flex",
      backgroundColor: theme.palette.primary.main,
      flexDirection: "column",
      justifyContent: "center",
      alignContent: "center",
      textAlign: "center",
    },
    logo: {
      maxWidth: "100%",
      borderRadius: "20px",
    },
    button: {
      width: "5px",
    },
    arrow: {
      color: theme.palette.text.primary,
      margin: "0px 5px",
    },
    carouselItem: {
      width: "65%",
      alignItems: "center",
    },
    carousel: {
      alignItems: "center",
      alignSelf: "center",
      alignContent: "center",
      width: "100%",
      marginBottom: "15px",
    },
    titleText: {
      marginTop: "25px",
      color: theme.palette.secondary.main,
      fontSize: 30,
      fontWeight: 500,
    },
    centerText: {
      textAlign: "center",
      alignItems: "center",
      width: "100vw",
      height: "100vh",
    },
    warningText: {
      color: theme.palette.text.primary,
      fontSize: 21,
      marginTop: "5px",
      marginBottom: "30px",
      width: "30vw",
    },
  })
);

export default useStyles;
