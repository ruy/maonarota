import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { Switch } from 'react-router-dom';

import IStore from '../../features/IStore';
import ToursPage from './ToursPage';
import TourModel from '../../features/tours/models/TourModel';
import * as TourAction from '../../features/tours/TourAction';
import * as AuthAction from '../../features/auth/AuthAction';
import { selectRequesting } from '../../features/requesting/RequestingSelector';
import { hasErrors } from '../../features/error/ErrorSelector';
import LoadingPage from '../../views/components/LoadingPage';
import AuthRoute from '../../routes/AuthRoute';
import ErrorPage from '../../views/components/ErrorPage';

const TourContainer = Loadable({
  loader: () => import('../../views/tour-page/TourPageContainer'),
  loading: (props: any) =>
    LoadingPage({ ...props, retry: () => window.location.reload(true) }),
});

const TourContainerCreation = Loadable({
  render(loaded, props) {
    let Component = loaded.default;
    return <Component {...props} creation />;
  },
  loader: () => import('../../views/tour-page/TourPageContainer'),
  loading: (props: any) =>
    LoadingPage({ ...props, retry: () => window.location.reload(true) }),
});

interface IStateToProps {
  tours: TourModel[] | null;
  loading: boolean;
  hasError: boolean;
}

interface IDispatchToProps {
  getTours: () => void;
  onCreateTour: () => void;
  logout: () => void;
}

const mapStateToProps = (state: IStore): IStateToProps => ({
  tours: state.tours.tours,
  loading: selectRequesting(state, [TourAction.REQUEST_TOUR]),
  hasError: hasErrors(state, [TourAction.REQUEST_TOUR_FINISHED]),
});

const mapDispatchToProps = (
  dispatch: Dispatch,
  ownProps: any
): IDispatchToProps => ({
  getTours: () => dispatch<any>(TourAction.getTours()),
  onCreateTour: () => ownProps.history.push('/rota/nova'),
  logout: () => dispatch<any>(AuthAction.logout()),
});

const ToursPageContainer = (props: IStateToProps & IDispatchToProps) => {
  const { getTours }: { getTours: () => void } = props;

  React.useEffect(() => {
    getTours();
  }, []);

  return (
    <Switch>
      <AuthRoute key={1} path='/' exact>
        <ToursPage {...props} />
      </AuthRoute>
      <AuthRoute
        key={2}
        path='/rota/nova'
        exact
        component={TourContainerCreation}
      />
      <AuthRoute key={3} path='/rota/:tourId' component={TourContainer} />
      <AuthRoute key={4} component={ErrorPage} />
    </Switch>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ToursPageContainer);
