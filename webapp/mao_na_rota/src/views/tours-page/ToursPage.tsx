import React from 'react';
import {
  Grid,
  Button,
  Typography,
  IconButton,
  CircularProgress,
} from '@material-ui/core';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { Link } from 'react-router-dom';

import useStyles from './ToursPage.styles';
import Header from '../components/Header';
import theme from '../../theme';
import TourModel from '../../features/tours/models/TourModel';

interface IProps {
  tours: TourModel[] | null;
  loading: boolean;
  hasError: boolean;
  onCreateTour: () => void;
  logout: () => void;
}

interface IState {
  index: number;
}

const initialState: IState = {
  index: 0,
};

const ArrowLeft = (classes: any) => {
  return (
    <IconButton aria-label='next-tour' className={classes.arrow}>
      <NavigateBeforeIcon fontSize='large' />
    </IconButton>
  );
};

const ArrowRight = (classes: any) => {
  return (
    <IconButton aria-label='next-tour' className={classes.arrow}>
      <NavigateNextIcon fontSize='large' />
    </IconButton>
  );
};

const RetryWarning = (classes: any) => {
  return (
    <div className={classes.subContainer}>
      <Grid
        container
        direction='column'
        justify='center'
        alignContent='center'
        className={classes.centerText}
      >
        <Typography className={classes.titleText}>Ops!</Typography>
        <Typography className={classes.warningText}>
          Não foi possível buscar as rotas existentes
        </Typography>
        <Button variant='outlined' style={{ width: '200px' }}>
          TENTAR NOVAMENTE
        </Button>
      </Grid>
    </div>
  );
};

const EmptyWarning = (classes: any) => {
  return (
    <div className={classes.subContainer}>
      <Grid
        container
        direction='column'
        justify='center'
        alignContent='center'
        className={classes.centerText}
      >
        <Typography className={classes.titleText}>Ops!</Typography>
        <Typography className={classes.warningText}>
          Ainda não há rotas registradas, tente criar uma no botão no topo da
          página!
        </Typography>
      </Grid>
    </div>
  );
};

const renderLoading = (classes: any) => {
  return (
    <div className={classes.subContainer}>
      <CircularProgress color='secondary' />
      <Typography className={classes.titleText}>Carregando..</Typography>
    </div>
  );
};

const ToursPage = (props: IProps) => {
  const classes = useStyles();
  const [state, setState] = React.useState<IState>(initialState);

  return (
    <Grid
      container
      direction='column'
      justify='center'
      alignContent='center'
      className={classes.container}
    >
      <Header
        onClickLeftButton={(_) => props.onCreateTour()}
        onClickRightButton={(_) => props.logout()}
        leftButtonText='+ CRIAR ROTA'
        rightButtonText='SAIR'
        leftButtonTextColor={theme.palette.text.primary}
        rightButtonTextColor={theme.palette.text.secondary}
      />
      {props.loading ? (
        renderLoading(classes)
      ) : props.hasError || props.tours === null ? (
        RetryWarning(classes)
      ) : props.tours !== null && props.tours.length === 0 ? (
        EmptyWarning(classes)
      ) : (
        <div className={classes.carousel}>
          <Carousel
            infinite
            addArrowClickHandler
            arrowLeft={ArrowLeft(classes)}
            arrowRight={ArrowRight(classes)}
            onChange={(index: number) =>
              setState({ ...state, index: index % (props.tours?.length || 1) })
            }
          >
            {props.tours!.map((tour) => (
              <Grid
                key={tour.id}
                container
                direction='column'
                spacing={0}
                justify='center'
                alignContent='center'
                className={classes.carouselItem}
              >
                <Link to={'/rota/' + props.tours![state.index].id}>
                  <img
                    src={
                      tour.pictureUrl ||
                      'https://images.unsplash.com/photo-1562932831-afcfe48b5786'
                    }
                    alt='logo'
                    className={classes.logo}
                  />
                </Link>
                <Typography className={classes.titleText}>
                  {tour.title}
                </Typography>
              </Grid>
            ))}
          </Carousel>
        </div>
      )}
    </Grid>
  );
};

export default ToursPage;
