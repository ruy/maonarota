import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Switch, useHistory } from 'react-router-dom';

import IStore from '../../features/IStore';
import TourPointPage from './TourPointPage';
import * as TourAction from '../../features/tours/TourAction';
import TourPointModel from '../../features/tours/models/TourPointModel';
import AuthRoute from '../../routes/AuthRoute';
import ErrorPage from '../../views/components/ErrorPage';

interface IStateToProps {
  readonly tourPoint: TourPointModel;
  readonly tourId: string;
  readonly creation: boolean;
}

interface IDispatchToProps {
  readonly onRemove: (tourId: string, tourPointId: string) => void;
  readonly onSave: (tourPoint: Partial<TourPointModel>, tourId: string) => void;
}

const mapStateToProps = (state: IStore, ownProps: any): IStateToProps => ({
  tourPoint: state.tours
    .tours!.find((tour) => tour.id === ownProps.match.params.tourId)!
    .points.find(
      (tourPoint) => tourPoint.id === ownProps.match.params.tourPointId
    )!,
  tourId: ownProps.match.params.tourId!,
  creation: ownProps.creation,
});

const mapDispatchToProps = (
  dispatch: Dispatch,
  ownProps: any
): IDispatchToProps => ({
  onRemove: (tourId: string, tourPointId: string) => {
    dispatch<any>(TourAction.removeTourPoint(tourId, tourPointId));
    ownProps.history.push('/rota/' + tourId);
  },
  onSave: ownProps.creation
    ? async (tourPoint: Partial<TourPointModel>, tourId: string) => {
        await dispatch<any>(TourAction.createTourPoint(tourPoint, tourId));
        ownProps.history.push('/rota/' + tourId);
      }
    : async (tourPoint: Partial<TourPointModel>, tourId: string) => {
        await dispatch<any>(TourAction.saveTourPoint(tourPoint, tourId));
        ownProps.history.push('/rota/' + tourId);
      },
});

const TourPointPageContainer = (props: IStateToProps & IDispatchToProps) => {
  const history = useHistory();
  const {
    tourPoint,
    tourId,
  }: { tourPoint: TourPointModel; tourId: string } = props;

  React.useEffect(() => {
    if (!tourId || (!tourPoint && !props.creation)) {
      history.replace('/erro');
    }
  }, []);

  return (
    <Switch>
      <AuthRoute key={20} path='/rota/:tourId/parada/:tourPointId' exact>
        <TourPointPage {...props} />
      </AuthRoute>
      <AuthRoute key={21} path='/rota/:tourId/parada/nova' exact>
        <TourPointPage {...props} creation />
      </AuthRoute>
      <AuthRoute key={22} path='*' component={ErrorPage} />
    </Switch>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TourPointPageContainer);
