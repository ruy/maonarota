import React from 'react';
import { TextField, Button, Typography } from '@material-ui/core';
import { cloneDeep } from 'lodash';

import useStyles from '../tour-point-page/TourPointPage.styles';
import Header from '../components/Header';
import ActionButton from '../components/ActionButton';
import theme from '../../theme';
import TriviaItem from '../components/TriviaItem';
import GalleryItem from '../components/GalleryItem';
import NewGalleryItem from '../components/NewGalleryItem';
import TourPointModel from '../../features/tours/models/TourPointModel';
import TriviaModel from '../../features/tours/models/TriviaModel';

interface IProps {
  tourPoint: TourPointModel;
  tourId: string;
  creation: boolean;
  onRemove: (tourId: string, tourPointId: string) => void;
  onSave: (tourPoint: Partial<TourPointModel>, tourId: string) => void;
}

interface IState {
  tourPointDraft: Partial<TourPointModel>;
}

const TourPointPage = (props: IProps) => {
  const classes = useStyles();
  const [state, setState] = React.useState<IState>({
    tourPointDraft: { ...props.tourPoint },
  });

  React.useEffect(() => {
    if (!props.tourPoint) {
      setState((state) => {
        return {
          ...state,
          tourPointDraft: {
            ...state.tourPointDraft,
            ...props.tourPoint,
          },
        };
      });
    }
  }, [props.tourPoint]);

  return (
    <div className={classes.container}>
      <Header
        onClickLeftButton={
          !props.creation
            ? (_) => {
                props.onRemove(props.tourPoint.id, props.tourId);
              }
            : undefined
        }
        onClickRightButton={(_) =>
          props.onSave(state.tourPointDraft, props.tourId)
        }
        leftButtonText={!props.creation ? 'REMOVER PARADA' : undefined}
        rightButtonText='SALVAR'
        leftButtonTextColor={'#DC5252'}
        rightButtonTextColor={theme.palette.text.primary}
      />
      <div className={classes.infoContainer}>
        <div className={classes.gallery}>
          <div className={classes.galleryItem}>
            <NewGalleryItem
              onUpload={(picture: string) => {
                setState({
                  ...state,
                  tourPointDraft: {
                    ...state.tourPointDraft,
                    pictureUrls: [
                      picture,
                      ...(state.tourPointDraft.pictureUrls || []),
                    ],
                  },
                });
              }}
            />
          </div>
          {!!state.tourPointDraft?.pictureUrls &&
            state.tourPointDraft.pictureUrls.map((url, index) => (
              <div className={classes.galleryItem} key={'gallery-' + index}>
                <GalleryItem
                  src={url}
                  onClick={() => {
                    let newUrls: string[] = state.tourPointDraft.pictureUrls!.filter(
                      (_, currentIndex) => index !== currentIndex
                    );

                    setState({
                      ...state,
                      tourPointDraft: {
                        ...state.tourPointDraft,
                        pictureUrls: newUrls,
                      },
                    });
                  }}
                />
              </div>
            ))}
        </div>
        <Typography className={classes.title}>TÍTULO</Typography>
        <TextField
          label='Insira o título'
          fullWidth
          variant='filled'
          margin='dense'
          InputProps={{ disableUnderline: true, className: 'color: red' }}
          color='secondary'
          value={state.tourPointDraft.title}
          onChange={(
            event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) =>
            setState({
              ...state,
              tourPointDraft: {
                ...state.tourPointDraft,
                title: event.target.value,
              },
            })
          }
        />
        <Typography className={classes.descriptionLabel}>DESCRIÇÃO</Typography>
        <TextField
          label='Insira a descrição'
          fullWidth
          variant='filled'
          margin='dense'
          multiline
          InputProps={{ disableUnderline: true }}
          value={state.tourPointDraft.description}
          onChange={(
            event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) =>
            setState({
              ...state,
              tourPointDraft: {
                ...state.tourPointDraft,
                description: event.target.value,
              },
            })
          }
        />
        <ActionButton
          text={'Alterar áudio descrição'}
          onUpload={() => {}}
          icon='audio'
          className={classes.button}
        />
        <ActionButton
          text={'Alterar localização'}
          onUpload={() => {}}
          icon='map'
          className={classes.button}
        />
        <div className={classes.sectionContainer}>
          <Typography className={classes.sectionTitle}>TRIVIA</Typography>
          <Button
            onClick={() => {
              setState({
                ...state,
                tourPointDraft: {
                  ...state.tourPointDraft,
                  trivia: [
                    new TriviaModel({}),
                    ...(state.tourPointDraft.trivia || []),
                  ],
                },
              });
            }}
          >
            <Typography className={classes.sectionButton}>
              + ADICIONAR TRIVIA
            </Typography>
          </Button>
        </div>
        {!!state.tourPointDraft.trivia &&
          state.tourPointDraft.trivia.map((triviaItem, index) => {
            return (
              <TriviaItem
                key={
                  'trivia-' + (state.tourPointDraft.trivia!.length - index - 1)
                }
                trivia={triviaItem}
                onRemove={() => {
                  let newTrivia: TriviaModel[] = state.tourPointDraft.trivia!.filter(
                    (_, currentIndex) => index !== currentIndex
                  );

                  setState({
                    ...state,
                    tourPointDraft: {
                      ...state.tourPointDraft,
                      trivia: newTrivia,
                    },
                  });
                }}
                onSave={(newTriviaItem: Partial<TriviaModel>) => {
                  let newTrivia: TriviaModel[] = cloneDeep(
                    state.tourPointDraft.trivia!
                  );

                  newTrivia[index] = {
                    ...state.tourPointDraft.trivia![index],
                    ...newTriviaItem,
                  } as TriviaModel;

                  setState({
                    ...state,
                    tourPointDraft: {
                      ...state.tourPointDraft,
                      trivia: newTrivia,
                    },
                  });
                }}
              />
            );
          })}
      </div>
    </div>
  );
};

export default TourPointPage;
