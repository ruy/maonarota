import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: theme.palette.primary.main,
      width: "100vw",
      height: "100%",
      minHeight: "100vh",
      spacing: 0,
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      alignItems: "flex-start",
      flexGrow: 1,
    },
    gallery: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      width: "100%",
      marginBottom: "40px",
      overflow: "scroll",
    },
    galleryItem: {
      marginTop: "10px",
      marginRight: "20px",
    },
    headerImage: {
      height: "20vh",
      display: "flex",
      justifyContent: "flex-end",
      alignItems: "flex-end",
      width: "100vw",
      padding: "10px 30px",
      backgroundSize: "100vw",
      backgroundImage: `linear-gradient(180deg, rgba(2,0,36,0) 55%, rgba(34,34,34,1) 100%), url(${"https://images.unsplash.com/photo-1562932831-afcfe48b5786"})`,
    },
    text: {
      fontWeight: 500,
      fontSize: 18,
      color: "white",
    },
    infoContainer: {
      padding: "10px 40px 30px 40px",
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      alignItems: "flex-start",
      width: "100%",
      height: "100%",
      flexGrow: 1,
    },
    upperTitle: {
      color: theme.palette.secondary.main,
      fontSize: 20,
    },
    title: {
      color: theme.palette.secondary.main,
      fontSize: 24,
      marginBottom: "5px",
    },
    descriptionLabel: {
      marginTop: "15px",
      marginBottom: "5px",
      color: theme.palette.text.primary,
      fontSize: 20,
    },
    description: {
      marginTop: "30px",
      color: theme.palette.text.secondary,
      fontSize: 20,
    },
    sectionTitle: {
      color: theme.palette.secondary.main,
      fontSize: 20,
    },
    button: {
      marginTop: "25px",
    },
    sectionContainer: {
      marginTop: "25px",
      marginBottom: "25px",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      width: "100%",
    },
    sectionButton: {
      fontSize: 20,
      color: theme.palette.text.secondary,
    },
  })
);

export default useStyles;
