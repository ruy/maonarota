import React from 'react';
import { Button, createStyles, makeStyles, Theme } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import { toBase64 } from '../../helpers/base64';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      width: '147px',
      height: '162px',
      backgroundSize: '300%',
      backgroundPosition: 'center',
      borderRadius: 10,
      border: '2px solid #7B7B7B',
    },
    icon: {
      fontSize: 24,
    },
    buttonContainer: {
      backgroundColor: theme.palette.primary.main,
      '&:hover': {
        backgroundColor: theme.palette.primary.main,
      },
    },
  })
);

interface IProps {
  onUpload: (file: string) => void;
}

const NewGalleryItem = (props: IProps) => {
  const classes = useStyles();
  const inputRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;

  return (
    <Button
      className={classes.buttonContainer}
      onClick={(event) => inputRef.current!.click()}
    >
      <input
        ref={inputRef!}
        style={{ display: 'none' }}
        type='file'
        onChange={async (event) => {
          if (!!event.target.files) {
            const base64: string = await toBase64(event.target.files![0]);

            props.onUpload(base64);
          }
        }}
        accept='image/*'
        max={1}
      />
      <div className={classes.container}>
        <AddIcon className={classes.icon} />
      </div>
    </Button>
  );
};

export default NewGalleryItem;
