import React from 'react';
import { IconButton, createStyles, makeStyles, Theme } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      position: 'relative',
      width: '147px',
      height: '162px',
      backgroundSize: '300%',
      backgroundPosition: 'center',
      borderRadius: 10,
    },
    button: {
      fontSize: 20,
    },
    buttonContainer: {
      position: 'absolute',
      top: '-20x',
      right: '-10px',
      marginTop: '-10px',
      width: '30px',
      height: '30px',
      borderRadius: 20,
      backgroundColor: theme.palette.text.primary,
      '&:hover': {
        backgroundColor: theme.palette.text.primary,
      },
    },
  })
);

interface IProps {
  src: string;
  onClick: () => void;
}

const GalleryItem = (props: IProps) => {
  const classes = useStyles();

  return (
    <div
      className={classes.container}
      style={{
        backgroundImage: `linear-gradient(360deg, #4A4A4A 0%, rgba(117, 117, 117, 0) 55.47%), url(${props.src})`,
      }}
    >
      <IconButton
        className={classes.buttonContainer}
        onClick={() => props.onClick()}
      >
        <CloseIcon className={classes.button} />
      </IconButton>
    </div>
  );
};

export default GalleryItem;
