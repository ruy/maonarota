import React from "react";
import {Typography, makeStyles, createStyles, Theme} from "@material-ui/core";

import Header from "./Header";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      backgroundColor: theme.palette.primary.main,
      height: "100vh",
      width: "100vw",
      flexDirection: "column",
      justifyContent: "center",
      alignContent: "center",
      textAlign: "center",
      alignItems: "center",
    },
    button: {
      marginTop: "50px",
    },
    titleText: {
      color: theme.palette.secondary.main,
      fontSize: 30,
      fontWeight: 500,
    },
    warningText: {
      color: theme.palette.text.primary,

      fontSize: 21,
      marginTop: "5px",
      marginBottom: "30px",
      width: "30vw",
    },
  })
);

interface Props {}

const ErrorPage = (props: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Header />
      <Typography className={classes.titleText}>Ops!</Typography>
      <Typography className={classes.warningText}>
        Esta página não existe
      </Typography>
    </div>
  );
};

export default ErrorPage;
