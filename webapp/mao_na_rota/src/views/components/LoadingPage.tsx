import React from "react";
import {
  Typography,
  Button,
  makeStyles,
  createStyles,
  Theme,
} from "@material-ui/core";

import Header from "./Header";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      backgroundColor: theme.palette.primary.main,
      height: "100vh",
      width: "100vw",
      flexDirection: "column",
      justifyContent: "center",
      alignContent: "center",
      textAlign: "center",
    },
    subContainer: {
      display: "flex",
      backgroundColor: theme.palette.primary.main,
      flexDirection: "column",
      justifyContent: "center",
      alignContent: "center",
      alignItems: "center",
      textAlign: "center",
    },
    button: {
      marginTop: "50px",
    },
    titleText: {
      color: theme.palette.secondary.main,
      fontSize: 30,
      fontWeight: 500,
    },
    warningText: {
      color: theme.palette.text.primary,
      fontSize: 21,
      marginTop: "5px",
      marginBottom: "30px",
      width: "30vw",
    },
  })
);

interface Props {
  error: boolean;
  retry: () => void;
}

const renderError = (classes: any, retry: () => void) => {
  return (
    <div className={classes.subContainer}>
      <Typography className={classes.titleText}>Ops!</Typography>
      <Typography className={classes.warningText}>Ocorreu um erro</Typography>
      <Button variant="outlined" style={{width: "200px"}}>
        TENTAR NOVAMENTE
      </Button>
    </div>
  );
};

const renderLoading = (classes: any) => {
  return (
    <div className={classes.subContainer}>
      <Typography className={classes.titleText}>Carregando..</Typography>
    </div>
  );
};

const LoadingPage = (props: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Header />
      {props.error ? renderError(classes, props.retry) : renderLoading(classes)}
    </div>
  );
};

export default LoadingPage;
