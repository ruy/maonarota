import React from 'react';
import {
  Button,
  Typography,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';
import { Link, useLocation } from 'react-router-dom';

import TourPointModel from '../../features/tours/models/TourPointModel';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      backgroundColor: 'transparent',
      width: '100%',
      marginBottom: '50px',
    },
    text: {
      color: theme.palette.text.primary,
      fontSize: 20,
    },
    image: {
      height: '150px',
      width: '100%',
      backgroundSize: '100%',
      backgroundPosition: 'center',
    },
    button: {
      width: '100%',
      padding: 0,
      marginBottom: '20px',
    },
    titleContainer: {
      marginBottom: '10px',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',
    },
    suffixButton: {
      fontSize: 20,
      color: '#DC5252',
    },
  })
);

interface IProps {
  point: TourPointModel;
  onClick: () => void;
}

const TourPointListItem = (props: IProps) => {
  const classes = useStyles();
  let location = useLocation();

  return (
    <div className={classes.container}>
      <Link
        to={location.pathname + '/parada/' + props.point.id}
        className={classes.button}
      >
        <div
          className={classes.image}
          style={{
            backgroundImage: `linear-gradient(180deg, rgba(2,0,36,0) 55%, rgba(34,34,34,1) 100%), url(${props.point.pictureUrls[0]})`,
          }}
        />
      </Link>
      <div className={classes.titleContainer}>
        <Typography className={classes.text}>{props.point.title}</Typography>

        <Button onClick={() => props.onClick()}>
          <Typography className={classes.suffixButton}>
            REMOVER PARADA
          </Typography>
        </Button>
      </div>
    </div>
  );
};

export default TourPointListItem;
