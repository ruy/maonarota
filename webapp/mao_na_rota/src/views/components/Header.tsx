import React from "react";
import {
  Grid,
  Button,
  Toolbar,
  Typography,
  createStyles,
  makeStyles,
  Theme,
  AppBar,
} from "@material-ui/core";
import {Link} from "react-router-dom";

import logo from "../assets/images/logo_white.png";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: "flex",
      backgroundColor: theme.palette.primary.main,
      height: "100px",
      padding: "30px 60px",
    },
    subContainer: {
      display: "flex",
      backgroundColor: theme.palette.primary.main,
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      width: "100%",
    },
    logo: {
      width: "28px",
    },
    logoText: {
      color: "#FFFFFF",
      fontSize: "20px",
      textAlign: "left",
      marginLeft: "18px",
      fontWeight: 500,
    },
    buttonText: {
      color: "#FFFFFF",
      fontSize: "20px",
      fontWeight: 500,
    },
    leftButton: {
      marginRight: "20px",
    },
    toolbar: theme.mixins.toolbar,
  })
);

interface IProps {
  leftButtonText?: string | undefined;
  rightButtonText?: string | undefined;
  leftButtonTextColor?: string | undefined;
  rightButtonTextColor?: string | undefined;
  onClickLeftButton?:
    | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
    | undefined;
  onClickRightButton?:
    | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
    | undefined;
}

const Header = (props: IProps) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <AppBar elevation={0}>
        <Toolbar>
          <Link
            to={"/"}
            style={{textDecoration: "none"}}
            className={classes.subContainer}
          >
            <img src={logo} alt="logo" className={classes.logo} />
            <Typography className={classes.logoText}>MÃO NA ROTA</Typography>
          </Link>

          {(!!props.onClickLeftButton || !!props.onClickRightButton) && (
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
            >
              {!!props.leftButtonText && (
                <Button
                  onClick={props.onClickLeftButton}
                  className={classes.leftButton}
                >
                  <Typography
                    className={classes.buttonText}
                    style={{color: props.leftButtonTextColor}}
                  >
                    {props.leftButtonText}
                  </Typography>
                </Button>
              )}
              {!!props.rightButtonText && (
                <Button onClick={props.onClickRightButton}>
                  <Typography
                    className={classes.buttonText}
                    style={{color: props.rightButtonTextColor}}
                  >
                    {props.rightButtonText}
                  </Typography>
                </Button>
              )}
            </Grid>
          )}
        </Toolbar>
      </AppBar>
      <div className={classes.toolbar} />
    </React.Fragment>
  );
};

export default Header;
