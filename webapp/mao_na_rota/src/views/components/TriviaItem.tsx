import React from 'react';
import {
  TextField,
  Radio,
  FormControlLabel,
  Button,
  Typography,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';

import ActionButton from '../components/ActionButton';
import TriviaModel from '../../features/tours/models/TriviaModel';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      backgroundColor: 'transparent',
      width: '100%',
      marginBottom: '25px',
    },
    row: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: 'transparent',
      width: '100%',
      marginTop: '15px',
    },
    inputRow: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      backgroundColor: 'transparent',
      alignItems: 'center',
      width: '100%',
    },
    title: {
      color: theme.palette.text.primary,
      fontSize: 16,
      marginBottom: '5px',
      marginRight: '15px',
    },
    descriptionLabel: {
      marginTop: '15px',
      marginRight: '15px',
      marginBottom: '5px',
      color: theme.palette.text.primary,
      fontSize: 14,
    },
    description: {
      marginTop: '30px',
      color: theme.palette.text.secondary,
      fontSize: 18,
    },
    button: {
      padding: 0,
    },
    suffixButtonText: {
      fontSize: 18,
      color: '#DC5252',
      minWidth: '200px',
      padding: 0,
    },
    typeContainer: {
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: 'transparent',
      minWidth: '300px',
    },
  })
);

interface IProps {
  trivia: TriviaModel;
  onRemove: () => void;
  onSave: (trivia: Partial<TriviaModel>) => void;
}

interface IState {
  triviaDraft: Partial<TriviaModel>;
}

const TriviaItem = (props: IProps) => {
  const classes = useStyles();
  const [state, setState] = React.useState<IState>({
    triviaDraft: { ...props.trivia },
  });

  return (
    <div className={classes.container}>
      <div className={classes.inputRow}>
        <Typography className={classes.title}>TÍTULO</Typography>
        <TextField
          label='Insira o título'
          fullWidth
          variant='filled'
          margin='dense'
          InputProps={{ disableUnderline: true, className: 'color: red' }}
          color='secondary'
          value={state.triviaDraft.title}
          onChange={(
            event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) => {
            setState({
              ...state,
              triviaDraft: {
                ...state.triviaDraft,
                title: event.target.value,
              },
            });
            props.onSave({
              ...state.triviaDraft,
              title: event.target.value,
            });
          }}
        />
      </div>
      <div className={classes.inputRow}>
        <Typography className={classes.descriptionLabel}>DESCRIÇÃO</Typography>
        <TextField
          label='Insira a descrição'
          fullWidth
          variant='filled'
          margin='dense'
          multiline
          InputProps={{ disableUnderline: true }}
          value={state.triviaDraft.description}
          onChange={(
            event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) => {
            setState({
              ...state,
              triviaDraft: {
                ...state.triviaDraft,
                description: event.target.value,
              },
            });
            props.onSave({
              ...state.triviaDraft,
              description: event.target.value,
            });
          }}
        />
      </div>
      <div className={classes.row}>
        <ActionButton
          text={'Alterar anexo'}
          onUpload={() => {}}
          className={classes.button}
        />
        <div className={classes.typeContainer}>
          <FormControlLabel
            control={
              <Radio
                value={state.triviaDraft.type}
                color='secondary'
                onChange={(event) => {
                  setState({
                    ...state,
                    triviaDraft: {
                      ...state.triviaDraft,
                      type: 'audio',
                    },
                  });
                  props.onSave({
                    ...state.triviaDraft,
                    type: 'audio',
                  });
                }}
                checked={state.triviaDraft.type === 'audio'}
              />
            }
            label='Áudio'
          />
          <FormControlLabel
            control={
              <Radio
                value={state.triviaDraft.type}
                color='secondary'
                onChange={(event) => {
                  setState({
                    ...state,
                    triviaDraft: {
                      ...state.triviaDraft,
                      type: 'image',
                    },
                  });
                  props.onSave({
                    ...state.triviaDraft,
                    type: 'image',
                  });
                }}
                checked={state.triviaDraft.type === 'image'}
              />
            }
            label='Imagem'
          />
          <FormControlLabel
            control={
              <Radio
                value={state.triviaDraft.type}
                color='secondary'
                onChange={(event) => {
                  setState({
                    ...state,
                    triviaDraft: {
                      ...state.triviaDraft,
                      type: 'download',
                    },
                  });
                  props.onSave({
                    ...state.triviaDraft,
                    type: 'download',
                  });
                }}
                checked={state.triviaDraft.type === 'download'}
              />
            }
            label='Download'
          />
        </div>

        <Button onClick={() => props.onRemove()}>
          <Typography className={classes.suffixButtonText}>
            REMOVER TRIVIA
          </Typography>
        </Button>
      </div>
    </div>
  );
};

export default TriviaItem;
