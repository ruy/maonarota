import React from 'react';
import {
  Button,
  Typography,
  createStyles,
  makeStyles,
  Theme,
} from '@material-ui/core';

import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: 'transparent',
      height: '50px',
      padding: '30px 30px 30px 0px',
    },
    text: {
      marginLeft: '25px',
      color: theme.palette.text.primary,
      fontSize: 17,
    },
    iconContainer: {
      width: '50px',
      height: '50px',
      border: '1px solid #CBCBCB',
      boxSizing: 'border-box',
      borderRadius: '10px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    icon: {
      color: theme.palette.secondary.main,
      fontSize: 26,
    },
  })
);

const stringToIcon = (type: string, classes: any) => {
  switch (type) {
    case 'audio':
      return <VolumeUpIcon className={classes.icon} />;
    case 'map':
      return <LocationOnIcon className={classes.icon} />;
    default:
      return <AttachFileIcon className={classes.icon} />;
  }
};

const stringToFileFormat = (type: string) => {
  switch (type) {
    case 'audio':
      return 'audio/mp3';
    case 'map':
      return '.pbf';
    default:
      return 'image/*';
  }
};

interface IProps {
  text: string;
  icon?: string;
  className?: string | undefined;
  onUpload: (file: string) => void;
}

const ActionButton = (props: IProps) => {
  const classes = useStyles();
  const inputRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;

  return (
    <div className={props.className}>
      <Button
        className={classes.container}
        onClick={(event) => inputRef.current!.click()}
      >
        <input
          ref={inputRef!}
          style={{ display: 'none' }}
          type='file'
          onChange={(event) =>
            !!event.target.files &&
            props.onUpload(URL.createObjectURL(event.target.files![0]))
          }
          accept={stringToFileFormat(props.icon || '')}
          max={1}
        />
        <div className={classes.iconContainer}>
          {stringToIcon(props.icon || '', classes)}
        </div>
        <Typography className={classes.text}>
          {props.text.toUpperCase()}
        </Typography>
      </Button>
    </div>
  );
};

export default ActionButton;
