import React from 'react';
import { Dispatch } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import IStore from '../../features/IStore';
import LoginPage from './LoginPage';
import * as AuthAction from '../../features/auth/AuthAction';
import { selectRequesting } from '../../features/requesting/RequestingSelector';
import { selectErrorText } from '../../features/error/ErrorSelector';

interface IStateToProps {
	readonly authorized: boolean;
	readonly isLoggingIn: boolean;
	readonly loginErrorText: string;
}

interface IDispatchToProps {
	login(email: string, password: string): () => any;
}

const mapStateToProps = (state: IStore): IStateToProps => ({
	authorized: state.auth.authorized,
	isLoggingIn: selectRequesting(state, [ AuthAction.REQUEST_LOGIN ]),
	loginErrorText: selectErrorText(state, [ AuthAction.REQUEST_LOGIN_FINISHED ])
});

const mapDispatchToProps = (dispatch: Dispatch): IDispatchToProps => ({
	login: (email: string, password: string) => dispatch<any>(AuthAction.login(email, password))
});

const LoginPageContainer = (props: IStateToProps & IDispatchToProps) => {
	return props.authorized ? <Redirect to="/" /> : <LoginPage {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPageContainer);
