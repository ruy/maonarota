import React from 'react';

import { Grid, TextField, Typography, Button } from '@material-ui/core';
import logo from '../assets/images/logo_white.png';

import useStyles from './LoginPage.styles';

interface IProps {
  readonly isLoggingIn: boolean;
  readonly loginErrorText: string;
  login(email: string, password: string): () => any;
}

interface IState {
  email: string;
  errorEmail: boolean;
  password: string;
  errorPwd: boolean;
}

const initialState: IState = {
  email: '',
  errorEmail: false,
  password: '',
  errorPwd: false,
};

const isValidEmail = (email: string): boolean => {
  return email.trim().length > 0;
};

const isValidPassword = (password: string): boolean => {
  return password.trim().length > 0;
};

const LoginPage = (props: IProps) => {
  const classes = useStyles();
  const [state, setState] = React.useState<IState>(initialState);

  const handleLogin = (): any => {
    const errorEmail = !isValidEmail(state.email);
    const errorPassword = !isValidPassword(state.password);
    setState({
      ...state,
      errorEmail: errorEmail,
      errorPwd: errorPassword,
    });

    if (!errorEmail && !errorPassword) {
      return props.login(state.email, state.password);
    }
  };

  return (
    <div className={classes.container}>
      <Grid container direction='column' justify='center' alignItems='center'>
        <img src={logo} alt='logo' className={classes.logo} />
        <Typography className={classes.logoText}>MÃO NA ROTA</Typography>
        <TextField
          id='standard-basic'
          InputProps={{
            classes: {
              input: classes.inputText,
            },
          }}
          onChange={(event) =>
            setState({ ...state, email: event.target.value })
          }
          className={classes.input}
          InputLabelProps={{ style: { width: '100%', textAlign: 'center' } }}
          inputProps={{ min: 0, style: { textAlign: 'center' } }}
          label='Email'
        />
        <TextField
          id='standard-basic'
          InputProps={{
            classes: {
              input: classes.inputText,
            },
          }}
          onChange={(event) =>
            setState({ ...state, password: event.target.value })
          }
          type='password'
          className={classes.input}
          InputLabelProps={{ style: { width: '100%', textAlign: 'center' } }}
          inputProps={{
            min: 0,
            style: { textAlign: 'center', borderBottomColor: 'white' },
          }}
          label='Senha'
        />
        <Button
          variant='outlined'
          className={classes.button}
          onClick={() => handleLogin()}
        >
          ENTRAR
        </Button>
      </Grid>
    </div>
  );
};

export default LoginPage;
