import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
// import background from '../assets/images/login-background.svg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      backgroundColor: theme.palette.primary.main,
      height: '100vh',
    },
    logo: {
      width: '100px',
      marginBottom: '20px',
    },
    logoText: {
      color: '#FFFFFF',
      fontSize: '20px',
      textAlign: 'center',
      marginBottom: '10px',
      fontWeight: 500,
    },
    inputText: {
      color: theme.palette.text.primary,
      fontSize: '16px',
      textAlign: 'center',
    },
    button: {
      marginTop: '50px',
    },
    input: {
      marginTop: '10px',
      borderBottomColor: theme.palette.text.secondary,
      borderBottom: `.5px solid`
    }
  }),
);

export default useStyles;
