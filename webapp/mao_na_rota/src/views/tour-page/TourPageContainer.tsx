import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { Switch, useHistory } from 'react-router-dom';

import IStore from '../../features/IStore';
import TourPage from '../tour-page/TourPage';
import * as TourAction from '../../features/tours/TourAction';
import TourModel from '../../features/tours/models/TourModel';

import AuthRoute from '../../routes/AuthRoute';
import LoadingPage from '../../views/components/LoadingPage';
import ErrorPage from '../../views/components/ErrorPage';

const TourPointContainer = Loadable({
  loader: () => import('../../views/tour-point-page/TourPointPageContainer'),
  loading: (props: any) =>
    LoadingPage({ ...props, retry: () => window.location.reload(true) }),
});

const TourPointContainerCreation = Loadable({
  render(loaded, props) {
    let Component = loaded.default;
    return <Component {...props} creation />;
  },
  loader: () => import('../../views/tour-point-page/TourPointPageContainer'),
  loading: (props: any) =>
    LoadingPage({ ...props, retry: () => window.location.reload(true) }),
});

interface IStateToProps {
  readonly tour: TourModel;
  readonly creation: boolean;
}

interface IDispatchToProps {
  readonly onRemove: (tourId: string) => void;
  readonly onCreateTourPoint: (tourId: string) => void;
  // readonly onRemoveTourPoint: (tourId: string, tourPointId: string) => void;
  readonly onSave: (tour: Partial<TourModel>) => void;
}

const mapStateToProps = (state: IStore, ownProps: any): IStateToProps => ({
  tour: ownProps.creation
    ? new TourModel({})
    : (state.tours.tours || []).find(
        (tour) => tour.id === ownProps.match.params.tourId
      )!,
  creation: ownProps.creation,
});

const mapDispatchToProps = (
  dispatch: Dispatch,
  ownProps: any
): IDispatchToProps => ({
  onRemove: (tourId: string) => {
    dispatch<any>(TourAction.removeTour(tourId));
    ownProps.history.push('/');
  },
  onCreateTourPoint: (tourId: string) => {
    ownProps.history.push('/rota/' + tourId + '/parada/nova');
  },
  // onRemoveTourPoint: (tourId: string, tourPointId: string) =>
  //   dispatch<any>(TourAction.removeTourPoint(tourId, tourPointId)),
  onSave: ownProps.creation
    ? (tour: Partial<TourModel>) => {
        dispatch<any>(TourAction.createTour(tour));
        ownProps.history.push('/');
      }
    : (tour: Partial<TourModel>) => {
        dispatch<any>(TourAction.saveTour(tour));
        ownProps.history.push('/');
      },
});

const TourPageContainer = (props: IStateToProps & IDispatchToProps) => {
  const history = useHistory();
  const { tour }: { tour: TourModel } = props;

  React.useEffect(() => {
    if (!tour) {
      history.replace('/erro');
    }
  }, []);

  return (
    <Switch>
      <AuthRoute key={10} path='/rota/:tourId' exact>
        <TourPage {...props} />
      </AuthRoute>
      <AuthRoute key={11} path='/rota/nova' exact>
        <TourPage {...props} creation />
      </AuthRoute>
      <AuthRoute
        key={3}
        path='/rota/:tourId/parada/nova'
        exact
        component={TourPointContainerCreation}
      />
      <AuthRoute
        key={12}
        path='/rota/:tourId/parada/:tourPointId'
        exact
        component={TourPointContainer}
      />
      <AuthRoute key={13} component={ErrorPage} />
    </Switch>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(TourPageContainer);
