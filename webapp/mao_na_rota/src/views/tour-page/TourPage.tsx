import React, { MutableRefObject } from 'react';
import { TextField, Button, Typography } from '@material-ui/core';

import useStyles from './TourPage.styles';
import Header from '../components/Header';
import ActionButton from '../components/ActionButton';
import TourPointListItem from '../components/TourPointListItem';
import theme from '../../theme';
import TourModel from '../../features/tours/models/TourModel';
import { toBase64 } from '../../helpers/base64';

interface IProps {
  tour: TourModel;
  readonly creation: boolean;
  readonly onCreateTourPoint: (tourId: string) => void;
  readonly onRemove: (tourId: string) => void;
  readonly onSave: (tour: Partial<TourModel>) => void;
  // readonly onRemoveTourPoint: (tourId: string, tourPointId: string) => void;
}

interface IState {
  tourDraft: Partial<TourModel>;
}

const TourPage = (props: IProps) => {
  const imageInput = React.useRef() as React.MutableRefObject<HTMLInputElement>;
  const classes = useStyles();
  const [state, setState] = React.useState<IState>({
    tourDraft: { ...props.tour },
  });

  React.useEffect(() => {
    if (!props.tour) {
      setState((state) => {
        return {
          ...state,
          tourDraft: {
            ...state.tourDraft,
            ...props.tour,
          },
        };
      });
    }
  }, [props.tour]);

  return (
    <div className={classes.container}>
      <Header
        onClickLeftButton={
          !props.creation
            ? (_) => {
                props.onRemove(props.tour.id);
              }
            : undefined
        }
        onClickRightButton={(_) => props.onSave(state.tourDraft)}
        leftButtonText={!props.creation ? 'REMOVER ROTA' : undefined}
        rightButtonText='SALVAR'
        leftButtonTextColor={'#DC5252'}
        rightButtonTextColor={theme.palette.text.primary}
      />
      <div
        className={classes.headerImage}
        style={{
          backgroundImage: `linear-gradient(180deg, rgba(2,0,36,0) 55%, rgba(34,34,34,1) 100%), url(${`${state.tourDraft.pictureUrl}`})`,
        }}
      >
        <Button
          style={{ backgroundColor: 'transparent' }}
          onClick={(event) => imageInput.current!.click()}
        >
          <Typography className={classes.text}>
            <input
              ref={imageInput!}
              style={{ display: 'none' }}
              type='file'
              onChange={async (event) => {
                if (!!event.target.files) {
                  const picture: string = await toBase64(
                    event.target.files![0]
                  );

                  setState({
                    ...state,
                    tourDraft: {
                      ...state.tourDraft,
                      pictureUrl: picture,
                    },
                  });
                }
              }}
              accept='image/*'
              max={1}
            />
            {state.tourDraft.pictureUrl !== null &&
            state.tourDraft.pictureUrl !== ''
              ? 'ALTERAR FOTO'
              : 'ADICIONAR FOTO'}
          </Typography>
        </Button>
      </div>
      <div className={classes.infoContainer}>
        <Typography className={classes.title}>TÍTULO</Typography>
        <TextField
          label='Insira o título'
          fullWidth
          variant='filled'
          margin='dense'
          InputProps={{ disableUnderline: true, className: 'color: red' }}
          className={classes.title}
          color='secondary'
          value={state.tourDraft.title}
          onChange={(
            event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) =>
            setState({
              ...state,
              tourDraft: {
                ...state.tourDraft,
                title: event.target.value,
              },
            })
          }
        />
        <Typography className={classes.descriptionLabel}>DESCRIÇÃO</Typography>
        <TextField
          label='Insira a descrição'
          fullWidth
          variant='filled'
          margin='dense'
          multiline
          InputProps={{ disableUnderline: true }}
          value={state.tourDraft.description}
          onChange={(
            event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
          ) =>
            setState({
              ...state,
              tourDraft: {
                ...state.tourDraft,
                description: event.target.value,
              },
            })
          }
        />
        <ActionButton
          text={'Alterar áudio descrição'}
          onUpload={() => {}}
          icon='audio'
          className={classes.button}
        />
        <div className={classes.sectionRowContainer}>
          <Typography className={classes.sectionTitle}>DURAÇÃO</Typography>
          <TextField
            label='Insira a duração'
            variant='filled'
            margin='dense'
            InputProps={{ disableUnderline: true }}
            className={classes.sectionInput}
            value={state.tourDraft.duration}
            onChange={(
              event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) =>
              setState({
                ...state,
                tourDraft: {
                  ...state.tourDraft,
                  duration: parseInt(event.target.value),
                },
              })
            }
          />
          <Typography className={classes.text}>minutos</Typography>
        </div>
        <div className={classes.sectionRowContainer}>
          <Typography className={classes.sectionTitle}>DISTÂNCIA</Typography>
          <TextField
            label='Insira a distância percorrida'
            variant='filled'
            margin='dense'
            InputProps={{ disableUnderline: true }}
            className={classes.sectionInput}
            value={state.tourDraft.distance}
            onChange={(
              event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) =>
              setState({
                ...state,
                tourDraft: {
                  ...state.tourDraft,
                  distance: parseInt(event.target.value),
                },
              })
            }
          />
          <Typography className={classes.text}>km</Typography>
        </div>
        <div className={classes.sectionContainer}>
          <Typography className={classes.sectionTitle}>PARADAS</Typography>
          <Button onClick={() => props.onCreateTourPoint(props.tour.id)}>
            <Typography className={classes.sectionButton}>
              + ADICIONAR PARADA
            </Typography>
          </Button>
        </div>
        {!!state.tourDraft?.points &&
          state.tourDraft.points.map((point) => (
            <TourPointListItem
              key={'tour-' + props.tour.id + '-point-list-item-' + point.id}
              point={point}
              onClick={() =>
                setState({
                  ...state,
                  tourDraft: {
                    ...state.tourDraft,
                    points: state.tourDraft.points!.filter(
                      (pointItem) => point.id !== pointItem.id
                    ),
                  },
                })
              }
            />
          ))}
      </div>
    </div>
  );
};

export default TourPage;
