import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
  useLocation,
} from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Loadable from 'react-loadable';
import { Dispatch } from 'redux';
import { useDispatch } from 'react-redux';

import * as AuthAction from './features/auth/AuthAction';
import theme from './theme';
import AuthRoute from './routes/AuthRoute';
import LoadingPage from './views/components/LoadingPage';

const LoginPageContainer = Loadable({
  loader: () => import('./views/login-page/LoginPageContainer'),
  loading: (props: any) =>
    LoadingPage({ ...props, retry: () => window.location.reload(true) }),
});

const ToursContainer = Loadable({
  loader: () => import('./views/tours-page/ToursPageContainer'),
  loading: (props: any) =>
    LoadingPage({ ...props, retry: () => window.location.reload(true) }),
});

function App() {
  const { pathname } = useLocation();
  const dispatch: Dispatch = useDispatch();

  React.useEffect(() => {
    // dispatch<any>(AuthAction.getSession());
    dispatch<any>(AuthAction.getSession());
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter basename='/'>
        <Switch>
          <Redirect from='/:url*(/+)' to={pathname.slice(0, -1)} />
          <Route key={0} exact path='/login' component={LoginPageContainer} />
          <AuthRoute key={1} path='/' component={ToursContainer} />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
