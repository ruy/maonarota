import axios, { AxiosRequestConfig, AxiosResponse, Method } from 'axios';

import environment from "../environment";
import HttpErrorResponseModel from "../models/HttpErrorResponseModel";

const DEFAULT_HEADERS = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
};

const getHeaders = (headers: any) => {
  return {
    ...DEFAULT_HEADERS,
    ...headers,
  };
}

const defaultValidateStatus = (status: number) => status < 500;

const { baseUri, timeout, responseType, maxRedirects } = environment.maonarota;

const client = () => axios.create({
  baseURL: baseUri,
  timeout: timeout,
  responseType: (responseType as 'json'),
  maxRedirects: maxRedirects,
});

const apiRequest = async (restRequest: Partial<Request>, opts: any): Promise<any | HttpErrorResponseModel> => {
  try {
    const body = opts.body || {};
    const headers = getHeaders(opts.headers);
    const validateStatus = opts.validateStatus || defaultValidateStatus;

    const axiosRequestConfig: AxiosRequestConfig = {
      url: restRequest.url,
      method: restRequest.method as Method,
      headers: headers,
      data: body,
      validateStatus: validateStatus,
    };

    const { data }: AxiosResponse = await client().request(axiosRequestConfig);
    return data;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code that falls out of the range of 2xx
      const { status, statusText, data } = error.response;
      const errors: string[] = (data !== null && data.hasOwnProperty('errors')) ?
        [statusText, ...data.errors] : [statusText];

      return _fillInErrorWithDefaults(
        {
          status,
          message: errors.filter(Boolean).join(' - '),
          errors,
          url: error.request.responseURL,
          raw: error.response,
        },
        restRequest
      );
    } else if (error.request) {
      // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
      const { status, statusText, responseURL } = error.request;

      return _fillInErrorWithDefaults(
        {
          status,
          message: statusText,
          errors: [statusText],
          url: responseURL,
          raw: error.request,
        },
        restRequest
      );
    }

    // Something happened in setting up the request that triggered an Error
    return _fillInErrorWithDefaults(
      {
        status: 0,
        message: error.message,
        errors: [error.message],
        url: restRequest.url!,
        raw: error,
      },
      restRequest
    );
  }
};

export const login = async (email: string, password: string) => {
  return await apiRequest(
    {
      url: environment.maonarota.login,
      method: 'post'
    },
    {
      body: {
        email: email,
        password: password
      },
      validateStatus: (status: number) => status === 200,
    })
};

export const getTour = async (tourId: string) => {
  return await apiRequest(
    {
      url: environment.maonarota.tours.getById.replace(':tour-id', tourId),
      method: 'get',
    },
    {
      validateStatus: (status: number) => status === 200,
    }
  );
};

const _fillInErrorWithDefaults = (error: Partial<HttpErrorResponseModel>, request: Partial<Request>): HttpErrorResponseModel => {
  const model = new HttpErrorResponseModel();

  model.status = error.status || 0;
  model.message = error.message || 'Error requesting data';
  model.errors = error.errors!.length ? error.errors! : ['Error requesting data'];
  model.url = error.url || request.url!;
  model.raw = error.raw;

  // Remove anything with undefined or empty strings.
  model.errors = model.errors.filter(Boolean);

  return model;
};
