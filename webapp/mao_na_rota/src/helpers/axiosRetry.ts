import axios, { AxiosRequestConfig, AxiosPromise, AxiosError, AxiosResponse } from 'axios';
import AsyncRetry from 'async-retry';

const WHITELIST = [
  'ETIMEDOUT',
  'ECONNRESET',
  'EADDRINUSE',
  'ESOCKETTIMEDOUT',
  'ECONNREFUSED',
  'EPIPE',
];

const BLACKLIST = [
  'ENOTFOUND',
  'ENETUNREACH',

  // SSL errors from https://github.com/nodejs/node/blob/ed3d8b13ee9a705d89f9e0397d9e96519e7e47ac/src/node_crypto.cc#L1950
  'UNABLE_TO_GET_ISSUER_CERT',
  'UNABLE_TO_GET_CRL',
  'UNABLE_TO_DECRYPT_CERT_SIGNATURE',
  'UNABLE_TO_DECRYPT_CRL_SIGNATURE',
  'UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY',
  'CERT_SIGNATURE_FAILURE',
  'CRL_SIGNATURE_FAILURE',
  'CERT_NOT_YET_VALID',
  'CERT_HAS_EXPIRED',
  'CRL_NOT_YET_VALID',
  'CRL_HAS_EXPIRED',
  'ERROR_IN_CERT_NOT_BEFORE_FIELD',
  'ERROR_IN_CERT_NOT_AFTER_FIELD',
  'ERROR_IN_CRL_LAST_UPDATE_FIELD',
  'ERROR_IN_CRL_NEXT_UPDATE_FIELD',
  'OUT_OF_MEM',
  'DEPTH_ZERO_SELF_SIGNED_CERT',
  'SELF_SIGNED_CERT_IN_CHAIN',
  'UNABLE_TO_GET_ISSUER_CERT_LOCALLY',
  'UNABLE_TO_VERIFY_LEAF_SIGNATURE',
  'CERT_CHAIN_TOO_LONG',
  'CERT_REVOKED',
  'INVALID_CA',
  'PATH_LENGTH_EXCEEDED',
  'INVALID_PURPOSE',
  'CERT_UNTRUSTED',
  'CERT_REJECTED',
];

const isRetryAllowed = (err: AxiosError) => {
  if (!err || !err.code) {
    return true;
  }

  if (WHITELIST.indexOf(err.code) !== -1) {
    return true;
  }

  if (BLACKLIST.indexOf(err.code) !== -1) {
    return false;
  }

  return true;
};

export async function retry<T>(fn: () => AxiosPromise<T>, shouldRetry: (resp: AxiosResponse<T>, bail: (e: Error) => void) => boolean): Promise<T | null> {
  return AsyncRetry(
    async bail => {
      let res = null;
      try {
        res = await fn();
      } catch (err) {
        if (err.isAxiosError && isRetryAllowed(err)) {
          throw err; // AsyncRetry catches it
        }

        bail(err);
        return null;
      }

      if (shouldRetry(res, bail)) {
        throw new Error('shouldRetry condition');
      }

      return res.data;
    },
    { retries: 5 });
}
