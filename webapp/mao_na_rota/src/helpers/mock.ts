export async function getTours(): Promise<any[]> {
  return [
    {
      id: '1',
      updatedAt: '',
      title: 'Titulo',
      points: [
        {
          id: '10',
          title: 'Titulo',
          pictureUrls: [
            'https://images.unsplash.com/photo-1562932831-afcfe48b5786',
          ],
        },
      ],
      pictureUrl: 'https://images.unsplash.com/photo-1562932831-afcfe48b5786',
    },
  ];
}
